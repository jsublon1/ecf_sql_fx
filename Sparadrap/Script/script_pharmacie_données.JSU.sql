USE pharmacieJSU;

SET FOREIGN_KEY_CHECKS = 0;

TRUNCATE TABLE adresse;
INSERT INTO adresse (ADRESSE_NUMERORUE, ADRESSE_NOMRUE, ADRESSE_CODEPOSTAL, ADRESSE_NOMVILLE)
VALUES 	(75,"boulevard maréchal Foch", "54520", "Laxou"),
		(30, "rue de Nancy", "54630", "Richardménil"),
		(9, "impasse inconnue", "69007", "Une Ville en France"),
		(5, "chemin des marrons","54000", "Nancy"),
		(15, "avenue des délires", "54000", "Nancy"),
		(75, "rue Sacré Coeur","83990", "Saint-Tropez"),
		(9, "boulevard squelette","54000", "Nancy"),
		(3, "impasse des amoureux","19000", "Tulle"),
		(24, "rue qui n'existe pas","80000", "Amien"),
		(25, "rue des platres","29200", "Brest"),
		(26, "avenue principal","14000", "Caen"),
		(27, "boulevard général manager","01500", "Douvres"),
		(28, "rue des Vosges","88000", "Epinal"),
		(29, "rue abracadabra","76290", "Fontenay"),
		(30, "boulevard parralèle","71130", "Gueugnon"),
		(31, "avenue du parvenue","67500", "Haguenau");

TRUNCATE TABLE mutuelle;
INSERT INTO mutuelle (  ADRESSE_ID,
   MUTUELLE_NOM,
   MUTUELLE_EMAIL,
   MUTUELLE_NUMEROTELEPHONE,
   MUTUELLE_DEPARTEMENT,
   MUTUELLE_TAUXPRISEENCHARGE)
VALUES 	(9,"Entubeur Mutuelle","no.reply@rien.com", "0000000000", "Creuse", 12),
		(3,"??? Mutuelle","mystere.mutuelle@alacazam.com", "0101010101", "Martinique", 80),
		(10,"La mutuelle","lamutuelle@alacazam.com", "0101010102", "Finistere", 80),
		(11,"Covid plus","covid.pluse@alacazam.com", "0101010103", "Calvados", 80);

TRUNCATE TABLE specialite;
INSERT INTO specialite (SPECIALITE_NOM)
VALUES 	("cardialogue"),
		("dermatologue"),
		("gynecologue"),
		("urologue"),
		("oncologie"),
		("merdologie");

TRUNCATE TABLE typemedicament;
INSERT INTO typemedicament(TYPEMEDICAMENT_NOM)
VALUES	("analgesique"),
		("antibiotique"),
		("dermatologie"),
		("contraception"),
		("urologie"),
		("oncologie"),
		("Merdigique");

TRUNCATE TABLE medicament;
INSERT INTO medicament(TYPEMEDICAMENT_ID,
   MEDICAMENT_NOM,
   MEDICAMENT_PRIX,
   MEDICAMENT_DATEMISESERVICE,
   MEDICAMENT_STOCK)
VALUES	(1, "Ouch", 3.50, "1994-11-25", 300),
		(2, "Magik", 24.50, "1978-08-15", 800),
        (3, "Clear Skin", 16, "2009-05-12", 223),
        (4, "No Baby YEAH", 64, "1999-01-21", 154),
        (5, "Up", 25, "2017-12-31", 210),
        (6, "Chimio plus", 15, "2016-11-30", 350),
        (7, "Popo", 10, "2015-10-29", 1),
        (1, "Glissgliss 500", 8, "2014-09-28", 498),
        (2, "Oflocet", 11, "2013-08-27", 987),
        (7, "Sirop", 24, "2012-07-26", 268);
        

TRUNCATE TABLE personne;
INSERT INTO personne  (ADRESSE_ID,
   SPECIALITE_ID,
   MEDECIN_ID,
   MUTUELLE_ID,
   PERSONNE_NOM,
   PERSONNE_PRENOM,
   PERSONNE_EMAIL,
   PERSONNE_NUMEROTELEPHONE,
   PERSONNE_DATENAISSANCE,
   PERSONNE_NUMERO_SECURITE_SOCIALE,
   PERSONNE_NUMERO_AGREMENT)
VALUES	(5,null, null, null, "Foldingue", "Damien", "damien.foldingue@admdf.doc", "0345967412", null, null, "41684J648I36157"),
		(6, null, null, null, "Dorian", "John", "jdthedoc@apple.com","0494326845", null, null, "40000LP68490243"),
		(7, 3, null, null, "Bones", "Clara","clara.bones@admdf.doc", "0345218763", null, null, null),
		(8, 1, null, null, "Heart", "Love","l.heart@admdf.doc", "0345658941", null, null, null),
		(2, null, null, null, "Retournay", "Steve","steve.Retournayaccrobatique@yahoo.fr", "0619864580", null, null, "4568F14M3589475"),
		(4, null, 5, 1, "Balboa", "Rocky","rocky.balboa@yahoo.fr", "0635679456", "1970-02-23", "100000000000000", null),
        (1, null, 2, 2, "Sublon", "Julien","julien.sublon@best.com", "0612345678", "1970-02-23", "186088841300015", null),
        (11, 5, null, null, "Miraculeux", "Mickey","mikey.miracle@holy.com", "0345698741", "1989-11-21", null, null),
        (12, 6, null, null, "Persin", "Terence","ter.pers@hotmail.fr", "0698745625", "1984-03-15", null, null),
        (13, null, null, null, "Baudon", "Alan","francis.lalane@yahoo.com", "0668489651", "1994-05-26", null, "45169H984P14587"),
        (14, null, 10, 3, "Chirean", "Alex","kiki@moi.fr", "0648965471", "1979-09-23", "179095412596512", null),
        (15, null, 5, 4, "Bayat", "Eshan","eshan.the.machine@apple.com", "0669852367", "1993-02-14", "193026917598415", null),
        (16, null, 1, 1, "Duwig", "Nicolas","saint.nicolas@noel.com", "0422521010", "1998-07-11", "198078848961513", null);

TRUNCATE specialiste;
INSERT INTO specialiste( CLIENT_ID, SPECIALISTE_ID)
VALUES 	(7,3),
		(11,8),
		(11,9),
		(12,3),
		(12,4),
		(13,8),
		(13,9),
		(13,3);

TRUNCATE TABLE achat;
INSERT INTO achat ( CLIENT_ID,
   SPECIALISTE_ID,
   MEDECIN_ID,
   ACHAT_DATE)
VALUES	(6,null,null, NOW()),
		(7, 3, 2, NOW()),
		(12, 4, 5, NOW()),
		(13, 8, 1, NOW()),
		(13, 9, 10, NOW()),
        (7,null,null, NOW());
        
TRUNCATE TABLE contenir;
INSERT INTO contenir ( MEDICAMENT_ID, ACHAT_ID, CONTENIR_QUANTITE)
VALUES	(3,1,2),
		(5,2,5),
		(6,2,1),
		(7,2,3),
		(8,3,1),
		(9,3,2),
		(10,4,3),
		(1,4,1),
		(2,4,1),
		(3,4,2),
		(4,5,3),
		(5,5,1),
		(6,5,2),
		(7,6,1),
		(8,6,2);

SET FOREIGN_KEY_CHECKS = 1;
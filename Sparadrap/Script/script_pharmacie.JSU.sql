/*==============================================================*/
/* Nom de SGBD :  MySQL 5.0                                     */
/* Date de création :  03/11/2022 09:34:58                      */
/*==============================================================*/

drop database if exists pharmacieJSU;
create database pharmacieJSU;
use pharmacieJSU;


/*==============================================================*/
/* Table : ADRESSE                                              */
/*==============================================================*/
create table ADRESSE
(
   ADRESSE_ID           int not null auto_increment,
   ADRESSE_NUMERORUE    char(5) not null,
   ADRESSE_NOMRUE       char(50) not null,
   ADRESSE_CODEPOSTAL   char(5) not null,
   ADRESSE_NOMVILLE     char(20) not null,
   primary key (ADRESSE_ID)
);

/*==============================================================*/
/* Table : SPECIALITE                                           */
/*==============================================================*/
create table SPECIALITE
(
   SPECIALITE_ID        int not null auto_increment,
   SPECIALITE_NOM       char(20) not null,
   primary key (SPECIALITE_ID)
);

/*==============================================================*/
/* Table : MUTUELLE                                             */
/*==============================================================*/
create table MUTUELLE
(
   MUTUELLE_ID          int not null auto_increment,
   ADRESSE_ID           int not null,
   MUTUELLE_NOM         char(20) not null,
   MUTUELLE_EMAIL       char(30) not null,
   MUTUELLE_NUMEROTELEPHONE char(10) not null,
   MUTUELLE_DEPARTEMENT char(20) not null,
   MUTUELLE_TAUXPRISEENCHARGE decimal(2,0) not null,
   primary key (MUTUELLE_ID),
   constraint FK_LOGER foreign key (ADRESSE_ID)
      references ADRESSE (ADRESSE_ID)
);

/*==============================================================*/
/* Table : PERSONNE                                             */
/*==============================================================*/
create table PERSONNE
(
   PERSONNE_ID          int not null auto_increment,
   ADRESSE_ID           int not null,
   SPECIALITE_ID        int,
   MEDECIN_ID           int,
   MUTUELLE_ID          int,
   PERSONNE_NOM         varchar(20) not null,
   PERSONNE_PRENOM      varchar(20) not null,
   PERSONNE_EMAIL       varchar(50) not null,
   PERSONNE_NUMEROTELEPHONE char(10) not null,
   PERSONNE_DATENAISSANCE date,
   PERSONNE_NUMERO_SECURITE_SOCIALE char(15) UNIQUE,
   PERSONNE_NUMERO_AGREMENT char(15) UNIQUE,
   primary key (PERSONNE_ID),
   constraint FK_HABITER foreign key (ADRESSE_ID)
      references ADRESSE (ADRESSE_ID),
   constraint FK_EXCERCER foreign key (SPECIALITE_ID)
      references SPECIALITE (SPECIALITE_ID),
   constraint FK_MEDECIN_TRAITANT foreign key (MEDECIN_ID)
      references PERSONNE (PERSONNE_ID),
   constraint FK_REMBOURSER foreign key (MUTUELLE_ID)
      references MUTUELLE (MUTUELLE_ID),
   constraint CK_Med_Spe CHECK ( NOT(PERSONNE_NUMERO_AGREMENT IS NOT NULL AND SPECIALITE_ID IS NOT NULL))
);



/*==============================================================*/
/* Table : ACHAT                                                */
/*==============================================================*/
create table ACHAT
(
   ACHAT_ID             int not null auto_increment,
   CLIENT_ID            int not null,
   SPECIALISTE_ID       int,
   MEDECIN_ID           int,
   ACHAT_DATE           date,
   primary key (ACHAT_ID),
   constraint FK_CLIENT foreign key (CLIENT_ID)
      references PERSONNE (PERSONNE_ID),
   constraint FK_RECOMMANDER foreign key (SPECIALISTE_ID)
      references PERSONNE (PERSONNE_ID),
   constraint FK_PRESCRIRE foreign key (MEDECIN_ID)
      references PERSONNE (PERSONNE_ID)
);

/*==============================================================*/
/* Table : TYPEMEDICAMENT                                       */
/*==============================================================*/
create table TYPEMEDICAMENT
(
   TYPEMEDICAMENT_ID    int not null auto_increment,
   TYPEMEDICAMENT_NOM   char(20) not null,
   primary key (TYPEMEDICAMENT_ID)
);

/*==============================================================*/
/* Table : MEDICAMENT                                           */
/*==============================================================*/
create table MEDICAMENT
(
   MEDICAMENT_ID        int not null auto_increment,
   TYPEMEDICAMENT_ID    int not null,
   MEDICAMENT_NOM       char(20) not null,
   MEDICAMENT_PRIX      float(5,2) not null,
   MEDICAMENT_DATEMISESERVICE date not null,
   MEDICAMENT_STOCK     int not null,
   primary key (MEDICAMENT_ID),
   constraint FK_CATEGORISER foreign key (TYPEMEDICAMENT_ID)
      references TYPEMEDICAMENT (TYPEMEDICAMENT_ID),
	constraint CK_STOCK CHECK (MEDICAMENT_STOCK>=0)
);

/*==============================================================*/
/* Table : CONTENIR                                             */
/*==============================================================*/
create table CONTENIR
(
   MEDICAMENT_ID        int not null,
   ACHAT_ID             int not null,
   CONTENIR_QUANTITE    smallint,
   primary key (MEDICAMENT_ID, ACHAT_ID),
   constraint FK_CONTENIR2 foreign key (MEDICAMENT_ID)
      references MEDICAMENT (MEDICAMENT_ID),
   constraint FK_CONTENIR foreign key (ACHAT_ID)
      references ACHAT (ACHAT_ID),
   constraint CK_quantite CHECK (CONTENIR_QUANTITE>=0)
);

/*==============================================================*/
/* Table : SPECIALISTE                                          */
/*==============================================================*/
create table SPECIALISTE
(
   CLIENT_ID            int not null,
   SPECIALISTE_ID       int not null,
   primary key (CLIENT_ID, SPECIALISTE_ID),
   constraint FK_SPECIALISTE2 foreign key (CLIENT_ID)
      references PERSONNE (PERSONNE_ID),
   constraint FK_SPECIALISTE foreign key (SPECIALISTE_ID)
      references PERSONNE (PERSONNE_ID)
);

/*==============================================================*/
/* Procédure vérification médecin                               */
/*==============================================================*/

DELIMITER |
DROP PROCEDURE IF EXISTS verif_medecin|
CREATE PROCEDURE verif_medecin (IN med_id INT, IN per_id INT)
BEGIN
			IF med_id NOT IN (SELECT PERSONNE_ID FROM personne WHERE PERSONNE_NUMERO_AGREMENT IS NOT NULL)
				THEN
					SIGNAL SQLSTATE '45000'
					SET MESSAGE_TEXT = "Le médecin n'est pas correct";
			ELSE IF per_id = med_id
				THEN 
					SIGNAL SQLSTATE '45000'
					SET MESSAGE_TEXT = "Un médecin ne peut pas être son propre médecin traitant";
				END IF;
			END IF;
END|
DELIMITER ;

/*==============================================================*/
/* Procédure vérification client                                */
/*==============================================================*/

DELIMITER |
DROP PROCEDURE IF EXISTS verif_client|
CREATE PROCEDURE verif_client (IN cli_id INT)
BEGIN
			IF cli_id NOT IN (SELECT PERSONNE_ID FROM personne WHERE MUTUELLE_ID IS NOT NULL 
																	AND PERSONNE_DATENAISSANCE IS NOT NULL 
																	AND PERSONNE_NUMERO_SECURITE_SOCIALE IS NOT NULL
                                                                    AND MEDECIN_ID IS NOT NULL)
				THEN
					SIGNAL SQLSTATE '45000'
					SET MESSAGE_TEXT = "Le client n'est pas correct";
			END IF;
END|
DELIMITER ;

/*==============================================================*/
/* Procédure vérification spécialiste                           */
/*==============================================================*/

DELIMITER |
DROP PROCEDURE IF EXISTS verif_specialiste|
CREATE PROCEDURE verif_specialiste (IN spe_id INT)
BEGIN
			IF spe_id NOT IN (SELECT PERSONNE_ID FROM personne WHERE SPECIALITE_ID IS NOT NULL)
				THEN
					SIGNAL SQLSTATE '45000'
					SET MESSAGE_TEXT = "Le spécialiste n'est pas correct";
			END IF;
END|
DELIMITER ;

/*==============================================================*/
/* Tiggers : PERSONNE                                           */
/*==============================================================*/

DELIMITER |
DROP TRIGGER IF EXISTS creation_client|
CREATE TRIGGER creation_client
	BEFORE INSERT ON personne
	FOR EACH ROW
	BEGIN
		IF NEW.personne_datenaissance > CURDATE() 
			THEN
					SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = "Date de naissance invalide";
		ELSE
				IF new.medecin_id IS NOT NULL
					THEN 
						CALL verif_medecin(new.medecin_id, new.personne_id);
				END IF;
		END IF;
	END|
DELIMITER ;

DELIMITER |
DROP TRIGGER IF EXISTS update_client|
CREATE TRIGGER update_client
	BEFORE UPDATE ON personne
	FOR EACH ROW
	BEGIN
		IF new.medecin_id IS NOT NULL
			THEN 
				CALL verif_medecin(new.medecin_id,new.personne_id);
		END IF;
	END|
DELIMITER ;

/*==============================================================*/
/* Tiggers : SPECIALISTE                                        */
/*==============================================================*/

DELIMITER |
DROP TRIGGER IF EXISTS creation_specialiste|
CREATE TRIGGER creation_specialiste
	BEFORE INSERT ON specialiste
	FOR EACH ROW
	BEGIN
		CALL verif_specialiste(new.specialiste_id);
        CALL verif_client(new.client_id);
	END|
DELIMITER ;

DELIMITER |
DROP TRIGGER IF EXISTS update_specialiste|
CREATE TRIGGER update_specialiste
	BEFORE UPDATE ON specialiste
	FOR EACH ROW
	BEGIN
		CALL verif_specialiste(new.specialiste_id);
        CALL verif_client(new.client_id);
	END|
DELIMITER ;

/*==============================================================*/
/* Tiggers : ACHAT                                              */
/*==============================================================*/

DELIMITER |
DROP TRIGGER IF EXISTS creation_achat|
CREATE TRIGGER creation_achat
	BEFORE INSERT ON achat
	FOR EACH ROW
	BEGIN
		IF NEW.achat_date > CURDATE() 
			THEN
				SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = "Date d'achat invalide";
        ELSE
			IF new.medecin_id IS NOT NULL
				THEN 
					CALL verif_medecin(new.medecin_id, new.client_id);
					CALL verif_client(new.client_id);
					IF new.specialiste_id IS NOT NULL
						THEN
							CALL verif_specialiste(new.specialiste_id);
					END IF;
			ELSE
				IF new.specialiste_id IS NOT NULL
					THEN 
						SIGNAL SQLSTATE '45000'
						SET MESSAGE_TEXT = "Le spécialiste ne peut pas apparaitre sans médecin";
				ELSE 
					CALL verif_client(new.client_id);
				END IF;
			END IF;
		END IF;
	END|
DELIMITER ;

DELIMITER |
DROP TRIGGER IF EXISTS update_achat|
CREATE TRIGGER update_achat
	BEFORE UPDATE ON achat
	FOR EACH ROW
	BEGIN
		IF new.medecin_id IS NOT NULL
			THEN 
				CALL verif_medecin(new.medecin_id, new.client_id);
                CALL verif_client(new.client_id);
                IF new.specialiste_id IS NOT NULL
					THEN
						CALL verif_specialiste(new.specialiste_id);
				END IF;
		ELSE
			IF new.specialiste_id IS NOT NULL
				THEN 
					SIGNAL SQLSTATE '45000'
					SET MESSAGE_TEXT = "Le spécialiste ne peut pas apparaitre sans médecin";
			ELSE 
				CALL verif_client(new.client_id);
			END IF;
		END IF;
	END|
DELIMITER ;

/*==============================================================*/
/* Tiggers : Medicament                                         */
/*==============================================================*/
DELIMITER |
CREATE TRIGGER verifier_date_mise_en_service
	BEFORE INSERT ON medicament
	FOR EACH ROW
	BEGIN
		IF NEW.MEDICAMENT_DATEMISESERVICE > CURDATE() 
			THEN
				SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = "Date de mise en service invalide";
		END IF;
	END |
DELIMITER ;
package test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import exceptions.EntreeException;
import metier.Adresse;
import metier.Mutuelle;

class TestMutuelle {

	static Adresse adresseABCD = null;

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		adresseABCD = new Adresse(0,"15","rue machin","12584","Ici");
	}

	@Test
	void testConstructeur() throws EntreeException {
		
		Mutuelle mutuelle = new Mutuelle(0,"ABCD", "abcd@arnaque.fr", "Doubs",
				"0365947815", 60, adresseABCD);
		assertEquals(0, mutuelle.getId());
		assertEquals("ABCD", mutuelle.getNom());
		assertEquals("abcd@arnaque.fr", mutuelle.getEmail());
		assertEquals("Doubs", mutuelle.getDepartement());
		assertEquals("0365947815", mutuelle.getNumeroTelephone());
		assertEquals(60, mutuelle.getTauxPriseEnCharge());
		assertEquals(adresseABCD, mutuelle.getAdresse());
	}
	
	@Test
	void testToString() throws EntreeException
	{
		Mutuelle mutuelle = new Mutuelle(0,"ABCD", "abcd@arnaque.fr", "Doubs",
				"0365947815", 60, adresseABCD);
		assert(mutuelle.toString().contains("ABCD"));
	}
	
	@Test
	void testErreurId() {
		
		try {
			Mutuelle mutuelle = new Mutuelle(-1,"ABCD", "abcd@arnaque.fr", "Doubs",
					"0365947815", 60, adresseABCD);
		} catch (EntreeException e) {
			assertEquals("un identifiant ne peut pas être négatif", e.getMessage() );
		}
		
		
	}
	
	@Test
	void testErreurSetNom_null() throws EntreeException {
		try {
			Mutuelle mutuelle = new Mutuelle(0,null, "abcd@arnaque.fr", "Doubs",
					"0365947815", 60, adresseABCD);
		} catch (EntreeException e) {
			assert (e.getMessage()
					.contains("Le nom de la mutuelle est trop petit"));
		}

	}
	
	@Test
	void testErreurSetNom_vide() throws EntreeException {
		try {
			Mutuelle mutuelle = new Mutuelle(0,"", "abcd@arnaque.fr", "Doubs",
					"0365947815", 60, adresseABCD);
		} catch (EntreeException e) {
			assert (e.getMessage()
					.contains("Le nom de la mutuelle est trop petit"));
		}
	}

	@Test
	void testErreurSetEmail_null() throws EntreeException {
		try {
			Mutuelle mutuelle = new Mutuelle(0,"ABCD", null, "Doubs",
					"0365947815", 60, adresseABCD);
		} catch (EntreeException e) {
			assert (e.getMessage().contains(
					"L'email ne doit pas être vide"));
		}
	}
	
	@Test
	void testErreurSetEmail_mauvaisFormat() throws EntreeException {
		try {
			Mutuelle mutuelle = new Mutuelle(0,"ABCD", "abcdarnaque.fr", "Doubs",
					"0365947815", 60, adresseABCD);
		} catch (EntreeException e) {
			assert (e.getMessage().contains(
					"L'email de la mutuelle doit être au format #@#.#"));
		}
	}

	@Test
	void testErreurSetDepartement_majuscule() throws EntreeException {
		try {
			Mutuelle mutuelle = new Mutuelle(0,"ABCD", "abcd@arnaque.fr", "doubs",
					"0365947815", 60, adresseABCD);
		} catch (EntreeException e) {
			assert (e.getMessage().contains(
					"Le Nom du département doit commencer pas par une majuscule"));
		}

	}
	@Test
	void testErreurSetDepartement_null() throws EntreeException {

		try {
			Mutuelle mutuelle = new Mutuelle(0,"ABCD", "abcd@arnaque.fr", null,
					"0365947815", 60, adresseABCD);
		} catch (EntreeException e) {
			assert (e.getMessage().contains("Aucun nom de département indiqué"));
		}

	}
	@Test
	void testErreurSetDepartement_vide() throws EntreeException {
		try {
			Mutuelle mutuelle = new Mutuelle(0,"ABCD", "abcd@arnaque.fr", "",
					"0365947815", 60, adresseABCD);
		} catch (EntreeException e) {
			assert (e.getMessage().contains("Aucun nom de département indiqué"));
		}
	}


	@Test
	void testErreurNumeroTelephone_mauvaisFormat() throws EntreeException {
		try {
			Mutuelle mutuelle = new Mutuelle(0,"ABCD", "abcd@arnaque.fr", "Doubs",
					"03659478", 60, adresseABCD);
		} catch (EntreeException e) {
			assert (e.getMessage().contains(
					"Le numéro de téléphone de la mutuelle doit contenir une suite de 10 chiffres commençant par 0"));
		}
	}
	@Test
	void testErreurNumeroTelephone_null() throws EntreeException {
		try {
			Mutuelle mutuelle = new Mutuelle(0,"ABCD", "abcd@arnaque.fr", "Doubs",
					null, 60, adresseABCD);
		} catch (EntreeException e) {
			assert (e.getMessage().contains(
					"Aucun numéro de téléphone ajouté"));
		}
	}
	@Test
	void testErreurNumeroTelephone_vide() throws EntreeException {
		try {
			Mutuelle mutuelle = new Mutuelle(0,"ABCD", "abcd@arnaque.fr", "Doubs",
					"", 60, adresseABCD);
		} catch (EntreeException e) {
			assert (e.getMessage().contains(
					"Aucun numéro de téléphone ajouté"));
		}
	}

	@Test
	void testErreurTauxDeRemboursement() throws EntreeException {
		try {
			Mutuelle mutuelle = new Mutuelle(0,"ABCD", "abcd@arnaque.fr", "Doubs",
					"0365947815", 160, adresseABCD);
		} catch (EntreeException e) {
			assert (e.getMessage().contains(
					"Le taux de prise en charge doit être compris entre 0 et 100"));
		}
	}

	@Test
	void testErreurTauxDeRemboursementTropGrand() throws EntreeException {
		try {
			Mutuelle mutuelle = new Mutuelle(0,"ABCD", "abcd@arnaque.fr", "Doubs",
					"0365947815", 160, adresseABCD);
		} catch (EntreeException e) {
			assert (e.getMessage().contains(
					"Le taux de prise en charge doit être compris entre 0 et 100"));
		}
	}

	@Test
	void testErreurTauxDeRemboursementTropPetit() throws EntreeException {
		try {
			Mutuelle mutuelle = new Mutuelle(0,"ABCD", "abcd@arnaque.fr", "Doubs",
					"0365947815", -60, adresseABCD);
		} catch (EntreeException e) {
			assert (e.getMessage().contains(
					"Le taux de prise en charge doit être compris entre 0 et 100"));
		}
	}
	
	@Test
	void testErreurAdresse() throws EntreeException {
		try {
			Mutuelle mutuelle = new Mutuelle(0,"ABCD", "abcd@arnaque.fr", "Doubs",
					"0365947815", 60, null);
		} catch (EntreeException e) {
			assert (e.getMessage().contains(
					"Aucune adresse ajouté"));
		}
	}

}

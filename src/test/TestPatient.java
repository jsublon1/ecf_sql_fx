package test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDate;
import java.util.ArrayList;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import exceptions.EntreeException;
import metier.Adresse;
import metier.Medecin;
import metier.Mutuelle;
import metier.Patient;
import metier.Specialiste;

class TestPatient {

	static Adresse adresseDrFoldingue;
	static Adresse adresseDrDorian;
	static Adresse adresseDrBones;
	static Adresse adresseDrHeart;
	static Adresse adresseEM;
	static Medecin foldingue;
	static Medecin dorian;
	static Specialiste bones;
	static Specialiste heart;
	static Mutuelle entubeur;
	static Adresse adresseRocky;

	@BeforeAll
	static void init() throws EntreeException {

		adresseRocky = new Adresse(0,"5", "chemin des marrons", "54000", "Nancy");
		adresseDrFoldingue = new Adresse(0,"15", "avenue des délires", "54000",
				"Nancy");
		adresseDrDorian = new Adresse(0,"75", "rue Sacré Coeur", "83990",
				"Saint-Tropez");
		adresseDrBones = new Adresse(0,"9", "boulevard squelette", "54000",
				"Nancy");
		adresseDrHeart = new Adresse(0,"3", "impasse des amoureux", "19000",
				"Tulle");
		adresseEM = new Adresse(0,"48", "rue qui n'existe pas", "23096",
				"Guéret");
		foldingue = new Medecin(0,"Foldingue", "Damien",
				"damien.foldingue@admdf.doc", "0345967412", adresseDrFoldingue,
				"41684J648I36157");
		bones = new Specialiste(0,"Bones", "Clara", "clara.bones@admdf.doc",
				"0345218763", adresseDrBones, "urologue");
		dorian = new Medecin(0,"Dorian", "John", "jdthedoc@apple.com",
				"0494326845", adresseDrDorian, "40000LP68490243");
		heart = new Specialiste(0,"Heart", "Love", "l.heart@admdf.doc",
				"0345658941", adresseDrHeart, "cardialogue");
		entubeur = new Mutuelle(0,"Entubeur Mutuelle", "no.reply@rien.com",
				"Creuse", "0000000000", 12, adresseEM);
	}


	@Test
	void testToString() throws EntreeException {
		Patient rocky = new Patient(0,"Balboa", "Rocky", "rocky.balboa@yahoo.fr",
				"0635679456", adresseRocky, LocalDate.parse("1970-02-15"),
				"170066835489615", foldingue, entubeur);
		assertEquals("Rocky Balboa", rocky.toString());
	}
	@Test
	void testToString_aucun() throws EntreeException {
		Patient rocky = new Patient();
		assertEquals("Aucun", rocky.toString());
	}

	@Test
	void testConstructeurPatient() throws EntreeException {
		Patient rocky = new Patient(0,"Balboa", "Rocky", "rocky.balboa@yahoo.fr",
				"0635679456", adresseRocky, LocalDate.parse("1970-02-15"),
				"170066835489615", foldingue, entubeur);
		assertEquals(0, rocky.getId());
		assertEquals("Balboa", rocky.getNom());
		assertEquals("Rocky", rocky.getPrenom());
		assertEquals("rocky.balboa@yahoo.fr", rocky.getEmail());
		assertEquals("0635679456", rocky.getNumeroTelephone());
		assertEquals(adresseRocky, rocky.getAdresse());
		assertEquals("1970-02-15", rocky.dateToString());
		assertEquals("170066835489615", rocky.getNumeroDeSecuriteSociale());
		assertEquals(foldingue, rocky.getDocteur());
		assertEquals(entubeur, rocky.getMutuelle());
	}
	
	@Test
	void testConstructeurPatient_listeSpecialiste() throws EntreeException
	{
		ArrayList<Specialiste> listeSpecialiste= new ArrayList<>();
		listeSpecialiste.add(bones);
		Patient rocky = new Patient(0,"Balboa", "Rocky", "rocky.balboa@yahoo.fr",
				"0635679456", adresseRocky, LocalDate.parse("1970-02-15"),
				"170066835489615", foldingue,
				listeSpecialiste, entubeur);
		assertEquals("Bones", rocky.getListeSpecialiste().get(0).getNom());
	}

	@Test
	void testErreurIDNegatif()
	{
		try {
			Patient rocky = new Patient(-1,"Balboa", "Rocky", "rocky.balboa@yahoo.fr",
					"0635679456", adresseRocky, LocalDate.parse("1970-02-15"),
					"170066835489615", foldingue, entubeur);
		} catch (EntreeException e) {
			assert(e.getMessage().contains("L'identifiant de peut pas être négatif"));
		}
	}
	@Test
	void testErreurConstructeurMajusculeNom() throws EntreeException {
		try {
			Patient rocky = new Patient(0,"balboa", "Rocky",
					"rocky.balboa@yahoo.fr", "0635679456", adresseRocky, LocalDate.parse("1970-02-15"), "170066835489615", foldingue,
					entubeur);
		} catch (EntreeException me) {
			assert (me.getMessage()
					.contains("Le Nom doit commencer pas par une majuscule"));
		}
	}

	@Test
	void testErreurConstructeurMajusculePrenom() throws EntreeException {
		try {
			Patient rocky = new Patient(0,"Balboa", "rocky",
					"rocky.balboa@yahoo.fr", "0635679456", adresseRocky, LocalDate.parse("1970-02-15"), "170066835489615", foldingue, entubeur);
		} catch (EntreeException me) {
			assert (me.getMessage().contains(
					"Le Prénom doit commencer pas par une majuscule"));
		}
	}

	@Test
	void testErreurConstructeur_nomNull() throws EntreeException {
		try {
			Patient rocky = new Patient(0,null, "Rocky", "rocky.balboa@yahoo.fr",
					"0635679456", adresseRocky, LocalDate.parse("1970-02-15"),
					"170066835489615", foldingue, entubeur);
		} catch (EntreeException me) {
			assert (me.getMessage().contains("Le nom entré est trop petit"));
		}
	}
	@Test
	void testErreurConstructeur_nomVide() throws EntreeException {
		try {
			Patient rocky = new Patient(0,"", "Rocky", "rocky.balboa@yahoo.fr",
					"0635679456", adresseRocky, LocalDate.parse("1970-02-15"),
					"170066835489615", foldingue, entubeur);
		} catch (EntreeException me) {
			assert (me.getMessage().contains("Le nom entré est trop petit"));
		}
	}

	@Test
	void testErreurConstructeurEmail_null() throws EntreeException {
		try {
			Patient rocky = new Patient(0,"Balboa", "Rocky", null, "0635679456",
					adresseRocky, LocalDate.parse("1970-02-15"), "170066835489615",
					foldingue, entubeur);

		} catch (EntreeException me) {
			assert (me.getMessage().contains("Aucun email indiqué"));
		}

	}
	@Test
	void testErreurConstructeurEmail_vide() throws EntreeException {

		try {
			Patient rocky = new Patient(0,"Balboa", "Rocky", "", "0635679456",
					adresseRocky, LocalDate.parse("1970-02-15"), "170066835489615",
					foldingue, entubeur);
		} catch (EntreeException me) {
			assert (me.getMessage().contains("Aucun email indiqué"));
		}

	}
	@Test
	void testErreurConstructeurEmail_mauvaisFormat() throws EntreeException {

		try {
			Patient rocky = new Patient(0,"Balboa", "Rocky",
					"rocky.balboa@yahoofr", "0635679456", adresseRocky, LocalDate.parse("1970-02-15"), "170066835489615", foldingue, entubeur);
		} catch (EntreeException me) {
			assert (me.getMessage()
					.contains("L'adresse Email doit être au format #@#.#"));
		}
	}

	@Test
	void testErreurConstructeur_prenomNull() throws EntreeException {
		try {
			Patient rocky = new Patient(0,"Balboa", null, "rocky.balboa@yahoo.fr",
					"0635679456", adresseRocky, LocalDate.parse("1970-02-15"),
					"170066835489615", foldingue, entubeur);
		} catch (EntreeException me) {
			assert (me.getMessage().contains("Le prénom entré est trop petit"));
		}

	}
	
	@Test
	void testErreurConstructeur_prenomVide() throws EntreeException {
		try {
			Patient rocky = new Patient(0,"Balboa", "", "rocky.balboa@yahoo.fr",
					"0635679456", adresseRocky, LocalDate.parse("1970-02-15"),
					"170066835489615", foldingue, entubeur);
		} catch (EntreeException me) {
			assert (me.getMessage().contains("Le prénom entré est trop petit"));
		}
	}

	@Test
	void testErreurNumeroTelephone_null() throws EntreeException {
		try {
			Patient rocky = new Patient(0,"Balboa", "Rocky",
					"rocky.balboa@yahoo.fr", null, adresseRocky, LocalDate.parse("1970-02-15"), "170066835489615", foldingue, entubeur);
		} catch (EntreeException me) {
			assert (me.getMessage()
					.contains("Aucun numéro de téléphone indiqué"));
		}

	}
	@Test
	void testErreurNumeroTelephone_vide() throws EntreeException {

		try {
			Patient rocky = new Patient(0,"Balboa", "Rocky",
					"rocky.balboa@yahoo.fr", "     ", adresseRocky, LocalDate.parse("1970-02-15"), "170066835489615", foldingue, entubeur);
		} catch (EntreeException me) {
			assert (me.getMessage()
					.contains("Aucun numéro de téléphone indiqué"));
		}

	}
	@Test
	void testErreurNumeroTelephone_mauvaisFormat() throws EntreeException {

		try {
			Patient rocky = new Patient(0,"Balboa", "Rocky",
					"rocky.balboa@yahoo.fr", "1635679456", adresseRocky, LocalDate.parse("1970-02-15"), "170066835489615", foldingue, entubeur);
		} catch (EntreeException me) {
			assert (me.getMessage().contains(
					"Le numéro de téléphone est une suite de 10 chiffres commençant par 0"));
		}
	}

	@Test
	void testErreurNumeroSecuriteSociale_null() throws EntreeException {
		try {
			Patient rocky = new Patient(0,"Balboa", "Rocky",
					"rocky.balboa@yahoo.fr", "0635679456", adresseRocky, LocalDate.parse("1970-02-15"), null, foldingue, entubeur);
		} catch (EntreeException me) {
			assert (me.getMessage()
					.contains("Aucun numero de sécurité sociale ajouté"));
		}
	}
	@Test
	void testErreurNumeroSecuriteSociale_vide() throws EntreeException {

		try {
			Patient rocky = new Patient(0,"Balboa", "Rocky",
					"rocky.balboa@yahoo.fr", "0635679456", adresseRocky, LocalDate.parse("1970-02-15"), "", foldingue, entubeur);
		} catch (EntreeException me) {
			assert (me.getMessage()
					.contains("Aucun numero de sécurité sociale ajouté"));
		}

	}
	@Test
	void testErreurNumeroSecuriteSociale_mauvaisFormat() throws EntreeException {

		try {
			Patient rocky = new Patient(0,"Balboa", "Rocky",
					"rocky.balboa@yahoo.fr", "0635679456", adresseRocky, LocalDate.parse("1970-02-15"), "1700668354896159", foldingue,
					entubeur);
		} catch (EntreeException me) {
			assert (me.getMessage().contains(
					"Le numero de securité sociale est une suite de 15 chiffres"));
		}
	}

	@Test
	void testSetDocteur() throws EntreeException {
		Patient rocky = new Patient(0,"Balboa", "Rocky", "rocky.balboa@yahoo.fr",
				"0635679456", adresseRocky, LocalDate.parse("1970-02-15"),
				"170066835489615", foldingue, entubeur);
		rocky.setDocteur(dorian);
		assertEquals(dorian, rocky.getDocteur());
	}

	@Test
	void testAjouterSpecialiste() throws EntreeException {
		Patient rocky = new Patient(0,"Balboa", "Rocky", "rocky.balboa@yahoo.fr",
				"0635679456", adresseRocky, LocalDate.parse("1970-02-15"),
				"170066835489615", foldingue, entubeur);
		rocky.ajouterSpecialiste(heart);
		assertEquals(heart.getNom(), rocky.getListeSpecialiste().get(0).getNom());
	}
	
	@Test
	void testErreurAjouterSpecialiste_null(){
		Patient rocky;
		try {
			rocky = new Patient(0,"Balboa", "Rocky", "rocky.balboa@yahoo.fr",
					"0635679456", adresseRocky, LocalDate.parse("1970-02-15"),
					"170066835489615", foldingue, entubeur);
			rocky.ajouterSpecialiste(null);
		} catch (EntreeException e) {
			assertEquals("Aucun spécialiste ajoutée", e.getMessage());
		}
		
		
	}

	@Test
	void testErreurMedecin() {
		try {
			Patient rocky = new Patient(0,"Balboa", "Rocky",
					"rocky.balboa@yahoo.fr", "0635679456", adresseRocky,LocalDate.parse("1970-02-15"), "170066835489615", null, entubeur);

		} catch (EntreeException e) {
			assert (e.getMessage().contains("Aucun médecin ajoutée"));
		}
	}

	@Test
	void testErreurMutuelle() {
		try {
			Patient rocky = new Patient(0,"Balboa", "Rocky",
					"rocky.balboa@yahoo.fr", "0635679456", adresseRocky, LocalDate.parse("1970-02-15"), "170066835489615", foldingue, null);

		} catch (EntreeException e) {
			assert (e.getMessage().contains("Aucune mutuelle ajoutée"));
		}
	}

	@Test
	void testErreurAdresse() {
		try {
			Patient rocky = new Patient(0,"Balboa", "Rocky",
					"rocky.balboa@yahoo.fr", "0635679456", null, LocalDate.parse("1970-02-15"), "170066835489615", foldingue, entubeur);
		} catch (EntreeException e) {
			assert (e.getMessage().contains("Aucune adresse indiqué"));
		}
	}
	@Test
	void testErreurDateNaissance_null() {
		try {
			Patient rocky = new Patient(0,"Balboa", "Rocky",
					"rocky.balboa@yahoo.fr", "0635679456", adresseRocky, null, "170066835489615", foldingue, entubeur);
		} catch (EntreeException e) {
			assert (e.getMessage().contains("Aucune date de Naissance renseignée"));
		}
	}
	@Test
	void testErreurDateNaissance_futur() {
		try {
			Patient rocky = new Patient(0,"Balboa", "Rocky",
					"rocky.balboa@yahoo.fr", "0635679456", adresseRocky, LocalDate.parse("2030-02-15"), "170066835489615", foldingue, entubeur);
		} catch (EntreeException e) {
			assert (e.getMessage().contains("La date de naissance est après aujourd'hui"));
		}
	}
}

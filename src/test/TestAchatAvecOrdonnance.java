package test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDate;
import java.util.ArrayList;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import exceptions.EntreeException;
import metier.AchatAvecOrdonnance;
import metier.Adresse;
import metier.Medecin;
import metier.Medicament;
import metier.Mutuelle;
import metier.Patient;
import metier.Specialiste;

class TestAchatAvecOrdonnance {

	static Patient rocky;
	static Medecin foldingue;
	static Specialiste bones;
	static Mutuelle mutuelle;
	static Medicament malex;
	static Adresse adresseRocky;
	static Adresse adresseDrFoldingue;
	static ArrayList<Medicament> listeMedicament = new ArrayList<Medicament>();

	@BeforeAll
	static void setUpBeforeClass() throws EntreeException {
		adresseRocky = new Adresse(0,"5", "chemin des marrons", "54000", "Nancy");
		adresseDrFoldingue = new Adresse(0,"15", "avenue des délires", "54000",
				"Nancy");
		mutuelle = new Mutuelle(0,"Entubeur Mutuelle", "no.reply@rien.com",
				"Creuse", "0000000000", 12, adresseDrFoldingue );
		foldingue = new Medecin(0,"Foldingue", "Damien",
				"damien.foldingue@admdf.doc", "0345967412", adresseDrFoldingue,
				"41684J648I36157" );
		bones = new Specialiste(0,"Bones", "Clara", "clara.bones@admdf.doc",
				"0345218763", adresseDrFoldingue, "urologue");
		rocky = new Patient(0,"Balboa", "Rocky", "rocky.balboa@yahoo.fr",
				"0635679456", adresseRocky, LocalDate.parse("1970-02-15"),
				"170066835489615", foldingue, mutuelle);
		rocky.ajouterSpecialiste(bones);
		malex = new Medicament(0,"Malex", ".analgesique", 4, 12, LocalDate.parse("1970-02-15"));
		listeMedicament.add(malex);
	}

	@Test
	void testToString() throws EntreeException {
		AchatAvecOrdonnance ordonnance = new AchatAvecOrdonnance(0,rocky,
				listeMedicament, foldingue, bones );
		assertEquals("Client: " + "Balboa" + " " + "Rocky"
				+ "\nMedecin traitant: Dr. " + "Foldingue" + "\nMedicaments:\n"
				+ malex.toString() + "\n" + "\nPrix à payer :" + 3.52 + "€",
				ordonnance.toString());
	}

	@Test
	void testConstructeur_AvecDate_sansSpecialiste() throws EntreeException
	{
		AchatAvecOrdonnance ordonnance = new AchatAvecOrdonnance(0, rocky, LocalDate.parse("2012-06-15"),
				listeMedicament, foldingue) ;
		assertEquals(LocalDate.parse("2012-06-15"), ordonnance.getDate());
		assertEquals("2012-06-15", ordonnance.dateToString());
	}
	@Test
	void testConstructeur_AvecDate_avecSpecialiste() throws EntreeException
	{
		AchatAvecOrdonnance ordonnance = new AchatAvecOrdonnance(0, rocky, LocalDate.parse("2012-06-15"),
				listeMedicament, foldingue,bones) ;
		assertEquals(bones, ordonnance.getSpecialiste());
	}
	@Test
	void testAchatAvecOrdonnanceAvecSpecialiste() throws EntreeException {
		AchatAvecOrdonnance ordonnance = new AchatAvecOrdonnance(0,rocky,
				listeMedicament, foldingue, bones );
		Medicament tueGrippe = new Medicament(0,"Tue Grippe",
				"antibiotique", 6, 45, LocalDate.parse("1970-02-15"));
		ordonnance.ajouterMedicament(tueGrippe);
		assertEquals(0, ordonnance.getId());
		assertEquals(rocky, ordonnance.getPatient());
		assertEquals(tueGrippe, ordonnance.getListeMedicament().get(1));
		assertEquals(foldingue, ordonnance.getDocteur());
		assertEquals(bones, ordonnance.getSpecialiste());
		assertEquals(8.8, ordonnance.getPrixTotal());
		assertEquals(LocalDate.now(), ordonnance.getDate());
		assertEquals(LocalDate.now().toString(), ordonnance.dateToString());
		assertEquals(true, ordonnance.isOrdonnance());
	}
	
	@Test
	void testSupprimerMedicament() throws EntreeException
	{
		AchatAvecOrdonnance ordonnance = new AchatAvecOrdonnance(0,rocky,
				listeMedicament, foldingue, bones );
		Medicament tueGrippe = new Medicament(0,"Tue Grippe",
				"antibiotique", 6, 45, LocalDate.parse("1970-02-15"));
		ordonnance.ajouterMedicament(tueGrippe);
		ordonnance.supprimerMedicament(malex);
		assertEquals(tueGrippe.getNom(), ordonnance.getListeMedicament().get(0).getNom());
	}
	@Test
	void testSupprimerAucunMedicament() throws EntreeException
	{
		AchatAvecOrdonnance ordonnance = new AchatAvecOrdonnance(0,rocky,
				listeMedicament, foldingue, bones );
		Medicament tueGrippe = new Medicament(0,"Tue Grippe",
				"antibiotique", 6, 45, LocalDate.parse("1970-02-15"));	
		ordonnance.supprimerMedicament(tueGrippe);
		assertEquals(1, ordonnance.getListeMedicament().size());

	}

	@Test
	void testAchatAvecOrdonnanceSansSpecialiste() throws EntreeException {
		AchatAvecOrdonnance ordonnance = new AchatAvecOrdonnance(0,rocky,
				listeMedicament, foldingue );
		assertEquals(rocky, ordonnance.getPatient());
		assertEquals(listeMedicament, ordonnance.getListeMedicament());
		assertEquals(foldingue, ordonnance.getDocteur());
		assertEquals(3.52, ordonnance.getPrixTotal());
	}

	@Test
	void testToStringSpecialiste() throws EntreeException {
		AchatAvecOrdonnance ordonnance = new AchatAvecOrdonnance(0,rocky,
				listeMedicament, foldingue, bones );
		assertEquals("Client: " + "Balboa" + " " + "Rocky"
				+ "\nMedecin traitant: Dr. " + "Foldingue" + "\nSpecialiste :"
				+ bones.toString() + "\nMedicaments:\n" + malex.toString()
				+ "\n" + "\nPrix à payer :" + 3.52 + "€",
				ordonnance.toString(bones));
	}

	@Test
	void testErreurMedecin() {
		try {
			AchatAvecOrdonnance ordonnance = new AchatAvecOrdonnance(0,rocky,
					listeMedicament, null );
		} catch (EntreeException e) {
			assert (e.getMessage()
					.contains("Le medecin traitant est obligatoire"));
		}
	}


	@Test
	void testAjouterMedicament() {
		try {
			AchatAvecOrdonnance ordonnance = new AchatAvecOrdonnance(0,rocky,
					listeMedicament, foldingue, bones );
			ordonnance.ajouterMedicament(null);
		} catch (EntreeException e) {
			assert (e.getMessage()
					.contains("Aucun médicament n'a été sélectionné"));
		}

	}

	@Test
	void testErreurPatient() {
		try {
			AchatAvecOrdonnance ordonnance = new AchatAvecOrdonnance(0,null,
					listeMedicament, foldingue );
		} catch (EntreeException e) {
			assert (e.getMessage().contains("Aucun client n'est ajouté"));
		}
	}

	@Test
	void testErreurListeMedicament_listeNull() {
		try {
			AchatAvecOrdonnance ordonnance = new AchatAvecOrdonnance(0,rocky,
					null, foldingue );
		} catch (EntreeException e) {
			assert (e.getMessage()
					.contains("L'achat ne comporte aucun médicament"));
		}
	}
	@Test
	void testErreurListeMedicament_listeVide() {
		try {
			AchatAvecOrdonnance ordonnance = new AchatAvecOrdonnance(0,rocky,
					new ArrayList<Medicament>(), foldingue );
		} catch (EntreeException e) {
			assert (e.getMessage()
					.contains("L'achat ne comporte aucun médicament"));
		}
	}
}

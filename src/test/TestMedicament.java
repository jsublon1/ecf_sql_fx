package test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDate;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import exceptions.EntreeException;
import metier.Medicament;

class TestMedicament {

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@Test
	void testMedicament() throws EntreeException {
		Medicament medicament = new Medicament(0,"Malex",
				"analgesique", 4, 12, LocalDate.parse("1970-02-15"));
		assertEquals(0, medicament.getId());
		assertEquals("Malex",medicament.getNom());
		assertEquals("analgesique",medicament.getCategorie());
		assertEquals(4,medicament.getPrix());
		assertEquals(12,medicament.getQuantite());
		assertEquals("1970-02-15",medicament.dateToString());
	}

	@Test
	void testErreurId() {
		try {
			Medicament medicament = new Medicament(-1,"Malex",
					"analgesique", 4, 12, LocalDate.parse("1970-02-15"));
		} catch (EntreeException e) {
			assert(e.getMessage().contains("un identifiant ne peut pas être négatif"));
		}
	}
	@Test
	void testErreurSetNomMajuscule() throws EntreeException {
		try {
			Medicament medicament = new Medicament(0,"malex",
					"analgesique", 4, 12, LocalDate.parse("1970-02-15"));

		} catch (EntreeException e) {
			assert(e.getMessage().contains("Le nom du médicament doit commencer pas par une majuscule"));
		} 
	}
	
	@Test
	void testErreurSetNomPetit() throws EntreeException {
		try {
			Medicament medicament = new Medicament(0,"",
					"analgesique", 4, 12, LocalDate.parse("1970-02-15"));
		} catch (EntreeException e) {
			assert(e.getMessage().contains("Le nom du médicament doit contenir au moins une lettre"));
		} 
	}

	@Test
	void testErreurSetCategorie() throws EntreeException {
		try
		{
			Medicament medicament = new Medicament(0,"Malex",
					null, 4, 12, LocalDate.parse("1970-02-15"));
		} catch (NullPointerException npe)
		{
			assert(npe.getMessage().contains("La catégorie du médicament ne peut être null"));
		}
	}


	@Test
	void testErreurSetPrix() throws EntreeException{
		try {
			Medicament medicament = new Medicament(0,"Malex",
					"analgesique", -4, 12, LocalDate.parse("1970-02-15"));
		} catch (EntreeException e) {
			assert(e.getMessage().contains("Le prix doit être supérieur à 0"));
		}
	}


	@Test
	void testErreurSetQuantite() throws EntreeException {
		try {
			Medicament medicament = new Medicament(0,"Malex",
					"analgesique", 4, -12, LocalDate.parse("1970-02-15"));
		} catch (EntreeException e) {
			assert(e.getMessage().contains("La quantité de médicament ne peut pas être inférieur à 0"));
		}
	}
	

	@Test
	void testToString() throws EntreeException {
		Medicament medicament = new Medicament(0,"Malex",
				"analgesique", 4, 12, LocalDate.parse("1970-02-15"));
		assertEquals("Malex : analgesique",medicament.toString());
	}

}

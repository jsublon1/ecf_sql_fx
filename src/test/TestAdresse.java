package test;



import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import exceptions.EntreeException;
import metier.Adresse;


class TestAdresse {


	@Test
	void testAdresse() throws EntreeException {
		Adresse adresse = new Adresse(0,"15","rue Jolie","61354","Une ville en France");
		assertEquals(0, adresse.getId());
		assertEquals("15",adresse.getNumeroRue());
		assertEquals("rue Jolie",adresse.getVoie());
		assertEquals("61354",adresse.getCodePostal());
		assertEquals("Une ville en France",adresse.getVille());
	}

	@Test
	void testErreurSetID_InferieurZero()
	{
		try {
			Adresse adresse = new Adresse(-1,"15","rue Jolie","61354","Une ville en France");
		} catch (EntreeException e) {
			assert(e.getMessage().contains("un identifiant ne peut pas être négatif"));
		}
	}
	@Test
	void testErreurSetNumeroRue_null() {
		try {
			Adresse adresse = new Adresse(0,null,"rue Jolie","61354","Une ville en France");
		} catch (EntreeException e)
		{
			assert(e.getMessage().contains("Aucun numéro de rue ajouté"));
		}
	}
	
	@Test
	void testErreurSetNumeroRue_tropGrand() {
		try {
			Adresse adresse = new Adresse(0,"1500","rue Jolie","61354","Une ville en France");
		} catch (EntreeException e)
		{
			assert(e.getMessage().contains("Le numéro de rue est un nombre d'un à trois chiffres"));
		}
		
	}

	@Test
	void testErreurSetCodePostal_tropPetit() {
		try {
			Adresse adresse = new Adresse(0,"15","rue Jolie","6354","Une ville en France");
		} catch (EntreeException e)
		{
			assert(e.getMessage().contains("Le code postal doit contenir 5 chiffres"));
		}

	}
	@Test
	void testErreurSetCodePostal_tropGrand() {
		try {
			Adresse adresse = new Adresse(0,"15","rue Jolie","635478","Une ville en France");
		} catch (EntreeException e)
		{
			assert(e.getMessage().contains("Le code postal doit contenir 5 chiffres"));
		}

	}
	@Test
	void testErreurSetCodePostal_null() {
		try {
			Adresse adresse = new Adresse(0,"15","rue Jolie",null,"Une ville en France");
		} catch (EntreeException e)
		{
			assert(e.getMessage().contains("Aucun code postal ajouté"));
		}
	}

	@Test
	void testErreurSetVoie_tropPetit(){
		try {
			Adresse adresse = new Adresse(0,"15","r","61354","Une ville en France");
		} catch (EntreeException e)
		{
			assert(e.getMessage().contains("Le nom de la rue doit contenir au moins 2 lettres et commencer par une lettre"));
		}
	}
	
	@Test
	void testErreurSetVoie_null(){
		try {
			Adresse adresse = new Adresse(0,"15",null,"61354","Une ville en France");
		} catch (EntreeException e)
		{
			assert(e.getMessage().contains("Aucune rue ajouté"));
		}
	}
	
	@Test
	void testErreurSetVille_Majuscule() {
		try {
			Adresse adresse = new Adresse(0,"15","rue Jolie","61354","une ville en France");
		} catch (EntreeException e)
		{
			assert(e.getMessage().contains("Le nom de la ville doit commencer pas par une majuscule"));
		}
	}
	
	@Test
	void testErreurSetVille_vide() {
		try {
			Adresse adresse = new Adresse(0,"15","rue Jolie","61354","");
		} catch (EntreeException e)
		{
			assert(e.getMessage().contains("Aucune ville ajouté"));
		}
	}
	
	@Test
	void testErreurSetVille_null() {
		try {
			Adresse adresse = new Adresse(0,"15","rue Jolie","61354",null);
		} catch (EntreeException e)
		{
			assert(e.getMessage().contains("Aucune ville ajouté"));
		}
	}

	
	@Test
	void testToString() throws EntreeException {
		Adresse adresse = new Adresse(0,"15","rue Jolie","61354","Une ville en France");
		assertEquals("15"+", "+"rue Jolie"+"\n"+"61354"+"  "+"Une ville en France",adresse.toString());
	}

}

package test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDate;
import java.util.ArrayList;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import exceptions.EntreeException;
import metier.AchatAvecOrdonnance;
import metier.AchatSansOrdonnance;
import metier.Adresse;
import metier.Medecin;
import metier.Medicament;
import metier.Mutuelle;
import metier.Patient;
import metier.Specialiste;

class TestMedecin {

	static Adresse adresseDrFoldingue;
	static Patient rocky;
	static Adresse adresseRocky;
	static Mutuelle entubeur;
	static Medicament malex;
	static ArrayList<Medicament> listeMedicament = new ArrayList<Medicament>();
	
	
	@BeforeAll
	static void init() throws EntreeException
	{
		adresseDrFoldingue = new Adresse(0,"15", "avenue des délires",
				"54000", "Nancy");
		adresseRocky = new Adresse(0,"5", "chemin des marrons",
				"54000", "Nancy");
		malex = new Medicament(0,"Malex",
				"analgesique", 4, 12, LocalDate.parse("1970-02-15"));
		listeMedicament.add(malex);
		entubeur = new Mutuelle(0,"Entubeur Mutuelle",
				"no.reply@rien.com", "Creuse", "0000000000", 12, adresseRocky);
		
	}
	@Test
	void testToString() throws EntreeException {
		
		Medecin foldingue = new Medecin(0,"Foldingue", "Damien",
				"damien.foldingue@admdf.doc", "0345967412",
				adresseDrFoldingue, "41684J648I36157");
		
		assertEquals("Dr. Foldingue",foldingue.toString());
	}
	@Test
	void testToString_medecinVide() throws EntreeException {
		
		Medecin foldingue = new Medecin();
		
		assertEquals("Aucun",foldingue.toString());
	}

	@Test
	void testNumeroAgrement() throws EntreeException {
		Medecin foldingue = new Medecin(0,"Foldingue", "Damien",
				"damien.foldingue@admdf.doc", "0345967412",
				adresseDrFoldingue, "41684J648I36157");
		assertEquals("41684J648I36157",foldingue.getNumeroAgrement());
	}
	
	@Test
	void testErreurNumeroAgrement_null() throws EntreeException {
		try {
			Medecin foldingue = new Medecin(0,"Foldingue", "Damien",
					"damien.foldingue@admdf.doc", "0345967412",
					adresseDrFoldingue, null);
		} catch (EntreeException e) {
			assert(e.getMessage().contains("Aucun numéro d'agrément indiqué"));
		}
	}
	@Test
	void testErreurNumeroAgrement_vide() throws EntreeException {
		try {
			Medecin foldingue = new Medecin(0,"Foldingue", "Damien",
					"damien.foldingue@admdf.doc", "0345967412",
					adresseDrFoldingue, "");
		} catch (EntreeException e) {
			assert(e.getMessage().contains("Aucun numéro d'agrément indiqué"));
		}
	}
	@Test
	void testErreurNumeroAgrement_mauvaisFormat() throws EntreeException {
		try {
			Medecin foldingue = new Medecin(0,"Foldingue", "Damien",
					"damien.foldingue@admdf.doc", "0345967412",
					adresseDrFoldingue, "684J648I361574");
		} catch (EntreeException e) {
			assert(e.getMessage().contains("Le numero d'agrement doit commencer par un 4 suivi de 14 caractères alphanumériques"));
		}
	}


	@Test
	void testGetListePatient() throws EntreeException {
		Medecin foldingue = new Medecin(0,"Foldingue", "Damien",
				"damien.foldingue@admdf.doc", "0345967412",
				adresseDrFoldingue, "41684J648I36157");
		rocky = new Patient(0,"Balboa", "Rocky",
				"rocky.balboa@yahoo.fr", "0635679456", adresseRocky,
				LocalDate.parse("1970-02-15"), "170066835489615", foldingue, entubeur);
		assertEquals(rocky,foldingue.getListePatient().get(0));
	}


	@Test
	void testGetListeOrdonnance() throws EntreeException {
		Medecin foldingue = new Medecin(0,"Foldingue", "Damien",
				"damien.foldingue@admdf.doc", "0345967412",
				adresseDrFoldingue, "41684J648I36157");
		Specialiste bones = new Specialiste(0,"Bones", "Clara",
				"clara.bones@admdf.doc", "0345218763", adresseDrFoldingue,
				"urologue");
		AchatAvecOrdonnance ordonnance = new AchatAvecOrdonnance(0,rocky,
				listeMedicament, foldingue,
				bones);
		assertEquals(ordonnance,foldingue.getListeOrdonnance().get(0));
	}
	
	@Test
	void testErreurAjout_patientNull()
	{
		try {
			Medecin foldingue = new Medecin(0,"Foldingue", "Damien",
					"damien.foldingue@admdf.doc", "0345967412",
					adresseDrFoldingue, "41684J648I36157");
			foldingue.ajouterPatient(null);
		} catch (EntreeException e) {
			assert(e.getMessage().contains("Aucun patient à ajouter"));
		}
	}
	
	@Test
	void testErreurAjout_ordonnanceNull()
	{

		try {
			Medecin foldingue = new Medecin(0,"Foldingue", "Damien",
					"damien.foldingue@admdf.doc", "0345967412",
					adresseDrFoldingue, "41684J648I36157");
			foldingue.ajouterOrdonnance(null);
		} catch (EntreeException e) {
			assert(e.getMessage().contains("Aucune ordonnance à ajouter"));
		}
	}


}

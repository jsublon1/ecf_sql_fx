package test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDate;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import exceptions.EntreeException;
import metier.Adresse;
import metier.Medecin;
import metier.Mutuelle;
import metier.Patient;
import metier.Specialiste;

class TestSpecialiste {

	static Adresse adresseDrBones;

	@BeforeAll
	static void init() throws EntreeException
	{
		adresseDrBones = new Adresse(0,"9", "boulevard squelette",
				"54000", "Nancy");
	}
	@Test
	void testToString() throws EntreeException {
		Specialiste bones = new Specialiste(0,"Bones", "Clara",
				"clara.bones@admdf.doc", "0345218763", adresseDrBones,
				"urologue");
		assertEquals("Dr. Bones",bones.toString());
	}
	@Test
	void testToString_vide() throws EntreeException {
		Specialiste bones = new Specialiste();
		assertEquals("Aucun",bones.toString());
	}

	@Test
	void testConstructeurSpecialiste() throws EntreeException {
		Specialiste bones = new Specialiste(0,"Bones", "Clara",
				"clara.bones@admdf.doc", "0345218763", adresseDrBones,
				"urologue");
		assertEquals("urologue",bones.getSpecialite());
	}

	
	@Test
	void testSpecialisteVide() throws EntreeException {
		Specialiste aucun = new Specialiste();
		assertEquals(null,aucun.getNom());
	}

	
	@SuppressWarnings("unused")
	@Test
	void testErreurSetSpecialite() throws EntreeException {
		try {
			Specialiste bones = new Specialiste(0,"Bones", "Clara",
					"clara.bones@admdf.doc", "0345218763", adresseDrBones,
					null);

		} catch (NullPointerException e)
		{
			assert(e.getMessage().contains("Une specialité ne peut être null"));
		}
		
	}
	
	@Test
	void testAjouterPatient() throws EntreeException
	{
		
		Medecin foldingue = new Medecin(0,"Foldingue", "Damien",
				"damien.foldingue@admdf.doc", "0345967412", adresseDrBones,
				"41684J648I36157");
		Mutuelle entubeur = new Mutuelle(0,"Entubeur Mutuelle", "no.reply@rien.com",
				"Creuse", "0000000000", 12, adresseDrBones);
		Specialiste bones = new Specialiste(0,"Bones", "Clara",
				"clara.bones@admdf.doc", "0345218763", adresseDrBones,
				"urologue");
		Patient rocky = new Patient(0,"Balboa", "Rocky", "rocky.balboa@yahoo.fr",
				"0635679456", adresseDrBones, LocalDate.parse("1970-02-15"),
				"170066835489615", foldingue, entubeur);
		bones.ajouterPatient(rocky);
		assertEquals(rocky, bones.getListePatient().get(0));
	}

}

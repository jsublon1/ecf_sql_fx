package test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDate;
import java.util.ArrayList;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import exceptions.EntreeException;
import metier.AchatSansOrdonnance;
import metier.Adresse;
import metier.Medecin;
import metier.Medicament;
import metier.Mutuelle;
import metier.Patient;
import metier.Specialiste;

class TestAchatSansOrdonnance {

	static Patient rocky;
	static Medecin foldingue;
	static Specialiste bones;
	static Mutuelle mutuelle;
	static Medicament malex;
	static Adresse adresseRocky;
	static ArrayList<Medicament> listeMedicament = new ArrayList<Medicament>();

	@BeforeAll
	static void setUpBeforeClass() throws EntreeException {

		adresseRocky = new Adresse(0,"5", "chemin des marrons", "54000", "Nancy");
		mutuelle = new Mutuelle(0,"Entubeur Mutuelle", "no.reply@rien.com",
				"Creuse", "0000000000", 12, adresseRocky);
		foldingue = new Medecin(0,"Foldingue", "Damien",
				"damien.foldingue@admdf.doc", "0345967412", adresseRocky,
				"41684J648I36157");
		rocky = new Patient(0,"Balboa", "Rocky", "rocky.balboa@yahoo.fr",
				"0635679456", adresseRocky, LocalDate.parse("1970-02-15"),
				"170066835489615", foldingue, mutuelle);
		malex = new Medicament(0,"Malex", "analgesique", 4, 12, LocalDate.parse("1970-02-15"));
		listeMedicament.add(malex);
	}

	@Test
	void testToString() throws EntreeException {
		AchatSansOrdonnance achat = new AchatSansOrdonnance(0,rocky,
				listeMedicament);
		assertEquals("Client: " + "Balboa" + " " + "Rocky" + "\nMedicaments:\n"
				+ malex.toString() + "\n" + "\nPrix à payer :" + 4.0 + "€",
				achat.toString());
	}

	@Test
	void testAchatSansOrdonnance_sansDate() throws EntreeException {
		AchatSansOrdonnance achat = new AchatSansOrdonnance(0,rocky,
				listeMedicament);
		assertEquals(rocky, achat.getPatient());
		assertEquals(listeMedicament, achat.getListeMedicament());
		assertEquals(4, achat.getPrixTotal());
		assertEquals(LocalDate.now().toString(), achat.dateToString());
		
	}
	
	@Test
	void testAchatSansOrdonnance_avecDate() throws EntreeException
	{
		AchatSansOrdonnance achatAnterieur = new AchatSansOrdonnance(0,rocky,
				LocalDate.parse("2022-08-25"), listeMedicament);
		assertEquals("2022-08-25", achatAnterieur.dateToString());
	}
	
	@Test
	void testIdentifiantNegatif()
	{
		try {
			AchatSansOrdonnance achatAnterieur = new AchatSansOrdonnance(-1,rocky,
					LocalDate.parse("2022-08-25"), listeMedicament);
		} catch (EntreeException e) {
			assertEquals("un identifiant ne peut pas être négatif", e.getMessage());
		}
	}
	
	@Test
	void testGetMedecin() throws EntreeException {
		AchatSansOrdonnance achat = new AchatSansOrdonnance(0,rocky,
				listeMedicament);
		assertEquals(null, achat.getDocteur().getNom());
	}
	
	@Test
	void testGetSpecialiste() throws EntreeException {
		AchatSansOrdonnance achat = new AchatSansOrdonnance(0,rocky,
				listeMedicament);
		assertEquals(null, achat.getSpecialiste().getNom());
	}
	

}

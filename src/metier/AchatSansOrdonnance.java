package metier;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;

import exceptions.EntreeException;

/**
 * Classe fille achat sans ordonnance qui herite de la classe Achat
 * 
 * @author JSublon
 *
 */
public class AchatSansOrdonnance extends Achat implements Serializable{


	/**
	 * 
	 */
	private static final long serialVersionUID = -2452820198973852978L;

	/**
	 * Constructeur avec patient et liste de medicaments
	 * @param pId: identifiant de l'achat sans ordonnance
	 * @param pPatient:         client
	 * @param pListeMedicament: liste des medicaments
	 * @throws EntreeException : erreur de saisie
	 */
	public AchatSansOrdonnance(int pId, Patient pPatient,
			ArrayList<Medicament> pListeMedicament)
			throws EntreeException {
		super(pId,pPatient, pListeMedicament);
		this.setOrdonnance(false);
		this.setPrixTotal();
	}

	/**
	 * Constructeur d'un achat sans ordonnance ou la date doit etre rentre
	 * manuellement
	 * @param pId              : identifiant
	 * @param pPatient         : client
	 * @param pDate				: date de l'achat
	 * @param pListeMedicament : liste des medicaments achetes
	 * @throws EntreeException : erreur sur la saisie
	 */
	public AchatSansOrdonnance(int pId, Patient pPatient, LocalDate pDate, ArrayList<Medicament> pListeMedicament) throws EntreeException {
		super(pId, pPatient, pDate, pListeMedicament);
		this.setPrixTotal();
		this.setOrdonnance(false);

	}

	/**
	 * Methode pour calculer le prix total d'un achat sans ordonnance
	 */
	public double calculerPrixTotal() {
		double prix = 0;
		for (Medicament medicament : this.getListeMedicament()) {
			prix = prix + medicament.getPrix();
		}
		return prix;
	}

	/**
	 * Methode qui retourne un String
	 */
	public String toString() {
		String temp = "";
		for (Medicament medicament : this.getListeMedicament()) {
			temp = temp + medicament + "\n";
		}
		return "Client: " + this.getPatient().getNom() + " "
				+ this.getPatient().getPrenom() + "\nMedicaments:\n" + temp
				+ "\nPrix à payer :" + this.getPrixTotal() + "€";
	}
	
	/**
	 * getter du medecin vide
	 * @return medecin
	 */
	public Medecin getDocteur()
	{
		Medecin doc = new Medecin();
		return doc;
	}
	
	/**
	 * getter du specialiste vide
	 * @return specialiste
	 */
	public Specialiste getSpecialiste()
	{
		Specialiste doc = new Specialiste();
		return doc;
	}
}

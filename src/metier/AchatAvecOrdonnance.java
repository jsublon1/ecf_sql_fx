package metier;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;

import exceptions.EntreeException;

/**
 * Classe fille d'achat on y ajoute un medecin traitant et peut-être un
 * specialiste
 * 
 * @author JSublon
 *
 */
public class AchatAvecOrdonnance extends Achat implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8411887006664441172L;
	/**
	 * medecin qui a ecrit l'ordonnance
	 */
	private Medecin docteur;
	/**
	 * specialiste qui a recommande l'ordonnance
	 */
	private Specialiste specialiste;

	/**
	 * Constructeur avec patient, medecin, specialiste et la liste des
	 * medicaments
	 * @param pId : identifiant de l'achat avec ordonnance
	 * @param pPatient:         client
	 * @param pListeMedicament: liste des medicaments
	 * @param pDocteur:         medecin traitant
	 * @param pSpecialiste:     specialiste
	 * @throws EntreeException : erreur de saisie
	 */
	public AchatAvecOrdonnance(int pId,Patient pPatient,
			ArrayList<Medicament> pListeMedicament, Medecin pDocteur,
			Specialiste pSpecialiste)
					throws EntreeException {
		super(pId, pPatient, pListeMedicament);
		this.setOrdonnance(true);
		this.setDocteur(pDocteur);
		this.setSpecialiste(pSpecialiste);
		this.setPrixTotal();
		pDocteur.ajouterOrdonnance(this);

	}

	/**
	 * Constructeur avec patient, medecin et la liste des medicament
	 * @param pId: identifiant de l'achat avec ordonnance
	 * @param pPatient:         client
	 * @param pListeMedicament: liste des medicaments
	 * @param pDocteur:         medecin traitant
	 * @throws EntreeException : erreur de saisie
	 */
	public AchatAvecOrdonnance(int pId, Patient pPatient,
			ArrayList<Medicament> pListeMedicament, Medecin pDocteur) 
					throws EntreeException {
		super(pId, pPatient, pListeMedicament);
		this.setOrdonnance(true);
		this.setDocteur(pDocteur);
		this.setPrixTotal();
		pDocteur.ajouterOrdonnance(this);

	}
	/**
	 * Constructeur avec patient, medecin, specialiste et la liste des
	 * medicaments
	 * @param pId : identifiant de l'achat avec ordonnance
	 * @param pPatient:         client
	 * @param pDate : date de l'achat
	 * @param pListeMedicament: liste des medicaments
	 * @param pDocteur:         medecin traitant
	 * @param pSpecialiste:     specialiste
	 * @throws EntreeException : erreur de saisie
	 */
	public AchatAvecOrdonnance(int pId,Patient pPatient, LocalDate pDate,
			ArrayList<Medicament> pListeMedicament, Medecin pDocteur,
			Specialiste pSpecialiste)
					throws EntreeException {
		super(pId, pPatient, pDate, pListeMedicament);
		this.setOrdonnance(true);
		this.setDocteur(pDocteur);
		this.setSpecialiste(pSpecialiste);
		this.setPrixTotal();
		pDocteur.ajouterOrdonnance(this);

	}

	/**
	 * Constructeur avec patient, medecin et la liste des medicament
	 * @param pId: identifiant de l'achat avec ordonnance
	 * @param pPatient:         client
	 * @param pDate : date de l'achat
	 * @param pListeMedicament: liste des medicaments
	 * @param pDocteur:         medecin traitant
	 * @throws EntreeException : erreur de saisie
	 */
	public AchatAvecOrdonnance(int pId, Patient pPatient, LocalDate pDate,
			ArrayList<Medicament> pListeMedicament, Medecin pDocteur) 
					throws EntreeException {
		super(pId, pPatient, pDate, pListeMedicament);
		this.setOrdonnance(true);
		this.setDocteur(pDocteur);
		this.setPrixTotal();
		pDocteur.ajouterOrdonnance(this);

	}
	/**
	 * @return docteur: medecin traitant
	 */
	public Medecin getDocteur() {
		return docteur;
	}

	/**
	 * @param pDocteur: medecin traitant
	 * @throws EntreeException : erreur de saisie
	 */
	public void setDocteur(Medecin pDocteur) throws EntreeException {
		if (pDocteur == null)
			throw new EntreeException("Le medecin traitant est obligatoire");
		this.docteur = pDocteur;
	}

	/**
	 * @return specialiste: specialiste
	 */
	public Specialiste getSpecialiste() {
		return specialiste;
	}

	/**
	 * @param pSpecialiste: specialiste
	 * 
	 */
	public void setSpecialiste(Specialiste pSpecialiste){
		this.specialiste = pSpecialiste;
	}

	/**
	 * Methode pour calculer le prix total d'un achat avec ordonnance
	 * 
	 * @throws EntreeException : erreur de liste de médicament
	 */
	public double calculerPrixTotal() throws EntreeException {
		double prix = 0;

		for (Medicament medicament : this.getListeMedicament()) {
			prix = prix + medicament.getPrix();
		}
		return prix
				* (100 - this.getPatient().getMutuelle().getTauxPriseEnCharge())
				/ 100;

	}

	/**
	 * Methode qui retourne un String
	 * 
	 * @param pSpecialiste: specialiste
	 * @return chaine de caratcere qui decrit l'achat
	 */
	public String toString(Specialiste pSpecialiste) {
		String temp = "";
		for (Medicament medicament : this.getListeMedicament()) {
			temp = temp + medicament + "\n";
		}
		return "Client: " + this.getPatient().getNom() + " "
		+ this.getPatient().getPrenom() + "\nMedecin traitant: Dr. "
		+ this.getDocteur().getNom() + "\nSpecialiste :"
		+ this.getSpecialiste().toString() + "\nMedicaments:\n" + temp
		+ "\nPrix à payer :" + this.getPrixTotal() + "€";
	}

	/**
	 * Methode qui retourne un String
	 */
	public String toString() {
		String temp = "";
		for (Medicament medicament : this.getListeMedicament()) {
			temp = temp + medicament + "\n";
		}
		return "Client: " + this.getPatient().getNom() + " "
		+ this.getPatient().getPrenom() + "\nMedecin traitant: Dr. "
		+ this.getDocteur().getNom() + "\nMedicaments:\n" + temp
		+ "\nPrix à payer :" + this.getPrixTotal() + "€";
	}
}

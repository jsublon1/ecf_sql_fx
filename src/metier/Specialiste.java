package metier;

import java.io.Serializable;
import java.util.ArrayList;

import exceptions.EntreeException;

/**
 * Classe fille Specialiste de la classe Mere Personne
 * 
 * @author JSublon
 *
 */
public class Specialiste extends Personne implements Serializable{


	/**
	 * 
	 */
	private static final long serialVersionUID = -3209529419535119508L;
	// Attributs de la classe
	/**
	 * specialite du specialiste
	 */
	private String specialite;
	/**
	 * liste des patients du specialiste
	 */
	private ArrayList<Patient> listePatient = new ArrayList<Patient>();

	/**
	 * Constructeur avec tous les attributs
	 * @param pId : 			identifiant du specialiste
	 * @param pNom:             nom du specialiste
	 * @param pPrenom:          prenom du specialiste
	 * @param pEmail:           email du specialiste
	 * @param pNumeroTelephone: numero de telephone du specialiste
	 * @param pAdresse:         adresse du specialiste
	 * @param pSpecialite:      specialite medicale du specialiste
	 * @throws EntreeException : erreur de saisie
	 * 
	 */
	public Specialiste(int pId, String pNom, String pPrenom, String pEmail,
			String pNumeroTelephone, Adresse pAdresse, String pSpecialite)
					throws EntreeException {
		super(pId, pNom, pPrenom, pEmail, pNumeroTelephone, pAdresse);
		this.setSpecialite(pSpecialite);
	}

	/**
	 * constructeur vide
	 */
	public Specialiste() 
	{
	}


	/**
	 * @return specialite: specialite medicale du specialiste
	 */
	public String getSpecialite() {
		return specialite;
	}

	/**
	 * @param pSpecialite: specialite medicale du specialiste
	 */
	protected void setSpecialite(String pSpecialite) {
		if (pSpecialite == null)
			throw new NullPointerException("Une specialité ne peut être null");
		this.specialite = pSpecialite;
	}

	/**
	 * Methode pour ajouter un patient au specialiste
	 * 
	 * @param pPatient: patient du specialiste
	 */
	public void ajouterPatient(Patient pPatient) {
		this.listePatient.add(pPatient);
	}
	
	/**
	 * getter de la liste des patients
	 * @return liste des patients
	 */
	public ArrayList<Patient> getListePatient()
	{
		return this.listePatient;
	}

	@Override
	/**
	 * Methode pour creer une chaine de caractere avec les informations du
	 * specialiste
	 */
	public String toString() {
		if (this.getNom()==null) return "Aucun";
		else return "Dr. " + this.getNom();
	}

}

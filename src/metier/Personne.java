package metier;

import java.io.Serializable;
import java.util.regex.Pattern;

import exceptions.EntreeException;

/**
 * Classe abstraite Personne
 * 
 * @author JSublon
 *
 */
public abstract class Personne implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5436052191661812767L;
	/**
	 * identifiant de la personne
	 */
	private int id;
	/**
	 * nom de la personne
	 */
	private String nom;
	/**
	 * prenom de la personne
	 */
	private String prenom;
	/**
	 * email de la personne
	 */
	private String email;
	/**
	 * numero de telephone de la personne
	 */
	private String numeroTelephone;
	/**
	 * adresse de la personne
	 */
	private Adresse adresse;

	/**
	 * Constructeur de l'objet avec tous les attributs
	 * @param pId: identifiant de la personne
	 * @param pNom:             nom de la personne
	 * @param pPrenom:          prenom de la personne
	 * @param pEmail:           email de la personne
	 * @param pNumeroTelephone: numero de telephone de la personne
	 * @param pAdresse:         adresse de la personne
	 * @throws EntreeException : erreur de saisie
	 */
	Personne(int pId, String pNom, String pPrenom, String pEmail,
			String pNumeroTelephone, Adresse pAdresse) throws EntreeException

	{
		this.setId(pId);
		this.setNom(pNom);
		this.setPrenom(pPrenom);
		this.setEmail(pEmail);
		this.setNumeroTelephone(pNumeroTelephone);
		this.setAdresse(pAdresse);
	}

	/**
	 * Constructeur vide pour pouvoir créer un spécialiste vide
	 */
	Personne() {

	}
	/**
	 * 
	 * @return id: identifiant de la personne
	 */
	public int getId()
	{
		return id;
	}
	
	/**
	 * setter de l'identifiant
	 * @param pId : identifiant de la personne
	 * @throws EntreeException : erreur sur la saisie
	 */
	public void setId(int pId) throws EntreeException
	{
		if (pId < 0) throw new EntreeException("L'identifiant de peut pas être négatif");
		this.id = pId;
	}
	
	/**
	 * @return nom: nom de la personne
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * @param pNom nom de la personne
	 * @throws EntreeException : erreur sur le format du nom
	 */
	public void setNom(String pNom) throws EntreeException {
		if (pNom == null || pNom.trim().isEmpty())
			throw new EntreeException("Le nom entré est trop petit");
		if (!pNom.substring(0, 1).toUpperCase().equals(pNom.substring(0, 1)))
			throw new EntreeException(
					"Le Nom doit commencer pas par une majuscule");
		this.nom = pNom;
	}

	/**
	 * @return prenom: prenom de la personne
	 */
	public String getPrenom() {
		return prenom;
	}

	/**
	 * @param pPrenom: prenom de la personne
	 * @throws EntreeException : erreur sur le format du prenom
	 */
	public void setPrenom(String pPrenom) throws EntreeException {
		if (pPrenom == null || pPrenom.trim().isEmpty())
			throw new EntreeException("Le prénom entré est trop petit");
		if (!pPrenom.substring(0, 1).toUpperCase()
				.equals(pPrenom.substring(0, 1)))
			throw new EntreeException(
					"Le Prénom doit commencer pas par une majuscule");
		this.prenom = pPrenom;
	}

	/**
	 * @return email: email de la personne
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param pEmail: email de la personne
	 * @throws EntreeException : erreur sur le format de l'adresse mail
	 */
	public void setEmail(String pEmail) throws EntreeException {
		if (pEmail == null || pEmail.trim().isEmpty())
			throw new EntreeException("Aucun email indiqué");
		if (!Pattern.matches(".+@.+\\.[a-z]+", pEmail))
			throw new EntreeException(
					"L'adresse Email doit être au format #@#.#");
		this.email = pEmail;
	}

	/**
	 * @return numeroTelephone: numero de telephone de la personne
	 */
	public String getNumeroTelephone() {
		return numeroTelephone;
	}

	/**
	 * @param pNumeroTelephone: numero de telephone de la personne
	 * @throws EntreeException : erreur sur le format du numero de telephone
	 */
	public void setNumeroTelephone(String pNumeroTelephone)
			throws EntreeException {
		if (pNumeroTelephone == null || pNumeroTelephone.trim().isEmpty())
			throw new EntreeException("Aucun numéro de téléphone indiqué");
		if (!Pattern.matches("0\\d{9}", pNumeroTelephone))
			throw new EntreeException(
					"Le numéro de téléphone est une suite de 10 chiffres commençant par 0");
		this.numeroTelephone = pNumeroTelephone;

	}

	/**
	 * @return adresse: adresse de la personne
	 */
	public Adresse getAdresse() {
		return adresse;
	}

	/**
	 * @param pAdresse: adresse de la personne
	 * @throws EntreeException : erreur adresse null
	 */
	public void setAdresse(Adresse pAdresse) throws EntreeException {
		if (pAdresse == null)
			throw new EntreeException("Aucune adresse indiqué");
		this.adresse = pAdresse;
	}

	/**
	 * methode abstraite de transformation en String
	 */
	public abstract String toString();
}

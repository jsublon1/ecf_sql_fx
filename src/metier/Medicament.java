package metier;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.regex.Pattern;

import exceptions.EntreeException;

/**
 * Classe Objet Medicament
 * 
 * @author JSublon
 *
 */
public class Medicament implements Serializable{


	/**
	 * 
	 */
	private static final long serialVersionUID = 2649865399689927868L;
	// Attributs de la classe
	/**
	 * identifiant du medicament
	 */
	private int id;
	/**
	 * nom du medicament
	 */
	private String nom;
	/**
	 * categorie du medicament
	 */
	private String categorie;
	/**
	 * prix du medicament
	 */
	private double prix;
	/**
	 * quantite en stock du medicament
	 */
	private int quantite;
	/**
	 * date de mise en service du medicament
	 */
	private LocalDate dateMiseEnService;


	/**
	 * Constructeur avec LocalDate
	 * @param pId : 		identifiant
	 * @param pNom :       nom du medicament
	 * @param pCategorie : categorie du medecament
	 * @param pPrix :      prix du medicament
	 * @param pQuantite :  quantite en stock
	 * @param pDate : date de mise en service
	 * @throws EntreeException : erreur de saisie
	 */
	public Medicament(int pId, String pNom, String pCategorie, double pPrix,
			int pQuantite, LocalDate pDate) 
					throws EntreeException {
		this.setId(pId);
		this.setNom(pNom);
		this.setCategorie(pCategorie);
		this.setPrix(pPrix);
		this.setQuantite(pQuantite);
		this.setDateMiseEnService(pDate);
	}
	
	/**
	 * 
	 * @return id: identifiant
	 */
	public int getId() {
		return id;
	}

	/**
	 * 
	 * @param pId: identifiant
	 * @throws EntreeException : erreur identifiant
	 */
	public void setId(int pId) throws EntreeException {
		if (pId < 0) throw new EntreeException("un identifiant ne peut pas être négatif");
		this.id = pId;
	}

	/**
	 * @return nom: nom du medicament
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * @param pNom: nom du medicament
	 */
	protected void setNom(String pNom) throws EntreeException {
		if (!Pattern.matches("^^[a-zA-Z]+(?:[\\s-][a-zA-Z0-9]+)*$", pNom))
			throw new EntreeException(
					"Le nom du médicament doit contenir au moins une lettre");
		if (!pNom.substring(0, 1).toUpperCase().equals(pNom.substring(0, 1)))
			throw new EntreeException(
					"Le nom du médicament doit commencer pas par une majuscule");
		this.nom = pNom;
	}

	/**
	 * @return categorie: categorie du medicament
	 */
	public String getCategorie() {
		return categorie;
	}

	/**
	 * @param pCategorie: categorie du medicament
	 */
	protected void setCategorie(String pCategorie) {
		if (pCategorie == null)
			throw new NullPointerException(
					"La catégorie du médicament ne peut être null");
		this.categorie = pCategorie;
	}

	/**
	 * @return prix: prix du medicament
	 */
	public double getPrix() {
		return prix;
	}

	/**
	 * @param pPrix: prix du medicament
	 */
	protected void setPrix(double pPrix) throws EntreeException {
		if (pPrix < 0)
			throw new EntreeException("Le prix doit être supérieur à 0");
		this.prix = pPrix;
	}

	/**
	 * @return quantite: quantite en stock
	 */
	public int getQuantite() {
		return quantite;
	}

	/**
	 * setter de la quantite de medicament
	 * @param pQuantite: quantite en stock
	 * @throws EntreeException : erreur de saisie
	 */
	public void setQuantite(int pQuantite) throws EntreeException {
		if (pQuantite < 0)
			throw new EntreeException(
					"La quantité de médicament ne peut pas être inférieur à 0");
		this.quantite = pQuantite;
	}

	/**
	 * @return dateMiseEnService: date de mise en service du medicament
	 */
	public LocalDate getDateMiseEnService() {
		return dateMiseEnService;
	}
	


	/**
	 * Methode pour afficher la date sous forme de chaine de caractere
	 * 
	 * @return chaine de caractere
	 */
	public String dateToString() {
		DateTimeFormatter formatDate = DateTimeFormatter
				.ofPattern("yyyy-MM-dd");
		return this.getDateMiseEnService().format(formatDate);
	}


	/**
	 * Setter date avec LocalDate
	 * @param pDate: date de mise en service
	 * @throws EntreeException : erreur saisie
	 */
	protected void setDateMiseEnService(LocalDate pDate) throws EntreeException {
		if(pDate.isAfter(LocalDate.now())) throw new EntreeException("La date de mise en service est dans le futur");
		this.dateMiseEnService = pDate;
	}
	
	/**
	 * Methode pour obtenir les information du medicament en chaine de caractere
	 */
	public String toString() {
		return this.getNom() + " : " + this.getCategorie();
	}
}

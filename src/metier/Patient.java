package metier;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.regex.Pattern;

import exceptions.EntreeException;

/**
 * Objet fille Patient qui herite de la classe Personne
 * 
 * @author JSublon
 *
 */
public class Patient extends Personne implements Serializable{


	/**
	 * 
	 */
	private static final long serialVersionUID = -3029723727186392396L;
	// Attributs de l'objet
	/**
	 * date de naissance du patient
	 */
	private LocalDate dateDeNaissance;
	/**
	 * numero de securite sociale du patient
	 */
	private String numeroDeSecuriteSociale;
	/**
	 * medecin traitant du patient
	 */
	private Medecin docteur;
	/**
	 * liste des specialistes du patient
	 */
	private ArrayList<Specialiste> listeSpecialiste = new ArrayList<Specialiste>();
	/**
	 * mutuelle du patient
	 */
	private Mutuelle mutuelle;

	/**
	 * Constructeur vide
	 */
	public Patient() {};
	
	/**
	 * Constructeur avec tous les attributs
	 * @param pId : 					identifiant du patient
	 * @param pNom:                     nom du patient
	 * @param pPrenom:                  prenom du patient
	 * @param pEmail:                   email du patient
	 * @param pNumeroTelephone:         numero de telephone du patient
	 * @param pAdresse:                 adresse du patient
	 * @param pDate :					date de naissance
	 * @param pNumeroDeSecuriteSociale: numero de securite sociale du patient
	 * @param pDocteur:                 medecin traitant du patient
	 * @param pListeSpecialiste:        liste des specialistes du patient
	 * @param pMutuelle:                mutuelle du patient
	 * @throws EntreeException : erreur de saisie
	 */
	public Patient(int pId, String pNom, String pPrenom, String pEmail,
			String pNumeroTelephone, Adresse pAdresse, LocalDate pDate,
			String pNumeroDeSecuriteSociale, Medecin pDocteur,
			ArrayList<Specialiste> pListeSpecialiste, Mutuelle pMutuelle)
			throws EntreeException {
		super(pId, pNom, pPrenom, pEmail, pNumeroTelephone, pAdresse);
		this.setDateDeNaissance(pDate);
		this.setNumeroDeSecuriteSociale(pNumeroDeSecuriteSociale);
		this.setDocteur(pDocteur);
		this.setMutuelle(pMutuelle);
		this.setListeSpecialiste(pListeSpecialiste);
		pDocteur.ajouterPatient(this);
		for(Specialiste spe : pListeSpecialiste)
		{
			spe.ajouterPatient(this);
		}
	}
	
	/**
	 * Constructeur sans specialiste
	 * @param pId : 					identifiant du patient
	 * @param pNom :                     nom du patient
	 * @param pPrenom :                  prenom du patient
	 * @param pEmail :                   email du patient
	 * @param pNumeroTelephone :         numero de telephone du patient
	 * @param pAdresse :                 adresse du patient
	 * @param pDateNaissance :					date de naissance
	 * @param pNumeroDeSecuriteSociale : numero de securite sociale du patient
	 * @param pDocteur :                 medecin traitant du patient
	 * @param pMutuelle : mutuelle du patient
	 * @throws EntreeException : erreur saisie
	 */
	public Patient(int pId, String pNom, String pPrenom, String pEmail,
			String pNumeroTelephone, Adresse pAdresse, LocalDate pDateNaissance,
			String pNumeroDeSecuriteSociale, Medecin pDocteur, Mutuelle pMutuelle)
			throws EntreeException {
		super(pId, pNom, pPrenom, pEmail, pNumeroTelephone, pAdresse);
		this.setDateDeNaissance(pDateNaissance);
		this.setNumeroDeSecuriteSociale(pNumeroDeSecuriteSociale);
		this.setDocteur(pDocteur);
		this.setMutuelle(pMutuelle);
		pDocteur.ajouterPatient(this);

	}

	/**
	 * @return dateDeNaissance: date de naissance du patient
	 */
	public LocalDate getDateDeNaissance() {

		return this.dateDeNaissance;
	}
	

	/**
	 * stter de la date de naissance
	 * @param pDateNaissance : date de naissance
	 * @throws EntreeException : erreur saisie
	 */
	public void setDateDeNaissance(LocalDate pDateNaissance) throws EntreeException {
		if(pDateNaissance==null) throw new EntreeException("Aucune date de Naissance renseignée");
		if(pDateNaissance.isAfter(LocalDate.now())) throw new EntreeException("La date de naissance est après aujourd'hui");
		this.dateDeNaissance = pDateNaissance;
	}

	/**
	 * Methode pour recuperer la date en chaine de caractere
	 * 
	 * @return date sous forme jj/mm/aaaa
	 */
	public String dateToString() {
		DateTimeFormatter formatDate = DateTimeFormatter
				.ofPattern("yyyy-MM-dd");
		return this.getDateDeNaissance().format(formatDate);
	}

	/**
	 * @return numeroDeSecuriteSociale: numero de securite sociale du patient
	 */
	public String getNumeroDeSecuriteSociale() {
		return numeroDeSecuriteSociale;
	}

	/**
	 * @param pNumeroDeSecuriteSociale: numero de securite sociale du patient
	 * @throws EntreeException : erreur sur le format du numero de securite
	 *                         sociale
	 */
	public void setNumeroDeSecuriteSociale(String pNumeroDeSecuriteSociale)
			throws EntreeException {
		if (pNumeroDeSecuriteSociale == null
				|| pNumeroDeSecuriteSociale.trim() == "")
			throw new EntreeException(
					"Aucun numero de sécurité sociale ajouté");
		if (!Pattern.matches("[1-2]\\d{14}", pNumeroDeSecuriteSociale))
			throw new EntreeException(
					"Le numero de securité sociale est une suite de 15 chiffres");
		this.numeroDeSecuriteSociale = pNumeroDeSecuriteSociale;
	}

	/**
	 * @return docteur: medecin traitant du patient
	 */
	public Medecin getDocteur() {
		return docteur;
	}

	/**
	 * @param pDocteur: medecin traitant du patient
	 * @throws EntreeException : erreur medecin null
	 */
	public void setDocteur(Medecin pDocteur) throws EntreeException {
		if (pDocteur == null)
			throw new EntreeException("Aucun médecin ajoutée");
		this.docteur = pDocteur;
	}

	/**
	 * @return listeSpecialiste: liste des scpecialistes
	 */
	public ArrayList<Specialiste> getListeSpecialiste() {
		return listeSpecialiste;
	}
	
	
	/**
	 * setter des specialistes
	 * @param pList : liste des specialiste
	 * @throws EntreeException : erreur dans la liste de specialtiste
	 */
	public void setListeSpecialiste(ArrayList<Specialiste> pList) throws EntreeException
	{
		this.listeSpecialiste=pList;
	}

	/**
	 * Methode pour ajouter un specialiste au patient
	 * 
	 * @param pSpecialiste: specialiste
	 * @throws EntreeException : erreur saisie
	 */
	public void ajouterSpecialiste(Specialiste pSpecialiste)
			throws EntreeException {
		if (pSpecialiste == null)
			throw new EntreeException("Aucun spécialiste ajoutée");
		this.listeSpecialiste.add(pSpecialiste);
	}

	/**
	 * @return mutuelle: mutuelle
	 */
	public Mutuelle getMutuelle() {
		return mutuelle;
	}

	/**
	 * @param pMutuelle: mutuelle
	 * @throws EntreeException : erreur saisie
	 */
	public void setMutuelle(Mutuelle pMutuelle) throws EntreeException {
		if (pMutuelle == null)
			throw new EntreeException("Aucune mutuelle ajoutée");
		this.mutuelle = pMutuelle;
	}

	@Override
	/**
	 * Methode qui retourne une chaine de caractere
	 */
	public String toString() {
		if(this.getNom()==null) return "Aucun";
		return this.getPrenom() + " " + this.getNom();
	}
}

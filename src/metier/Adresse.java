package metier;

import java.io.Serializable;
import java.util.regex.Pattern;

import exceptions.EntreeException;

/**
 * Classe pour définir le Type Adresse
 * 
 * @author JSublon
 *
 */
public class Adresse implements Serializable{


	/**
	 * 
	 */
	private static final long serialVersionUID = -2244356525340063708L;
	// Attributs de la classe
	/**
	 * identfiant de l'adresse
	 */
	private int id;
	/**
	 * adresse
	 */
	private String voie, ville, numeroRue, codePostal;

	/**
	 * Constructeur de l'adresse
	 * @param pId: identifiant
	 * @param pNumeroRue:  numero de rue
	 * @param pVoie:       nom de la rue
	 * @param pCodePostal: code postal
	 * @param pVille:      ville
	 * @throws EntreeException : erreur sur la saisie
	 */
	public Adresse(int pId, String pNumeroRue, String pVoie, String pCodePostal,
			String pVille) throws EntreeException {
		this.setId(pId);
		this.setNumeroRue(pNumeroRue);
		this.setVoie(pVoie);
		this.setCodePostal(pCodePostal);
		this.setVille(pVille);
	}

	/**
	 * 
	 * @return id: identifiant
	 */
	public int getId() {
		return id;
	}

	/**
	 * setter identifiant
	 * @param pId: identifiant
	 * @throws EntreeException : erreur saisie
	 */
	public void setId(int pId) throws EntreeException {
		if (pId < 0) throw new EntreeException("un identifiant ne peut pas être négatif");
		this.id = pId;
	}

	/**
	 * @return numeroRue: numero de la rue
	 */
	public String getNumeroRue() {
		return numeroRue;
	}

	/**
	 * @param pNumeroRue: numero de la rue
	 * @throws EntreeException : erreur sur le numero de la rue
	 */
	public void setNumeroRue(String pNumeroRue) throws EntreeException {
		if (pNumeroRue == null)
			throw new EntreeException("Aucun numéro de rue ajouté");
		if (!Pattern.matches("\\d{1,3}", pNumeroRue))
			throw new EntreeException(
					"Le numéro de rue est un nombre d'un à trois chiffres");
		this.numeroRue = pNumeroRue;
	}

	/**
	 * @return codePostal: code postal
	 */
	public String getCodePostal() {
		return codePostal;
	}

	/**
	 * @param pCodePostal : code postal
	 * @throws EntreeException : erreur sur le code postal
	 */
	public void setCodePostal(String pCodePostal) throws EntreeException {
		if (pCodePostal == null)
			throw new EntreeException("Aucun code postal ajouté");
		if (!Pattern.matches("\\d{5}", pCodePostal))
			throw new EntreeException(
					"Le code postal doit contenir 5 chiffres");
		this.codePostal = pCodePostal;
	}

	/**
	 * @return voie: nom de la rue
	 */
	public String getVoie() {
		return voie;
	}

	/**
	 * @param pVoie: nom de la rue
	 * @throws EntreeException : erreur sur le nom de la rue
	 */
	public void setVoie(String pVoie) throws EntreeException {
		if (pVoie == null)
			throw new EntreeException("Aucune rue ajouté");
		if (!Pattern.matches("^[a-zA-Z]+(?:[\\s-'][a-zA-Zàâäéèëêïîöôüû0-9]+)*$",
				pVoie) || pVoie.length() < 3)
			throw new EntreeException(
					"Le nom de la rue doit contenir au moins 2 lettres et commencer par une lettre");
		this.voie = pVoie;
	}

	/**
	 * @return ville: nom de la ville
	 */
	public String getVille() {
		return ville;
	}

	/**
	 * @param pVille : nom de la ville
	 * @throws EntreeException : erreur sur le nom de la ville
	 */
	public void setVille(String pVille) throws EntreeException {
		if (pVille == null || pVille.trim() == "")
			throw new EntreeException("Aucune ville ajouté");
		if (!pVille.substring(0, 1).toUpperCase()
				.equals(pVille.substring(0, 1)))
			throw new EntreeException(
					"Le nom de la ville doit commencer pas par une majuscule");
		this.ville = pVille;
	}

	/**
	 * Methode qui retourne l'adresse complete en chaine de caractere
	 */
	public String toString() {
		return this.getNumeroRue() + ", " + this.getVoie() + "\n"
				+ this.getCodePostal() + "  " + this.getVille();
	}

}

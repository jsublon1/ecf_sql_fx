package metier;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import exceptions.EntreeException;

/**
 * Classe mere abstraite Achat
 * 
 * @author JSublon
 *
 */
public abstract class Achat implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4966951259356947653L;
	// Attributs de l'objet achat
	/**
	 * identifiant de l'achat
	 */
	private int id;
	/**
	 * vrai/faux ordonnance
	 */
	private boolean ordonnance;
	/**
	 * date de l'achat
	 */
	private LocalDate date;
	/**
	 * patient qui a fait l'achat
	 */
	private Patient patient;
	/**
	 * liste des medicament de l'achat
	 */
	private ArrayList<Medicament> listeMedicament;
	/**
	 * prix total de l'achat
	 */
	private double prixTotal;

	/**
	 * Construecteur lorsque qu'on connait le client et les medicaments qu'il
	 * achete
	 * @param pId : identifiant achat
	 * @param pPatient:         client qui effectue l'achat
	 * @param pListeMedicament: liste des medicaments achetes
	 * @throws EntreeException : erreur saisie
	 */
	public Achat(int pId, Patient pPatient, ArrayList<Medicament> pListeMedicament)
			throws EntreeException {
		this.setPatient(pPatient);
		this.setDate();
		this.setListeMedicament(pListeMedicament);

	}

	/**
	 * Constructeur d'un achat ou la date doit etre rentre manuellement
	 * @param pId				: identifiant
	 * @param pPatient         : client
	 * @param pDate 			: date de l'achat
	 * @param pListeMedicament : liste des medicaments achetes
	 * @throws EntreeException : erreur sur la saisie
	 */
	public Achat(int pId, Patient pPatient, LocalDate pDate,
			ArrayList<Medicament> pListeMedicament) throws EntreeException {
		this.setId(pId);
		this.setPatient(pPatient);
		this.setDate(pDate);
		this.setListeMedicament(pListeMedicament);

	}

	/**
	 * getter de l'identifiant 
	 * @return id: identifiant de l'achat
	 */
	public int getId() {
		return id;
	}

	/**
	 * setter de l'identifiant
	 * @param pId : identifiant de l'achat
	 * @throws EntreeException : erreur saisie
	 */
	public void setId(int pId) throws EntreeException {
		if (pId < 0) throw new EntreeException("un identifiant ne peut pas être négatif");
		this.id = pId;
	}

	/**
	 * @return date: date de l'achat
	 */
	public LocalDate getDate() {
		return date;
	}

	/**
	 * Methode pour recuperer la date sous forme d'une chaine de caractere
	 * 
	 * @return date sous forme d'une chaine de caractere
	 */
	public String dateToString() {
		DateTimeFormatter formatDate = DateTimeFormatter
				.ofPattern("yyyy-MM-dd");
		return this.getDate().format(formatDate);
	}

	/**
	 * Setter de la date du jour
	 */
	public void setDate() {
		this.date = LocalDate.now();
	}
	
	/**
	 * Setter de la date du jour
	 * @param pDate : date de l'achat
	 */
	public void setDate(LocalDate pDate) {
		this.date = pDate;
	}

	/**
	 * @return patient: client qui achete
	 */
	public Patient getPatient() {
		return patient;
	}

	/**
	 * @param pPatient : client
	 * @throws EntreeException : erreur client null
	 */
	public void setPatient(Patient pPatient) throws EntreeException {
		if (pPatient == null)
			throw new EntreeException("Aucun client n'est ajouté");
		this.patient = pPatient;
	}

	/**
	 * @return listeMedicament : liste des medicaments
	 */
	public ArrayList<Medicament> getListeMedicament() {
		return listeMedicament;
	}

	/**
	 * @param pListeMedicament: liste des medicaments
	 * @throws EntreeException : erreur liste de medicament null ou vide
	 */
	public void setListeMedicament(ArrayList<Medicament> pListeMedicament)
			throws EntreeException {
		if (pListeMedicament == null || pListeMedicament.size() == 0)
			throw new EntreeException("L'achat ne comporte aucun médicament");
		this.listeMedicament = pListeMedicament;
	}

	/**
	 * @return prixTotal : prix des achats
	 */
	public double getPrixTotal() {
		return prixTotal;
	}

	/**
	 * 
	 * @throws EntreeException : erreur sur le nombre de médicament
	 */
	public void setPrixTotal() throws EntreeException {
		this.prixTotal = ((double) Math.round(calculerPrixTotal()*100))/100;
	}

	/**
	 * Methode pour ajouter un medicament a la liste de medicament
	 * 
	 * @param pMedicament : medicament achete
	 * @throws EntreeException : erreur medicament null
	 */
	public void ajouterMedicament(Medicament pMedicament)
			throws EntreeException {
		if (pMedicament == null)
			throw new EntreeException("Aucun médicament n'a été sélectionné");
		this.listeMedicament.add(pMedicament);
		setPrixTotal();
	}

	/**
	 * Methode pour supprimer unmedicament de la liste d'achat
	 * @param pMedicament : medicament a supprimer
	 */
	public void supprimerMedicament(Medicament pMedicament)
	{
		boolean supprimer = false;
		for(int i=0; i<this.getListeMedicament().size();i++)
		{
			if(pMedicament.getNom().equals(this.getListeMedicament().get(i).getNom())&&!supprimer)
			{
				this.getListeMedicament().remove(i);
				supprimer = true;
			}
		}
	}
	/**
	 * Methode abstraite du calcul du prix d'un achat
	 * 
	 * @return prixTotal de l'achat
	 * @throws EntreeException : erreur de saisie
	 */
	public abstract double calculerPrixTotal() throws EntreeException;

	/**
	 * Methode abstraite toString
	 */
	public abstract String toString();

	/**
	 * @return the ordonnance
	 */
	public boolean isOrdonnance() {
		return ordonnance;
	}

	/**
	 * setter du boolean ordonnance
	 * @param pOrdonnance : vrai/faux ordonnance
	 */
	public void setOrdonnance(boolean pOrdonnance) {
		this.ordonnance = pOrdonnance;
	}

}

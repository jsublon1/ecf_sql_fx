package metier;

import java.io.Serializable;
import java.util.regex.Pattern;

import exceptions.EntreeException;

/**
 * Objet Mutuelle
 * 
 * @author JSublon
 *
 */
public class Mutuelle implements Serializable{


	/**
	 * 
	 */
	private static final long serialVersionUID = -7838644942806637088L;
	// Attributs de la classe
	/**
	 * identifiant de la mutuelle
	 */
	private int id;
	/**
	 * Nom, email, departement et numero de telephone de la mutuelle
	 */
	private String nom, email, departement, numeroTelephone;
	/**
	 * taux de prise en charge de la mutuelle
	 */
	private double tauxPriseEnCharge;
	/**
	 * adresse de la mutuelle
	 */
	private Adresse adresse;

	/**
	 * Constructeur de l'objet avec tous les attributs
	 * @param pId: 				identifiant
	 * @param pNom:               nom de la mutuelle
	 * @param pEmail:             email de la mutuelle
	 * @param pDepartement:       departement du siege de la mutuelle
	 * @param pNumeroTelephone:   numero de telephone de la mutuelle
	 * @param pTauxPriseEnCharge: taux de prise en charge de la mutuelle
	 * @param pAdresse:           adresse de la mutuelle
	 * @throws EntreeException : erreur de saisie
	 * 
	 */
	public Mutuelle(int pId, String pNom, String pEmail, String pDepartement,
			String pNumeroTelephone, double pTauxPriseEnCharge,
			Adresse pAdresse) throws EntreeException {
		this.setId(pId);
		this.setNom(pNom);
		this.setEmail(pEmail);
		this.setDepartement(pDepartement);
		this.setNumeroTelephone(pNumeroTelephone);
		this.setTauxPriseEnCharge(pTauxPriseEnCharge);
		this.setAdresse(pAdresse);
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param pId : identifiant
	 * @throws EntreeException  : erreur identifiant
	 */
	public void setId(int pId) throws EntreeException {
		if (pId < 0) throw new EntreeException("un identifiant ne peut pas être négatif");
		this.id = pId;
	}

	/**
	 * @return nom: nom de la mutuelle
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * @param pNom: nom de la mutuelle
	 * @throws EntreeException : erreur sur le format du nom de la mutuelle
	 */
	public void setNom(String pNom) throws EntreeException {
		if (pNom == null || pNom.length() < 1)
			throw new EntreeException("Le nom de la mutuelle est trop petit");
		this.nom = pNom;
	}

	/**
	 * @return email: email de la mutuelle
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param pEmail: email de la mutuelle
	 * @throws EntreeException : mauvais format email
	 */
	public void setEmail(String pEmail) throws EntreeException {
		if (pEmail == null)
			throw new EntreeException("L'email ne doit pas être vide");
		if (!Pattern.matches(".+@.+\\.[a-z]+", pEmail))
			throw new EntreeException(
					"L'email de la mutuelle doit être au format #@#.#");
		this.email = pEmail;
	}

	/**
	 * @return departement: departement du siege de la mutuelle
	 */
	public String getDepartement() {
		return departement;
	}

	/**
	 * @param pDepartement: departement du siege de la mutuelle
	 * @throws EntreeException : erreur sur le format du departement de la
	 *                         mutuelle
	 */
	public void setDepartement(String pDepartement) throws EntreeException {
		if (pDepartement == null || pDepartement.trim() == "")
			throw new EntreeException("Aucun nom de département indiqué");
		if (!pDepartement.substring(0, 1).toUpperCase()
				.equals(pDepartement.substring(0, 1))) {
			throw new EntreeException(
					"Le Nom du département doit commencer pas par une majuscule");
		}
		this.departement = pDepartement;
	}

	/**
	 * @return numeroTelephone: numero de telephone de la mutuelle
	 */
	public String getNumeroTelephone() {
		return numeroTelephone;
	}

	/**
	 * @param pNumeroTelephone: numero de telephone de la mutuelle
	 * @throws EntreeException : erreur sur le numero de telephone de la
	 *                         mutuelle
	 */
	public void setNumeroTelephone(String pNumeroTelephone)
			throws EntreeException {
		if (pNumeroTelephone == null || pNumeroTelephone.trim() == "")
			throw new EntreeException("Aucun numéro de téléphone ajouté");
		if (!Pattern.matches("0\\d{9}", pNumeroTelephone))
			throw new EntreeException(
					"Le numéro de téléphone de la mutuelle doit contenir une suite de 10 chiffres commençant par 0");
		this.numeroTelephone = pNumeroTelephone;
	}

	/**
	 * @return tauxPriseEnCharge: taux de prise en charge de la mutuelle
	 */
	public double getTauxPriseEnCharge() {
		return tauxPriseEnCharge;
	}

	/**
	 * @param pTauxPriseEnCharge: taux de prise en charge de la mutuelle
	 * @throws EntreeException : erreur sur le taux de remboursement
	 */
	public void setTauxPriseEnCharge(double pTauxPriseEnCharge)
			throws EntreeException {
		if (pTauxPriseEnCharge < 0 | pTauxPriseEnCharge > 100)
			throw new EntreeException(
					"Le taux de prise en charge doit être compris entre 0 et 100");
		this.tauxPriseEnCharge = pTauxPriseEnCharge;
	}

	/**
	 * @return adresse: adresse de la mutuelle
	 */
	public Adresse getAdresse() {
		return adresse;
	}

	/**
	 * @param pAdresse: adresse de la mutuelle
	 * @throws EntreeException : erreur d'adresse null
	 */
	public void setAdresse(Adresse pAdresse) throws EntreeException {
		if (pAdresse == null)
			throw new EntreeException("Aucune adresse ajouté");
		this.adresse = pAdresse;
	}
	
	/**
	 * 
	 */
	public String toString()
	{
		return this.getNom();
	}
}

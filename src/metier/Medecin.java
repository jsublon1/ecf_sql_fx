package metier;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.regex.Pattern;

import exceptions.EntreeException;

/**
 * Classe fille de la classe Personne
 * 
 * @author JSublon
 *
 */
public class Medecin extends Personne implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = 1376881403006406981L;
	// Attributs de la classe
	/**
	 * numero d'agrement du medecin
	 */
	private String numeroAgrement;
	/**
	 * liste des patients du medecin
	 */
	private ArrayList<Patient> listePatient = new ArrayList<Patient>();
	/**
	 * liste des ordonnances du medecin
	 */
	private ArrayList<AchatAvecOrdonnance> listeOrdonnance = new ArrayList<AchatAvecOrdonnance>();

	/**
	 * Constructeur avec l'ensemble des attributs
	 * @param pId: 			identifiant du medecin
	 * @param pNom:             nom du medecin
	 * @param pPrenom:          prenom du medecin
	 * @param pEmail:           email du medecin
	 * @param pNumeroTelephone: numero de telephone du medecin
	 * @param pAdresse:         adresse du medecin
	 * @param numeroAgrement:   numero d'agreement du medecin
	 * @throws EntreeException : erreur de saisie
	 */
	public Medecin(int pId, String pNom, String pPrenom, String pEmail,
			String pNumeroTelephone, Adresse pAdresse, String numeroAgrement) 
					throws EntreeException {
		super(pId, pNom, pPrenom, pEmail, pNumeroTelephone, pAdresse);
		this.setNumeroAgrement(numeroAgrement);

	}
	
	/**
	 * Constructeur d'un medecin vide
	 */
	public Medecin()
	{
		
	}

	/**
	 * @return numeroAgrement: numero d'agrement du medecin
	 */
	public String getNumeroAgrement() {
		return this.numeroAgrement;
	}

	/**
	 * @param pNumeroAgrement : numero d'agrement du medecin
	 * @throws EntreeException : erreur de saisie du numero d'agrement
	 */
	public void setNumeroAgrement(String pNumeroAgrement)
			throws EntreeException {
		if (pNumeroAgrement == null || pNumeroAgrement.trim() == "")
			throw new EntreeException("Aucun numéro d'agrément indiqué");
		if (!Pattern.matches("4[0-9A-Z]{14}", pNumeroAgrement))
			throw new EntreeException(
					"Le numero d'agrement doit commencer par un 4 suivi de 14 caractères alphanumériques");
		this.numeroAgrement = pNumeroAgrement;
	}

	
	/**
	 * @return listePatient: liste des patients
	 */
	public ArrayList<Patient> getListePatient() {
		return this.listePatient;
	}

	/**
	 * Methode pour ajouter un patient au medecin
	 * 
	 * @param pPatient: patient
	 * @throws EntreeException : erreur de patient null
	 */
	public void ajouterPatient(Patient pPatient) throws EntreeException {
		if (pPatient == null)
			throw new EntreeException("Aucun patient à ajouter");
		this.listePatient.add(pPatient);
	}
	
	/**
	 * @return listeOrdonnance: liste des ordonnances
	 */
	public ArrayList<AchatAvecOrdonnance> getListeOrdonnance() {
		return this.listeOrdonnance;
	}

	/**
	 * Methode pour ajouter une ordonnance au medecin
	 * 
	 * @param pOrdonnance: ordonnance
	 * @throws EntreeException : erreur d'ordonnance null
	 */
	public void ajouterOrdonnance(AchatAvecOrdonnance pOrdonnance)
			throws EntreeException {
		if (pOrdonnance == null)
			throw new EntreeException("Aucune ordonnance à ajouter");
		this.listeOrdonnance.add(pOrdonnance);
	}

	@Override
	/**
	 * Methode pour retourner une chaine de caractere
	 */
	public String toString() {
		if(this.getNom()==null) return "Aucun";
		return "Dr. " + this.getNom();
	}

}

package exceptions;

/**
 * Classe Exception pour les DAO de l'application
 * @author JSublon
 *
 */
@SuppressWarnings("serial")
public class DAOException extends Exception {

	/**
	 * Constructeur avec message
	 * @param message : message d'erreur
	 */
	public DAOException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

}

package exceptions;

/**
 * Classe exception pour l'application pharmacie
 * 
 * @author JSublon
 *
 */
public class EntreeException extends Exception {

	/**
	 * Methode pour recuperer le message d'erreur
	 * 
	 * @param message : message de l'exception lancee
	 */
	public EntreeException(String message) {
		super(message);

	}

}

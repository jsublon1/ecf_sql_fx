package application;

import java.io.IOException;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * Controleur principal de l'application
 * @author JSublon
 *
 */
public class Controller extends Application{
	
	/**
	 * methode de lancement de l'application
	 * @param args : arguments de l'application
	 */
	public static void main(String[] args) {
		launch(args);
	}
	
	@Override
	/**
	 * Procedure qui demarre l'application
	 * @param stage : fenetre de l'application
	 */
	public void start(Stage stage) {
		
		
		try {
			//On associe la "scene" SceneAccueil à la root de la fenetre
			Parent root = FXMLLoader.load(getClass().getResource("vue/SceneAccueil.fxml"));
			Scene scene = new Scene(root);
			//On associe le fichier css pour paramétrer les éléments des fenetres
			scene.getStylesheets().add(
                    getClass().getResource("application.css").toExternalForm());
			stage.setScene(scene);
			stage.setTitle("SPARADRAP");
			stage.setResizable(false);
			stage.getIcons().add(new Image("sparadrap.png")); 
			stage.show();
			//On paramètre la fermeture de la fenetre avec la croix
			stage.setOnCloseRequest(event -> {
				event.consume();
				quitter(stage);
				});
		} catch (IOException e) {
			e.printStackTrace();
			
		}
	   
		  
	}
	
	
	/**
	 * Procedure pour afficher la fenetre de fermeture
	 * @param stage : fenetre a fermer
	 */
	public void quitter(Stage stage)
	{

		Stage fenetreQuitter = new Stage();
		try {
			Parent root = FXMLLoader.load(getClass().getResource("vue/SceneQuitter.fxml"));
			Scene scene = new Scene(root);
			scene.getStylesheets().add(
	                getClass().getResource("application.css").toExternalForm());
			fenetreQuitter.setScene(scene);
			fenetreQuitter.initStyle(StageStyle.UNDECORATED);
			fenetreQuitter.setX(690);
			fenetreQuitter.setY(450);
			fenetreQuitter.show();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
		
//		Alert alertQuitter = new Alert(AlertType.CONFIRMATION);
//		alertQuitter.setTitle("Quitter");
//		alertQuitter.setHeaderText("Vous allez quitter l'application");
//		alertQuitter.setContentText("Etes_vous sûr?");
//		
//		if (alertQuitter.showAndWait().get() == ButtonType.OK)
//		{
//			stage.close();
//			Singleton.closeInstanceDB();
//			System.exit(0);
//		}
	}
}

package application;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.ResourceBundle;

import dao.AchatDAO;
import exceptions.DAOException;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import metier.Achat;
import metier.AchatAvecOrdonnance;
import metier.Medecin;
import metier.Patient;
import metier.Specialiste;
/**
 * Classe de control de la page Historique des Achats
 * @author JSublon
 *
 */
public class ControllerHistoriqueAchat extends Pane implements Initializable {

	//déclaration de la fenetre et de la frame
	private Stage stage;
	private Scene scene;
	private Parent root;
	//Declaration de la liste des AChats
	ArrayList<Achat> listeAchat = new ArrayList<>();
	//Appel des DAO utilisé dans cette scene
	@FXML
	private AnchorPane panePrincipal;
	@FXML
	Button boutonRetour;
	@FXML
	Label labelDate;
	@FXML
	Button boutonQuitter;
	@FXML
	private TableView<Achat> tableHistorique;
	@FXML
	TableColumn<Achat, Patient> colClient;
	@FXML
	TableColumn<AchatAvecOrdonnance, Medecin> colMedecin;
	@FXML
	TableColumn<AchatAvecOrdonnance, Specialiste> colSpecialiste;
	@FXML
	TableColumn<Achat, Double> colPrix;
	//Appel la DAO utile pour la scene
	AchatDAO conAchat = new AchatDAO();
	
	/**
	 * Fonction qui appelle la page precedente
	 * @param event : l'evenement qui ceclenche la fonction
	 * @throws IOException : erreur de lecture
	 */
	public void retour(ActionEvent event) throws IOException
	{
		root = FXMLLoader.load(getClass().getResource("vue/SceneAccueil.fxml"));
		  stage = (Stage)((Node)event.getSource()).getScene().getWindow();
		  scene = new Scene(root);
		  scene.getStylesheets().add(
                  getClass().getResource("application.css").toExternalForm());
		  stage.setScene(scene);
		  stage.show();
	}
	
	/**
	 * Procedure pour quitter la fenetre de l'application
	 * @param event : evenement qui declenche la procedure
	 * @throws IOException ; erreur de lecture
	 */
	public void quitter(ActionEvent event) throws IOException
	{

		Stage fenetreQuitter = new Stage();
		try {
			Parent root = FXMLLoader.load(getClass().getResource("vue/SceneQuitter.fxml"));
			Scene scene = new Scene(root);
			scene.getStylesheets().add(
	                getClass().getResource("application.css").toExternalForm());
			fenetreQuitter.setScene(scene);
			fenetreQuitter.initStyle(StageStyle.UNDECORATED);
			fenetreQuitter.setX(690);
			fenetreQuitter.setY(450);
			fenetreQuitter.show();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	/**
	 * Initialisation de la page historique des achats
	 */
	public void initialize(URL arg0, ResourceBundle arg1) {
		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
		labelDate.setText(LocalDate.now().format(formatter));
		labelDate.setTextAlignment(TextAlignment.CENTER);
		//on paramètre les colonnes du tableau pour savoir quelles valeurs entrer
		colClient.setCellValueFactory(new PropertyValueFactory<>("patient"));
		colMedecin.setCellValueFactory(new PropertyValueFactory<>("docteur"));
		colSpecialiste.setCellValueFactory(new PropertyValueFactory<>("specialiste"));
		colPrix.setCellValueFactory(new PropertyValueFactory<>("prixTotal"));
		colPrix.setCellFactory(c -> new TableCell<>() {
		    @Override
		    protected void updateItem(Double balance, boolean empty) {
		        super.updateItem(balance, empty);
		        if (balance == null || empty) {
		            setText(null);
		        } else {
		            setText(String.format("%.2f", balance.doubleValue()));
		        }
		    }
		});
		
		tableHistorique.setRowFactory( tv -> {
		    TableRow<Achat> row = new TableRow<>();
		    row.setOnMouseClicked(event -> {
		        if (event.getClickCount() == 2 && (! row.isEmpty()) ) {
		            Achat rowData = row.getItem();
		            afficherDetailAchat(rowData);
		        }
		    });
		    return row ;
		});
		
		//on cherche tous les achats du jour et on les met dans la liste des achats
		try {
			listeAchat = conAchat.chercherTous();
		} catch (DAOException e) {
			Alert erreur = new Alert(AlertType.WARNING);
			erreur.setContentText(e.getMessage());
		}
		//on transforme cette liste en liste observable
		ObservableList<Achat> list = FXCollections.observableArrayList(listeAchat);
		//on remplit le tableau avec la liste observable
		tableHistorique.setItems(list);
	}
	
	/**
	 * Procedure pour afficher le detail d'un achat dans une
	 * nouvelle fenetre
	 * @param achat : l'ordonnance a afficher
	 */
	public void afficherDetailAchat(Achat achat)
	{
		//Creation d'une fenetre pour afficher les details
		Alert detailAchat = new Alert(AlertType.INFORMATION);
		detailAchat.setTitle("Client :"+achat.getPatient().toString());
		detailAchat.setHeaderText("Detail de l'ordonnance");
		detailAchat.setContentText(achat.toString());
		detailAchat.show();
	}
	/**
	 * Procedure pour changer la couleur du bouton "Retour"
	 */
	public void boutonRetour()
	{
		boutonRetour.setStyle("-fx-background-color: #FFE4E1;");
	}
	/**
	 * Procedure pour changer la couleur du bouton "Quitter"
	 */
	public void boutonQuitter()
	{
		boutonQuitter.setStyle("-fx-background-color: #FFE4E1;");
	}
	/**
	 * Procedure pour remettre la couleur des boutons
	 * a la valeur initiale
	 */
	public void boutonOff()
	{
		boutonRetour.setStyle("-fx-background-color: #ADD8E6;");
		 
		boutonQuitter.setStyle("-fx-background-color: #ADD8E6;");
		 
	}
}

package application;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.ResourceBundle;

import dao.MedecinDAO;
import exceptions.DAOException;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import metier.AchatAvecOrdonnance;
import metier.Medecin;
import metier.Medicament;
/**
 * Controleur de la page Detail des ordonnances
 * @author JSublon
 *
 */
public class ControllerDetailOrdonnances extends Pane implements Initializable {

	
	//déclaration de la fenetre et de la frame
	private Stage stage;
	private Scene scene;
	private Parent root;
	 
	//appel des éléments de la page FXML utiles dans le code
	@FXML
	private AnchorPane panePrincipal;
	@FXML
	private ComboBox<Medecin> boxMedecin;
	@FXML
	private TableView<AchatAvecOrdonnance> tableOrdonnances;
	@FXML
	private TableColumn<AchatAvecOrdonnance, String> colClient;
	@FXML
	private TableColumn<AchatAvecOrdonnance, String> colDate;
	@FXML
	Button boutonRetour;
	@FXML
	Button boutonQuitter;
	 
	MedecinDAO conMedecin = new MedecinDAO();
	 
	/**
	 * Fonction qui appelle la page precedente
	 * @param event : l'evenement qui ceclenche la fonction
	 * @throws IOException : erreur de lecture
	 */
	public void retour(ActionEvent event) throws IOException
	{
		root = FXMLLoader.load(getClass().getResource("vue/SceneAccueil.fxml"));
		  stage = (Stage)((Node)event.getSource()).getScene().getWindow();
		  scene = new Scene(root);
		  scene.getStylesheets().add(
                  getClass().getResource("application.css").toExternalForm());
		  stage.setScene(scene);
		  stage.show();
	}
	
	/**
	 * Fonction qui appelle une fenetre pour confirmer la fermeture
	 * de l'application
	 * @param event : evenement qui declenche la fenetre
	 * @throws IOException : erreur de lecture
	 */
	public void quitter(ActionEvent event) throws IOException
	{

		//creation d'une fenetre Alert avec 2 choix
		Stage fenetreQuitter = new Stage();
		try {
			Parent root = FXMLLoader.load(getClass().getResource("vue/SceneQuitter.fxml"));
			Scene scene = new Scene(root);
			scene.getStylesheets().add(
	                getClass().getResource("application.css").toExternalForm());
			fenetreQuitter.setScene(scene);
			fenetreQuitter.initStyle(StageStyle.UNDECORATED);
			fenetreQuitter.setX(690);
			fenetreQuitter.setY(450);
			fenetreQuitter.show();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	/**
	 * initialisation de la page
	 */
	public void initialize(URL arg0, ResourceBundle arg1) {
		
		//on ajoute tous les medecins a la comboxBox
		boxMedecin.getItems().add(new Medecin());
		try {
			boxMedecin.getItems().addAll(conMedecin.chercherTous());
		} catch (DAOException e) {
			Alert erreur = new Alert(AlertType.WARNING);
			erreur.setContentText(e.getMessage());
		}
		
		/*
		 * pour pouvoir double cliquer sur les cellules du tableau
		 * et voir l'ordonnance choisit dans ce cas
		 */
		tableOrdonnances.setRowFactory( tv -> {
		    TableRow<AchatAvecOrdonnance> row = new TableRow<>();
		    row.setOnMouseClicked(event -> {
		        if (event.getClickCount() == 2 && (! row.isEmpty()) ) {
		            AchatAvecOrdonnance rowData = row.getItem();
		            afficherDetailOrdonnance(rowData);
		        }
		    });
		    return row ;
		});
	} 
	
	/**
	 * Procedure pour afficher les ordonnances dans un tableau
	 * @param event : evenement declenchant la procedure
	 */
	public void afficherOrdonnances(ActionEvent event)
	{
		//On assigne les colonnes à des éléments de la classe AchatAvecOrdonnance
		colClient.setCellValueFactory(new PropertyValueFactory<>("patient"));
		colDate.setCellValueFactory(new PropertyValueFactory<>("date"));
    	
		//liste des ordonnances
		ArrayList<AchatAvecOrdonnance> listeOrdo = new ArrayList<>();
		//On remplit cette liste avec toutes les ordonnances du medecin choisit
		try {
			listeOrdo = conMedecin.getListeOrdonnance(boxMedecin.getValue().getId());
		} catch (DAOException e) {
			Alert erreur = new Alert(AlertType.WARNING);
			erreur.setContentText(e.getMessage());
		}
		//On transforme cette liste en un liste observable dans un tableau
		ObservableList<AchatAvecOrdonnance> list = FXCollections.observableArrayList(listeOrdo);
		//On assigne la liste observable au tableau
		tableOrdonnances.setItems(list);
	}
	
	/**
	 * Procedure pour afficher le detail d'une ordonnance dans une
	 * nouvelle fenetre
	 * @param achat : l'ordonnance a afficher
	 */
	public void afficherDetailOrdonnance(AchatAvecOrdonnance achat)
	{
		//Creation d'une fenetre pour afficher les details
		Alert detailOrdo = new Alert(AlertType.INFORMATION);
//		detailOrdo.setX(400);
//		detailOrdo.setY(200);
		detailOrdo.setTitle("Client :"+achat.getPatient().toString());
		detailOrdo.setHeaderText("Detail de l'ordonnance");
		detailOrdo.setContentText(achat.toString());
		detailOrdo.show();
	}
	/**
	 * Procedure pour changer la couleur du bouton "Retour"
	 */
	public void boutonRetour()
	{
		boutonRetour.setStyle("-fx-background-color: #FFE4E1;");
	}
	/**
	 * Procedure pour changer la couleur du bouton "Quitter"
	 */
	public void boutonQuitter()
	{
		boutonQuitter.setStyle("-fx-background-color: #FFE4E1;");
	}
	/**
	 * Procedure pour remettre la couleur des boutons
	 * a la valeur initiale
	 */
	public void boutonOff()
	{
		boutonRetour.setStyle("-fx-background-color: #ADD8E6;");
		 
		boutonQuitter.setStyle("-fx-background-color: #ADD8E6;");
		 
	}
}

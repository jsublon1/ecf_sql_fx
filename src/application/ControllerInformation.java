package application;

import java.io.IOException;
import java.net.URL;
import java.time.format.DateTimeFormatter;
import java.util.ResourceBundle;

import dao.PatientDAO;
import exceptions.DAOException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import metier.Patient;

/**
 * Controleur de la page "Information Client"
 * @author JSublon
 *
 */
public class ControllerInformation extends Pane implements Initializable {

	//Formatter de la date
	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
	//déclaration de la fenetre et de la frame
	private Stage stage;
	private Scene scene;
	private Parent root;

	//appel des éléments de la page FXML utiles dans le code
	@FXML
	private AnchorPane panePrincipal;
	@FXML
	private AnchorPane paneInformation;
	@FXML
	Button boutonRetour;
	@FXML
	Button boutonQuitter;
	@FXML
	private Label labelNom;
	@FXML
	private Label labelPrenom;
	@FXML
	private Label labelNaissance;
	@FXML
	private Label labelEmail;
	@FXML
	private Label labelTelephone;
	@FXML
	private Label labelSecuSociale;
	@FXML
	private Label labelAdresseNumero;
	@FXML
	private Label labelAdresseRue;
	@FXML
	private Label labelAdresseCodePostal;
	@FXML
	private Label labelAdresseVille;
	@FXML
	private Label labelMutuelle;
	@FXML
	private Label labelMedecin;
	@FXML
	private Label labelSpecialiste;
	@FXML
	private ComboBox<Patient> boxChoixClient;
	//Appel de la DAO utile pour la scene
	PatientDAO conPatient = new PatientDAO();
	 
	/**
	 * Fonction qui appelle la page precedente
	 * @param event : l'evenement qui ceclenche la fonction
	 * @throws IOException : erreur de lecture
	 */
	public void retour(ActionEvent event) throws IOException
	{
		root = FXMLLoader.load(getClass().getResource("vue/SceneAccueil.fxml"));
		  stage = (Stage)((Node)event.getSource()).getScene().getWindow();
		  scene = new Scene(root);
		  scene.getStylesheets().add(
                  getClass().getResource("application.css").toExternalForm());
		  stage.setScene(scene);
		  stage.show();
	}
	
	/**
	 * Procedure pour quitter la fenetre de l'application
	 * @param event : evenement qui declenche la procedure
	 * @throws IOException ; erreur de lecture
	 */
	public void quitter(ActionEvent event) throws IOException
	{

		Stage fenetreQuitter = new Stage();
		try {
			Parent root = FXMLLoader.load(getClass().getResource("vue/SceneQuitter.fxml"));
			Scene scene = new Scene(root);
			scene.getStylesheets().add(
	                getClass().getResource("application.css").toExternalForm());
			fenetreQuitter.setScene(scene);
			fenetreQuitter.initStyle(StageStyle.UNDECORATED);
			fenetreQuitter.setX(690);
			fenetreQuitter.setY(450);
			fenetreQuitter.show();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	/**
	 * Initialisation de la page
	 */
	public void initialize(URL arg0, ResourceBundle arg1) {
		
		//On vide la comboBox "choix du client"
		boxChoixClient.getItems().clear();
		//On affiche "Non Renseigné" sur tous les labels
		resetInfo();
		//On ajoute un patient vide au début
		boxChoixClient.getItems().add(new Patient());
		//On ajoute tous les patients de la BDD dans la comboBox
		try {
			boxChoixClient.getItems().addAll(conPatient.chercherTous());
		} catch (DAOException e) {
			Alert erreur = new Alert(AlertType.WARNING);
			erreur.setContentText(e.getMessage());
		}
		boxChoixClient.getSelectionModel().selectFirst();
	} 
	
	/**
	 * Procedure pour afficher les informations
	 * quand aucun client n'est selectionne
	 */
	private void resetInfo()
	{
		labelNom.setText("Non Renseigné");
		labelPrenom.setText("Non Renseigné");
		labelNaissance.setText("Non Renseigné");
		labelEmail.setText("Non Renseigné");
		labelTelephone.setText("Non Renseigné");
		labelSecuSociale.setText("Non Renseigné");
		labelAdresseNumero.setText("****");
		labelAdresseRue.setText("Non Renseigné");
		labelAdresseCodePostal.setText("*****");
		labelAdresseVille.setText("Non Renseigné");
		labelMutuelle.setText("Non Renseigné");
		labelMedecin.setText("Non Renseigné");
		labelSpecialiste.setText("Non Renseigné");
		
	}
	
	/**
	 * Procedure pour afficher les informations du client selectionne
	 * @param event : evenement declencheur
	 */
	public void afficherInformation(ActionEvent event)
	{
		//si aucun client n'est selectionne
		if(boxChoixClient.getValue().toString().contains("Aucun"))
		{
			resetInfo();
		}
		//Un client est selectionne
		else
		{
			//On crée un patient avec les donnees de la BDD
			Patient patient = boxChoixClient.getValue();
			//On remplit tous les champs avec les donnees du patient
			labelNom.setText(patient.getNom());
			labelPrenom.setText(patient.getPrenom());
			labelNaissance.setText(formatter.format(patient.getDateDeNaissance()));
			labelEmail.setText(patient.getEmail());
			labelTelephone.setText(patient.getNumeroTelephone());
			labelSecuSociale.setText(patient.getNumeroDeSecuriteSociale());
			labelAdresseNumero.setText(patient.getAdresse().getNumeroRue());
			labelAdresseRue.setText(patient.getAdresse().getVoie());
			labelAdresseCodePostal.setText(patient.getAdresse().getCodePostal());
			labelAdresseVille.setText(patient.getAdresse().getVille());
			labelMutuelle.setText(patient.getMutuelle().getNom());
			labelMedecin.setText(patient.getDocteur().toString());
			labelSpecialiste.setText(patient.getListeSpecialiste().toString());
		}
	}
	/**
	 * Procedure pour changer la couleur du bouton "Retour"
	 */
	public void boutonRetour()
	{
		boutonRetour.setStyle("-fx-background-color: #FFE4E1;");
	}
	/**
	 * Procedure pour changer la couleur du bouton "Quitter"
	 */
	public void boutonQuitter()
	{
		boutonQuitter.setStyle("-fx-background-color: #FFE4E1;");
	}
	/**
	 * Procedure pour remettre la couleur des boutons
	 * a la valeur initiale
	 */
	public void boutonOff()
	{
		boutonRetour.setStyle("-fx-background-color: #ADD8E6;");
		 
		boutonQuitter.setStyle("-fx-background-color: #ADD8E6;");
		 
	}

}

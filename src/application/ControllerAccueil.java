package application;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
/**
 * Classe de control de la page Accueil
 * @author JSublon
 *
 */
public class ControllerAccueil extends Pane implements Initializable{

	//déclaration de la fenetre et de la frame
	private Stage stage;
	private Scene scene;
	private Parent root;
	 
	//appel des éléments de la page FXML utiles dans le code
	@FXML
	private AnchorPane panePrincipal;
	@FXML
	Button boutonQuitter;
	@FXML
	Button boutonAchat;
	@FXML
	Button boutonHistoriqueAchat;
	@FXML
	Button boutonHistoriqueOrdonnances;
	@FXML
	Button boutonInformationClient;
	@FXML
	Button boutonNouveau;

	@Override
	/**
	 * Initialisation des elements de la page
	 */
	public void initialize(URL arg0, ResourceBundle arg1) {
		
		//On met le texte dans les boutons qui vont afficher plusieurs lignes
		boutonHistoriqueAchat.setText("HISTORIQUE\nDES\nACHATS");
//		boutonHistoriqueAchat.setWrapText(true);
		boutonHistoriqueAchat.setAlignment(Pos.CENTER);
		boutonHistoriqueOrdonnances.setText("HISTORIQUE\nDES\nORDONNANCES");
		boutonHistoriqueOrdonnances.setAlignment(Pos.CENTER);
		boutonInformationClient.setText("INFORMATION\nCLIENT");
		boutonInformationClient.setAlignment(Pos.CENTER);
		boutonNouveau.setText("NOUVELLE\nPERSONNE");
		boutonNouveau.setAlignment(Pos.CENTER);
	}

	/**
	 * Procedure pour quitter la fenetre de l'application
	 * @param event : evenement qui declenche la procedure
	 * @throws IOException ; erreur de lecture
	 */
	public void quitter(ActionEvent event) throws IOException
	{

		Stage fenetreQuitter = new Stage();
		try {
			Parent root = FXMLLoader.load(getClass().getResource("vue/SceneQuitter.fxml"));
			Scene scene = new Scene(root);
			scene.getStylesheets().add(
	                getClass().getResource("application.css").toExternalForm());
			fenetreQuitter.setScene(scene);
			fenetreQuitter.initStyle(StageStyle.UNDECORATED);
			fenetreQuitter.setX(690);
			fenetreQuitter.setY(450);
			fenetreQuitter.show();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Fonction qui appelle la page nouvelle personne
	 * @param event : l'evenement qui ceclenche la fonction
	 * @throws IOException : erreur de lecture
	 */
	public void afficherNouvellePersonne(ActionEvent event) throws IOException
	{
		root = FXMLLoader.load(getClass().getResource("vue/SceneNouvellePersonne.fxml"));
		  stage = (Stage)((Node)event.getSource()).getScene().getWindow();
		  scene = new Scene(root);
		  scene.getStylesheets().add(
                getClass().getResource("application.css").toExternalForm());
		  stage.setScene(scene);
		  stage.show();
	}
	
	/**
	 * Fonction qui appelle la page achat
	 * @param event : l'evenement qui ceclenche la fonction
	 * @throws IOException : erreur de lecture
	 */
	public void afficherAchat(ActionEvent event) throws IOException
	{
		root = FXMLLoader.load(getClass().getResource("vue/SceneAchat.fxml"));
		  stage = (Stage)((Node)event.getSource()).getScene().getWindow();
		  scene = new Scene(root);
		  scene.getStylesheets().add(
                  getClass().getResource("application.css").toExternalForm());
		  stage.setScene(scene);
		  stage.show();
	}
	
	/**
	 * Fonction qui appelle la page historique des achats du jour
	 * @param event : l'evenement qui ceclenche la fonction
	 * @throws IOException : erreur de lecture
	 */
	public void afficherHistoriqueAchats(ActionEvent event) throws IOException {
		root = FXMLLoader.load(getClass().getResource("vue/SceneHistoriqueAchat.fxml"));
		  stage = (Stage)((Node)event.getSource()).getScene().getWindow();
		  scene = new Scene(root);
		  scene.getStylesheets().add(
                  getClass().getResource("application.css").toExternalForm());
		  stage.setScene(scene);
		  stage.show();
	}
	
	/**
	 * Fonction qui appelle la page historique des ordonnances
	 * @param event : l'evenement qui ceclenche la fonction
	 * @throws IOException : erreur de lecture
	 */
	public void afficherHistoriqueOrdonnances(ActionEvent event) throws IOException {
		root = FXMLLoader.load(getClass().getResource("vue/SceneDetailOrdonnance.fxml"));
		  stage = (Stage)((Node)event.getSource()).getScene().getWindow();
		  scene = new Scene(root);
		  scene.getStylesheets().add(
                  getClass().getResource("application.css").toExternalForm());
		  stage.setScene(scene);
		  stage.show();
	}
	
	/**
	 * Fonction qui appelle la page information Client
	 * @param event : l'evenement qui ceclenche la fonction
	 * @throws IOException : erreur de lecture
	 */
	public void afficherInformationClient(ActionEvent event) throws IOException {
		root = FXMLLoader.load(getClass().getResource("vue/SceneInformationClient.fxml"));
		  stage = (Stage)((Node)event.getSource()).getScene().getWindow();
		  scene = new Scene(root);
		  scene.getStylesheets().add(
                  getClass().getResource("application.css").toExternalForm());
		  stage.setScene(scene);
		  stage.show();
	}
	
	/**
	 * Fonction qui change la couleur du bouton "Nouveau"
	 */
	public void boutonNouveau()
	{
		boutonNouveau.setStyle("-fx-background-color: #FFE4E1;");
	}
	/**
	 * Fonction qui change la couleur du bouton "Achat"
	 */
	public void boutonAchat()
	{
		boutonAchat.setStyle("-fx-background-color: #FFE4E1;");
	}
	/**
	 * Fonction qui change la couleur du bouton "Historique des achats"
	 */
	public void boutonHistoriqueAchat()
	{
		boutonHistoriqueAchat.setStyle("-fx-background-color: #FFE4E1;");
	}
	/**
	 * Fonction qui change la couleur du bouton "Historique des ordonnances"
	 */
	public void boutonOrdonnance()
	{
		boutonHistoriqueOrdonnances.setStyle("-fx-background-color: #FFE4E1;");
	}
	/**
	 * Fonction qui change la couleur du bouton "Information client"
	 */
	public void boutonInformation()
	{
		boutonInformationClient.setStyle("-fx-background-color: #FFE4E1;");
	}
	/**
	 * Fonction qui change la couleur du bouton "Quitter"
	 */
	public void boutonQuitter()
	{
		boutonQuitter.setStyle("-fx-background-color: #FFE4E1;");
	}
	/**
	 * Fonction qui remet la couleur de tous les boutons a la valeur initiale
	 */
	public void boutonOff()
	{
		boutonNouveau.setStyle("-fx-background-color: #ADD8E6;");
		 
		boutonQuitter.setStyle("-fx-background-color: #ADD8E6;");
		 
		boutonAchat.setStyle("-fx-background-color: #ADD8E6;");
	
		boutonHistoriqueAchat.setStyle("-fx-background-color: #ADD8E6;");
	
		boutonHistoriqueOrdonnances.setStyle("-fx-background-color: #ADD8E6;");
		 
		boutonInformationClient.setStyle("-fx-background-color: #ADD8E6;");
	}
}

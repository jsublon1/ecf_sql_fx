package application;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.ResourceBundle;

import dao.AchatDAO;
import dao.MedicamentDAO;
import dao.PatientDAO;
import dao.SpecialisteDAO;
import exceptions.DAOException;
import exceptions.EntreeException;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import metier.Achat;
import metier.AchatAvecOrdonnance;
import metier.AchatSansOrdonnance;
import metier.Medicament;
import metier.Patient;
import metier.Specialiste;
/**
 * Classe de control de la page achat
 * @author JSublon
 *
 */
public class ControllerAchat extends Pane implements Initializable{

	
	//déclaration de la fenetre et de la frame
	private Stage stage;
	private Scene scene;
	private Parent root;
	//déclaration des éléments utiles au code
	private Boolean avecOrdonnance; //pour savoir si l'achat se fait avec ou sans ordonnance
	private ArrayList<Medicament> listeMedicament = new ArrayList<>();
	private Specialiste rienSpecialiste = new Specialiste();
	private Achat achat = null;
	
	//Appel des DAO utilisé dans cette scene
	PatientDAO conPatient = new PatientDAO();
	MedicamentDAO conMedoc = new MedicamentDAO();
	SpecialisteDAO conSpecialiste = new SpecialisteDAO();
	AchatDAO conAchat = new AchatDAO();
	
	//appel des éléments de la page FXML utiles dans le code
	@FXML
	private AnchorPane panePrincipal; 
	@FXML
	Button boutonRetour;
	@FXML
	Button boutonQuitter;
	@FXML
	Button boutonAchatSansOrdonnance;
	@FXML
	Button boutonAchatAvecOrdonnance;
	@FXML
	Button boutonAjouter;
	@FXML
	Button boutonValider;
	@FXML
	Label labelMedecin;
	@FXML
	Label labelNomMedecin;
	@FXML
	Label labelSpecialiste;
	@FXML
	Label labelAvecChoix;
	@FXML
	Label labelSansChoix;
	@FXML
	ComboBox<Specialiste> boxSpecialiste;
	@FXML
	ComboBox<Patient> boxClient;
	@FXML
	ComboBox<Medicament> boxMedicament;
	@FXML
	TableView<Medicament> tableAchat;
	@FXML
	TableColumn<Medicament, String> colonneMedicament;
	@FXML
	TableColumn<Medicament, Double> colonnePrix;
	@FXML
	Label labelErreur;
	@FXML
	Label labelPrixPayer;
	@FXML
	Label labelPrixPayerValeur;
	@FXML
	Label labelPrixValeur;
	
	 
	private static final DecimalFormat df = new DecimalFormat("0.00");
	
	@Override
	/**
	 * Initialisation de la page
	 */
	public void initialize(URL arg0, ResourceBundle arg1) {
		
		//Par defaut, on affiche un achat sans ordonnance
		achatSansOrdonnance(null);
		//On paramètre les labels medecin et erreur en vide
		labelNomMedecin.setText("");
		labelErreur.setText("");
		labelErreur.setWrapText(true);
		labelPrixValeur.setText("");
		labelPrixPayerValeur.setText("");
		//On ajoute les clients et médicaments au comboBox
		try {
			boxClient.getItems().addAll(conPatient.chercherTous());
			boxMedicament.getItems().addAll(conMedoc.chercherTous());
		} catch (DAOException e) {
			labelErreur.setText(e.getMessage());
		}
		
		
		colonneMedicament.setCellValueFactory(new PropertyValueFactory<>("nom"));
		colonnePrix.setCellValueFactory(new PropertyValueFactory<>("prix"));
		colonnePrix.setCellFactory(c -> new TableCell<>() {
		    @Override
		    protected void updateItem(Double balance, boolean empty) {
		        super.updateItem(balance, empty);
		        if (balance == null || empty) {
		            setText(null);
		        } else {
		            setText(String.format("%.2f", balance.doubleValue()));
		        }
		    }
		});
		/**
		 * On paramètre le tableau pour qu'un double clic supprime un médicament
		 */
		tableAchat.setRowFactory( tv -> {
		    TableRow<Medicament> row = new TableRow<>();
		    row.setOnMouseClicked(event -> {
		        if (event.getClickCount() == 2 && (! row.isEmpty()) ) {
		            Medicament rowData = row.getItem();
		            listeMedicament.remove(rowData);
		            mettreAJourTableau();
		            
		        }
		    });
		    return row ;
		});
		
	}
	
	/**
	 * Fonction qui appelle la page precedente
	 * @param event : l'evenement qui ceclenche la fonction
	 * @throws IOException : erreur de lecture
	 */
	public void retour(ActionEvent event) throws IOException
	{
		root = FXMLLoader.load(getClass().getResource("vue/SceneAccueil.fxml"));
		  stage = (Stage)((Node)event.getSource()).getScene().getWindow();
		  scene = new Scene(root);
		  scene.getStylesheets().add(
                  getClass().getResource("application.css").toExternalForm());
		  stage.setScene(scene);
		  stage.show();
	}

	
	/**
	 * Procedure pour quitter la fenetre de l'application
	 * @param event : evenement qui declenche la procedure
	 * @throws IOException ; erreur de lecture
	 */
	public void quitter(ActionEvent event) throws IOException
	{

		Stage fenetreQuitter = new Stage();
		try {
			Parent root = FXMLLoader.load(getClass().getResource("vue/SceneQuitter.fxml"));
			Scene scene = new Scene(root);
			scene.getStylesheets().add(
	                getClass().getResource("application.css").toExternalForm());
			fenetreQuitter.setScene(scene);
			fenetreQuitter.initStyle(StageStyle.UNDECORATED);
			fenetreQuitter.setX(690);
			fenetreQuitter.setY(450);
			fenetreQuitter.show();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Procedure pour afficher la page achat sans ordonnance
	 * @param event : evenement qui declenche la procedure
	 */
	public void achatSansOrdonnance(ActionEvent event)
	{
		/**
		 * Ici on va rendre les labels et la comboBox
		 * pertinant à un achat avec ordonnance invisible
		 */
		labelMedecin.setVisible(false);
		
		labelNomMedecin.setVisible(false);
		
		labelSpecialiste.setVisible(false);
		 
		boxSpecialiste.setVisible(false);
		 
		labelPrixPayer.setVisible(false);
		
		labelPrixPayerValeur.setVisible(false);
		
		labelAvecChoix.setVisible(false);
		
		labelSansChoix.setVisible(true);
		 
		avecOrdonnance=false;
	}
	/**
	 * Procedure pour afficher la page achat avec ordonnance
	 * @param event : evenement qui declenche la procedure
	 */
	public void achatAvecOrdonnance(ActionEvent event)
	{
		/*
		* On rend les label medecin et specialiste visible
		* ainsi que la combobox pour choisir le specialiste
		*/
		labelMedecin.setVisible(true);
		
		labelNomMedecin.setVisible(true);
		
		labelSpecialiste.setVisible(true);
		 
		boxSpecialiste.setVisible(true);
		 
		labelPrixPayer.setVisible(true);
			
		labelPrixPayerValeur.setVisible(true);
		
		labelAvecChoix.setVisible(true);
		
		labelSansChoix.setVisible(false);
		 
		avecOrdonnance=true;
	}
	
	/**
	 * Procedure pour ajouter un medicament dans la liste
	 * des medicaments et dans le tableau
	 * @param event : evenement declencheur
	 */
	public void ajouterMedicament(ActionEvent event)
	{
		/**
		 * On ajoute la valeur de la combobox a la liste des medicaments
		 */
		if(!(boxClient.getValue()==null))
		{
			if(!(boxMedicament.getValue()==null))
			{
				Medicament medoc = boxMedicament.getValue();
				listeMedicament.add(medoc);
				//la liste observable est la liste des medeicaments ajoutes
				mettreAJourTableau();
			}
			else
			{
				labelErreur.setText("Aucun Médicament n'est sélectionné");
			}
		}
		else
		{
			labelErreur.setText("Aucun Client n'est sélectionné");
		}
		
			 
	}
	
	/**
	 * Procedure pour remplir le tableau
	 */
	public void mettreAJourTableau()
	{
		double prix = calculerPrix(listeMedicament);
		double prixMutuelle = prix*(1-boxClient.getValue().getMutuelle().getTauxPriseEnCharge()/100);
		labelPrixValeur.setText(df.format(prix)+" €");
		labelPrixPayerValeur.setText(df.format(prixMutuelle)+" €");
		
		ObservableList<Medicament> list = FXCollections.observableArrayList(listeMedicament);
		
		tableAchat.setItems(list); 	
	}
	
	/**
	 * Procedure pour valider un achat
	 * @param event : evenement declencheur
	 */
	public void validerAchat(ActionEvent event)
	{
		boolean creationOK = false; //boolean pour verifier que les procedures se passent bien
		if (avecOrdonnance==false) //achat sans ordonnance
		{
			try {
				achat = new AchatSansOrdonnance(0,boxClient.getValue(),listeMedicament);
				creationOK=true;
			} catch (EntreeException  e) {
				labelErreur.setText(e.getMessage());
			}
			
		}
		else //achat avec orodnnance
		{
			if(boxSpecialiste.getValue() == boxSpecialiste.getItems().get(0)) 
			{//si il n'y a pas de specialiste dans l'ordonnance
				try {
					achat = new AchatAvecOrdonnance(0,boxClient.getValue(),
								listeMedicament,boxClient.getValue().getDocteur());
					creationOK=true;
				} catch (EntreeException e) {
					labelErreur.setText(e.getMessage());
				}
			}
			else //si il y a un specialiste dans l'ordonnance
			{
				try {
					achat = new AchatAvecOrdonnance(0,boxClient.getValue(),
							listeMedicament,boxClient.getValue().getDocteur(),
							boxSpecialiste.getValue());
					creationOK=true;
					
				} catch (EntreeException  e) {
					labelErreur.setText(e.getMessage());
				}
			}	
		}
		/*
		 * Si on a cree un achat, on le sauvegarde dans la BDD
		 */
		if(creationOK==true)
		{
			try {
				creationOK=conAchat.creer(achat);
			}  catch (EntreeException e) {
				labelErreur.setText(e.getMessage());
				creationOK=false;
			}catch (DAOException e) {
				labelErreur.setText(e.getMessage());
				creationOK=false;
			} catch (SQLException  e) {
				creationOK=false;
				e.printStackTrace();
			}
			}
		/*
		 * On a sauvegarde dans la BDD, on peut revenir sur la page Accueil
		 * après avoir afficher l'achat
		 */
		if(creationOK==true)
		{
			Alert facture = new Alert(AlertType.INFORMATION);
//			detailOrdo.setX(400);
//			detailOrdo.setY(200);
			facture.setTitle("Client :"+achat.getPatient().toString());
			facture.setHeaderText("Detail de l'achat");
			facture.setContentText(achat.toString());
			facture.show();
			try {
				retour(event);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Procedure pour remplir la comboBox Specialiste
	 * et changer les informations de la page en fonction du client
	 * en fonction du patient choisit
	 * @param event : evenement declancheur
	 */
	public void changerSpecialiste(ActionEvent event)
	{
		//Si aucun patient n'est choisit, la comboBox est vide
		if(boxClient.getValue()==null)
		{
			labelNomMedecin.setText("");
		}
		/*
		 * on efface la comboBox et on la remplit avec les specialiste
		 * du patient plus un specialiste vide pour indiquer qu'on ne prend
		 * pas de specialiste
		 */
		else 
		{
			//effacer la comboBox
			boxSpecialiste.getItems().clear();
			//changer le label du nom du medecin
			labelNomMedecin.setText(boxClient.getValue().getDocteur().toString());
			//ajout du specialiste vide
			boxSpecialiste.getItems().add(rienSpecialiste);
			//ajout des specialistes du patient
			try {
				boxSpecialiste.getItems().addAll(conSpecialiste.chercherSpecialisteDuPatient(boxClient.getValue().getId()));
			} catch (DAOException e) {
				labelErreur.setText(e.getMessage());
			}
			//on place le premier element en visible
			boxSpecialiste.getSelectionModel().selectFirst();
		}
	}
	
	/**
	 * Fonction de calcul du prix total
	 * @param list : liste des medicament
	 * @return prix total de l'achat
	 */
	public double calculerPrix(ArrayList<Medicament> list)
	{
		double prix = 0;
		for(Medicament medoc : list)
		{
			prix = prix+medoc.getPrix();
		}
		return prix;
	}
	/**
	 * Procedure pour changer la couleur du bouton "Retour"
	 */
	public void boutonRetour()
	{
		boutonRetour.setStyle("-fx-background-color: #FFE4E1;");
	}
	/**
	 * Procedure pour changer la couleur du bouton "Quitter"
	 */
	public void boutonQuitter()
	{
		boutonQuitter.setStyle("-fx-background-color: #FFE4E1;");
	}
	/**
	 * Procedure pour changer la couleur du bouton "Valider"
	 */
	public void boutonValider()
	{
		boutonValider.setStyle("-fx-background-color: #FFE4E1;");
	}
	/**
	 * Procedure pour changer la couleur du bouton "Ajouter"
	 */
	public void boutonAjouter()
	{
		boutonAjouter.setStyle("-fx-background-color: #FFE4E1;");
	}
	/**
	 * Procedure pour changer la couleur du bouton "Achat Sans Ordonnance"
	 */
	public void boutonAchatSansOrdonnance()
	{
		boutonAchatSansOrdonnance.setStyle("-fx-background-color: #FFE4E1;");
	}
	/**
	 * Procedure pour changer la couleur du bouton "Achat Avec Ordonnance"
	 */
	public void boutonAchatAvecOrdonnance()
	{
		boutonAchatAvecOrdonnance.setStyle("-fx-background-color: #FFE4E1;");
	}
	/**
	 * Procedure pour remettre la couleur des boutons
	 * a la valeur initiale
	 */
	public void boutonOff()
	{
		boutonRetour.setStyle("-fx-background-color: #ADD8E6;");
		 
		boutonQuitter.setStyle("-fx-background-color: #ADD8E6;");
		 
		boutonAchatSansOrdonnance.setStyle("-fx-background-color: #ADD8E6;");
		
		boutonAchatAvecOrdonnance.setStyle("-fx-background-color: #ADD8E6;");
	
		boutonValider.setStyle("-fx-background-color: #ADD8E6;");
		
		boutonAjouter.setStyle("-fx-background-color: #ADD8E6;");
		 
	}
}

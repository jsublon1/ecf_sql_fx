package application;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

import dao.AdresseDAO;
import dao.MedecinDAO;
import dao.MutuelleDAO;
import dao.PatientDAO;
import dao.SpecialisteDAO;
import dao.UtilitaireDAO;
import exceptions.DAOException;
import exceptions.EntreeException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import metier.Adresse;
import metier.Medecin;
import metier.Mutuelle;
import metier.Patient;
import metier.Specialiste;

/**
 * Classe de control de la page Nouvelle Personne
 * @author JSublon
 *
 */
public class ControllerNouvellePersonne extends Pane implements Initializable{

	//déclaration de la fenetre et de la frame
	private Stage stage;
	private Scene scene;
	private Parent root;
	//déclaration des éléments utiles au code
	private String choix = "CLIENT";
	private ArrayList<Specialiste> listeSpe = new ArrayList<>();
	//Appel des DAO utilisé dans cette scene
	@FXML
	private AnchorPane panePrincipal;
	@FXML
	Button boutonRetour;
	@FXML
	Button boutonQuitter;
	@FXML
	Button boutonClient;
	@FXML
	Button boutonMedecin;
	@FXML
	Button boutonSpecialiste;
	@FXML
	Button boutonValider;
	@FXML
	TextField texteNom;
	@FXML
	TextField textePrenom;
	@FXML
	TextField texteEmail;
	@FXML
	TextField texteNumeroTelephone;
	@FXML
	TextField texteNumRue;
	@FXML
	TextField texteNomRue;
	@FXML
	TextField texteCodePostal;
	@FXML
	TextField texteVille;
	@FXML
	Label labelInfoSecuSociale;
	@FXML
	TextField texteNumSecuSociale;
	@FXML
	Label labelInfoNaissance;
	@FXML
	DatePicker choixDateNaissance;
	@FXML
	Label labelInfoMutuelle;
	@FXML
	ComboBox<Mutuelle> boxMutuelle;
	@FXML
	Label labelInfoMedecin;
	@FXML
	ComboBox<Medecin> boxMedecin;
	@FXML
	Label labelInfoSpecialiste;
	@FXML
	ComboBox<Specialiste> boxSpecialiste;
	@FXML
	Label labelInfoNumeroAgrement;
	@FXML
	TextField texteNumeroAgrement;
	@FXML
	Label labelInfoSpecialite;
	@FXML
	ComboBox<String> boxSpecialite;
	@FXML
	Label labelErreur;
	@FXML
	Button boutonAjouter;
	@FXML
	Button boutonReset;
	@FXML
	Label labelListeSpe;
	@FXML
	Label labelClientChoix;
	@FXML
	Label labelMedecinChoix;
	@FXML
	Label labelSpecialisteChoix;
	//Appel des DAO utiles au code
	MutuelleDAO conMutuelle = new MutuelleDAO();
	MedecinDAO conMedecin = new MedecinDAO();
	PatientDAO conPatient = new PatientDAO();
	SpecialisteDAO conSpecialiste = new SpecialisteDAO();
	
	@Override
	/**
	 * Initialisation de la page
	 */
	public void initialize(URL arg0, ResourceBundle arg1) {
		
		//Par defaut, on affiche l'entrée d'un nouveau client
		afficherNouveauClient();
		//On ajoute tous les medecins, specialistes et mutuelles dans les comboBox
		try {
			boxMutuelle.getItems().addAll(conMutuelle.chercherTous());
			boxMedecin.getItems().addAll(conMedecin.chercherTous());
			boxSpecialiste.getItems().addAll(conSpecialiste.chercherTous());
		} catch (DAOException e) {
			labelErreur.setText(e.getMessage());
		}
		boxSpecialiste.getSelectionModel().selectFirst();
		//on ajoute les specialites dans la comboBox
		boxSpecialite.getItems().addAll(conSpecialiste.cherSpecialiteTous());
		//Le label erreur est vide
		labelErreur.setText("");
		labelErreur.setTextFill(Color.RED);
		
	}
	
	/**
	 * Procedure pour afficher les elements pour creer un nouveau client
	 */
	public void afficherNouveauClient()
	{
		choix="CLIENT"; //Pour savoir qu'on est en train de creer un client
		//On met visible les elements pertinants et invisible les autres
		labelClientChoix.setVisible(true);
		labelMedecinChoix.setVisible(false);
		labelSpecialisteChoix.setVisible(false);
		labelInfoSecuSociale.setVisible(true);
		texteNumSecuSociale.setVisible(true);
		labelInfoNaissance.setVisible(true);
		choixDateNaissance.setVisible(true);
		labelInfoMutuelle.setVisible(true);
		boxMutuelle.setVisible(true);
		labelInfoMedecin.setVisible(true);
		boxMedecin.setVisible(true);
		labelInfoSpecialiste.setVisible(true);
		boxSpecialiste.setVisible(true);
		labelInfoNumeroAgrement.setVisible(false);
		texteNumeroAgrement.setVisible(false);
		labelInfoSpecialite.setVisible(false);
		boxSpecialite.setVisible(false);
		boutonAjouter.setVisible(true);
		boutonReset.setVisible(true);
		labelListeSpe.setVisible(true);
		
	}
	/**
	 * Procedure pour afficher les elements pour creer un nouveau medecin
	 */
	public void afficherNouveauMedecin()
	{
		choix="MEDECIN"; //Pour savoir qu'on est en train de creer un medecin
		//On met visible les elements pertinants et invisible les autres
		labelClientChoix.setVisible(false);
		labelMedecinChoix.setVisible(true);
		labelSpecialisteChoix.setVisible(false);
		labelInfoSecuSociale.setVisible(false);
		texteNumSecuSociale.setVisible(false);
		labelInfoNaissance.setVisible(false);
		choixDateNaissance.setVisible(false);
		labelInfoMutuelle.setVisible(false);
		boxMutuelle.setVisible(false);
		labelInfoMedecin.setVisible(false);
		boxMedecin.setVisible(false);
		labelInfoSpecialiste.setVisible(false);
		boxSpecialiste.setVisible(false);
		labelInfoNumeroAgrement.setVisible(true);
		texteNumeroAgrement.setVisible(true);
		labelInfoSpecialite.setVisible(false);
		boxSpecialite.setVisible(false);
		boutonAjouter.setVisible(false);
		boutonReset.setVisible(false);
		labelListeSpe.setVisible(false);
	}
	/**
	 * Procedure pour afficher les elements pour creer un nouveau specialiste
	 */
	public void afficherNouveauSpecialiste()
	{
		choix="SPECIALISTE"; //Pour savoir qu'on est en train de creer un specialiste
		//On met visible les elements pertinants et invisible les autres
		labelClientChoix.setVisible(false);
		labelMedecinChoix.setVisible(false);
		labelSpecialisteChoix.setVisible(true);
		labelInfoSecuSociale.setVisible(false);
		texteNumSecuSociale.setVisible(false);
		labelInfoNaissance.setVisible(false);
		choixDateNaissance.setVisible(false);
		labelInfoMutuelle.setVisible(false);
		boxMutuelle.setVisible(false);
		labelInfoMedecin.setVisible(false);
		boxMedecin.setVisible(false);
		labelInfoSpecialiste.setVisible(false);
		boxSpecialiste.setVisible(false);
		labelInfoNumeroAgrement.setVisible(false);
		texteNumeroAgrement.setVisible(false);
		labelInfoSpecialite.setVisible(true);
		boxSpecialite.setVisible(true);
		boutonAjouter.setVisible(false);
		boutonReset.setVisible(false);
		labelListeSpe.setVisible(false);
	}
	
	/**
	 * Procedure pour valider la creation d'une nouvelle personne
	 * @param event : evenement declencheur
	 * @throws IOException : erreur de lecture
	 */
	public void validerEntree(ActionEvent event) throws IOException
	{
		boolean creationOK = false;
		//On met null toutes les variables
		Adresse adresse = null;
		Patient patient = null;
		Medecin medecin = null;
		Specialiste specialiste = null;
		int id = 0;
		
		/*
		 * On prend les elements de l'adresse entree et on verifie
		 * si elle existe dans la BDD.
		 * Si elle existe, on récupère son ID, sinon on crée une nouvelle
		 * adresse dans la BDD et on prend son ID.
		 */
		try {
			
			adresse = new Adresse(0,texteNumRue.getText(),texteNomRue.getText(),
					texteCodePostal.getText(), texteVille.getText());
			id = verifAdresse(adresse);
			adresse.setId(id);
			creationOK=true;
		} catch (EntreeException e1) {
			creationOK = false;
			labelErreur.setText(e1.getMessage());
		}
		if(creationOK==true) //la creation de l'adresse s'est bien passé
		{
			switch(choix)
			{
			case "CLIENT": //cas où on crée un client
				//on modifie l'instance de Client avec les elements entrés
				try {
					patient = new Patient(0, texteNom.getText(), textePrenom.getText(),
							texteEmail.getText(), texteNumeroTelephone.getText(), adresse,
							choixDateNaissance.getValue(),
							texteNumSecuSociale.getText(), boxMedecin.getValue(), listeSpe, boxMutuelle.getValue());
					//On sauvegarde le client dans la BDD
					creationOK = conPatient.creer(patient);
				} catch (EntreeException e) {
					creationOK = false;
					labelErreur.setText(e.getMessage());
				} catch (DAOException e) {
					labelErreur.setText(e.getMessage());
				}
				break;
			case "MEDECIN": //cas où on crée un medecin
				//on modifie l'instance de Medecin avec les elements entrés
				try {
					medecin = new Medecin(0, texteNom.getText(), textePrenom.getText(),
							texteEmail.getText(), texteNumeroTelephone.getText(), adresse, texteNumeroAgrement.getText());
					//On sauvegarde le medecin dans la BDD
					creationOK = conMedecin.creer(medecin);
				} catch (EntreeException e) {
					creationOK = false;
					labelErreur.setText(e.getMessage());
				} catch (DAOException e) {
					labelErreur.setText(e.getMessage());
					creationOK=false;
				}
				break;
			case "SPECIALISTE": //cas où on crée un specialiste
				//on modifie l'instance de Specialiste avec les elements entrés
				try {
					specialiste = new Specialiste(0, texteNom.getText(), textePrenom.getText(),
								texteEmail.getText(), texteNumeroTelephone.getText(), adresse,boxSpecialite.getValue());
					//On sauvegarde le specialiste dans la BDD
					creationOK = conSpecialiste.creer(specialiste);
				} catch (EntreeException e) {
					creationOK = false;
					labelErreur.setText(e.getMessage());
				} catch (DAOException e) {
					creationOK=false;
					labelErreur.setText(e.getMessage());
				}
				break;
			}
		}
		//Si la creation s'est bien passé, on revient sur la page d'accueil
		if(creationOK==true)
		{
			Alert confirm = new Alert(AlertType.INFORMATION);
			confirm.setContentText("Le "+choix+" a bien été crée");
			confirm.show();
			retour(event);
			
		}
	}
	
	/**
	 * Procedure pour ajouter un specialiste au patient
	 */
	public void ajouterSpecialiste()
	{
		//On ajoute à la liste la valeur de la comboBox
		listeSpe.add(boxSpecialiste.getValue());
		//On supprime le spécialiste de la comboBox pour éviter les doublons
		boxSpecialiste.getItems().remove(boxSpecialiste.getValue());
		boxSpecialiste.getSelectionModel().selectFirst();
		//On affiche la liste des spécialiste
		String message = "liste des spécialiste : " + listeSpe;
		labelListeSpe.setText(message);
	}
	
	/**
	 * Procedure pour effacer tous les specialistes du client
	 */
	public void effacerSpecialiste()
	{
		//On efface la liste
		listeSpe.clear();
		//On efface la comboBox
		boxSpecialiste.getItems().clear();
		//O remplit de nouveau la comboBox
		try {
			boxSpecialiste.getItems().addAll(conSpecialiste.chercherTous());
			boxSpecialiste.getSelectionModel().selectFirst();
		} catch (DAOException e) {
			labelErreur.setText(e.getMessage());
		}
		//On efface l'affichage des spécialistes
		String message = "liste des spécialiste : ";
		labelListeSpe.setText(message);
	}
	
	/**
	 * Procedure qui retourne l'ID d'une adresse
	 * @param adresse : adresse ajoutee
	 * @return l'identifiant de l'adresse
	 */
	private int verifAdresse(Adresse adresse)
	{
		//On se connecte à la base de données
		AdresseDAO conAdresse = new AdresseDAO();
		
		int id = 0; //on initialise l'identifiant à 0
	
			//On recherche dans la BDD si l'adresse existe
			try {
				id = conAdresse.verifAdresse(adresse);
			} catch (DAOException e) {
				labelErreur.setText(e.getMessage());
			}
			//Si on n'a pas trouvé l'adresse dans la BDD
			if(id==0)
			{
				try {
					//On ajoute l'adresse entrée dans la BDD
					conAdresse.creer(adresse); 
					/*
					 * Pour avoir l'ID de cette adresse dans la BDD
					 * on refait une recherche d'adresse dans la BDD
					 * en étant sûr que l'adresse existe.
					 * On aura donc son ID sans avoir à créer une nouvelle
					 * fonction.
					 */
					id = conAdresse.verifAdresse(adresse);
				} catch (DAOException e) {
					labelErreur.setText(e.getMessage());
				}	
			}
		return id;
	}
	
	/**
	 * Fonction qui appelle la page precedente
	 * @param event : l'evenement qui ceclenche la fonction
	 * @throws IOException : erreur de lecture
	 */
	public void retour(ActionEvent event) throws IOException
	{
		root = FXMLLoader.load(getClass().getResource("vue/SceneAccueil.fxml"));
		  stage = (Stage)((Node)event.getSource()).getScene().getWindow();
		  scene = new Scene(root);
		  scene.getStylesheets().add(
                  getClass().getResource("application.css").toExternalForm());
		  stage.setScene(scene);
		  stage.show();
	}
	
	/**
	 * Procedure pour quitter la fenetre de l'application
	 * @param event : evenement qui declenche la procedure
	 * @throws IOException ; erreur de lecture
	 */
	public void quitter(ActionEvent event) throws IOException
	{

		Stage fenetreQuitter = new Stage();
		try {
			Parent root = FXMLLoader.load(getClass().getResource("vue/SceneQuitter.fxml"));
			Scene scene = new Scene(root);
			scene.getStylesheets().add(
	                getClass().getResource("application.css").toExternalForm());
			fenetreQuitter.setScene(scene);
			fenetreQuitter.initStyle(StageStyle.UNDECORATED);
			fenetreQuitter.setX(690);
			fenetreQuitter.setY(450);
			fenetreQuitter.show();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	/**
	 * Procedure pour changer la couleur du bouton "Retour"
	 */
	public void boutonRetour()
	{
		boutonRetour.setStyle("-fx-background-color: #FFE4E1;");
		 
	}
	/**
	 * Procedure pour changer la couleur du bouton "Quitter"
	 */
	public void boutonQuitter()
	{
		boutonQuitter.setStyle("-fx-background-color: #FFE4E1;");
	}
	/**
	 * Procedure pour changer la couleur du bouton "Ajouter
	 */
	public void boutonAjouter()
	{
		boutonAjouter.setStyle("-fx-background-color: #FFE4E1;");
	}
	/**
	 * Procedure pour changer la couleur du bouton "Reset"
	 */
	public void boutonReset()
	{
		boutonReset.setStyle("-fx-background-color: #FFE4E1;");
	}
	/**
	 * Procedure pour changer la couleur du bouton "Client"
	 */
	public void boutonClient()
	{
		boutonClient.setStyle("-fx-background-color: #FFE4E1;");
	}
	/**
	 * Procedure pour changer la couleur du bouton "Medecin"
	 */
	public void boutonMedecin()
	{
		boutonMedecin.setStyle("-fx-background-color: #FFE4E1;");
	}
	/**
	 * Procedure pour changer la couleur du bouton "Specialiste"
	 */
	public void boutonSpecialiste()
	{
		boutonSpecialiste.setStyle("-fx-background-color: #FFE4E1;");
	}
	/**
	 * Procedure pour changer la couleur du bouton "Valider"
	 */
	public void boutonValider()
	{
		boutonValider.setStyle("-fx-background-color: #FFE4E1;");
	}
	/**
	 * Procedure pour remettre la couleur des boutons
	 * a la valeur initiale
	 */
	public void boutonOff()
	{
		boutonClient.setStyle("-fx-background-color: #ADD8E6;");
		
		boutonRetour.setStyle("-fx-background-color: #ADD8E6;");
		 
		boutonQuitter.setStyle("-fx-background-color: #ADD8E6;");
		 
		boutonMedecin.setStyle("-fx-background-color: #ADD8E6;");
	
		boutonSpecialiste.setStyle("-fx-background-color: #ADD8E6;");
	
		boutonValider.setStyle("-fx-background-color: #ADD8E6;");
		
		boutonAjouter.setStyle("-fx-background-color: #ADD8E6;");
		
		boutonReset.setStyle("-fx-background-color: #ADD8E6;");
		 
	}

}

package application;

import java.net.URL;
import java.util.ResourceBundle;

import dao.Singleton;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

/**
 * Controleur de la fenetre "Quitter"
 * @author JSublon
 *
 */
public class ControllerQuitter extends Application implements Initializable{

	private Stage fenetreQuitter;
	@FXML
	Button boutonOui;
	@FXML
	Button boutonNon;
	@FXML
	AnchorPane panePrincipal;
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {	
	}
	@Override
	public void start(Stage arg0) throws Exception {	
	}
	
	/**
	 * Methode de validation de la fermeture
	 * @param event : evenement declancheur
	 * @return resultat de l'operation
	 */
	public boolean valider(ActionEvent event)
	{
//		Singleton.getInstanceDB();
//		Singleton.closeInstanceDB();
		Platform.exit();
		return true;
	}
	
	/**
	 * Methode d'annulation de la fermeture
	 * @param event : evenement declancheur
	 * @return resultat de l'operation
	 */
	public boolean annuler(ActionEvent event)
	{
		fenetreQuitter = (Stage) panePrincipal.getScene().getWindow();
		fenetreQuitter.close();
		return false;
	}

	

}

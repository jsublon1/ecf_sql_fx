package dao;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
/**
 * Classe de connexion a la base de donnee
 * @author JSublon
 *
 */
public class Singleton {
	
	private static final String PATHCONF = "conf.properties";
	private static final Properties props = new Properties();
	private static Connection connexion;
	
	/**
	 * Constructeur de la connexion
	 */
	private Singleton()
	{
		try {
			FileInputStream file = new FileInputStream(PATHCONF);
			props.load(file);
			props.setProperty("user", props.getProperty("jdbc.login"));
			props.setProperty("password", props.getProperty("jdbc.password"));
			
			connexion = DriverManager.getConnection(
					props.getProperty("jdbc.url"),
					props);
			
		} catch (IOException | SQLException e) {
			System.out.println("erreur : " + e.getMessage());
		} 
	}

	/**
	 * getter de l'instance de connexion
	 * @return connexion a la BDD
	 */
	public static Connection getInstanceDB()
	{
		if (getConnection() == null)
		{
			new Singleton();
		}
		return getConnection();
	}
	
	/**
	 * Ferme la connexion a la BDD
	 */
	public static void closeInstanceDB()
	{
		try {
			Singleton.getConnection().close();
		} catch (SQLException e) {
			System.out.println("erreur DB : " + e.getMessage());
		}
	}

	/**
	 * getter de la connexion
	 * @return connexion a la BDD
	 */
	private static Connection getConnection() {
		return connexion;
	}
}

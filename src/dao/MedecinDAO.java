package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import exceptions.DAOException;
import exceptions.EntreeException;
import metier.AchatAvecOrdonnance;
import metier.Medecin;
import metier.Patient;

/**
 * Classe DAO pour le médecin
 * @author JSublon
 *
 */
public class MedecinDAO extends DAO<Medecin> {

	@Override
	/**
	 * Ajout d'un medecin dans la BDD
	 * @param obj : medecin a ajouter
	 * @return statut de l'operation
	 */
	public boolean creer(Medecin obj) throws DAOException {
		
		//Si le medecin a ajouter est deja dans la BDD en specialiste
		if(UtilitaireDAO.verifExistant(obj)==-1) throw new DAOException("Cette personne est un spécialiste");
		//Si le medecin n'est pas dans la BDD
		if(UtilitaireDAO.verifExistant(obj)==0)
		{
			StringBuilder insertMedecin = new StringBuilder();
			insertMedecin.append("insert into personne ");
			insertMedecin.append("(ADRESSE_ID, ");
			insertMedecin.append("PERSONNE_NOM, ");
			insertMedecin.append("PERSONNE_PRENOM, ");
			insertMedecin.append("PERSONNE_EMAIL, ");
			insertMedecin.append("PERSONNE_NUMEROTELEPHONE, ");
			insertMedecin.append("PERSONNE_NUMERO_AGREMENT) ");
			insertMedecin.append("values (?, ?, ?, ?, ?, ?)");
			
			try {
				PreparedStatement prepareStatement = this.connexion.prepareStatement(insertMedecin.toString());
				prepareStatement.setInt(1, obj.getAdresse().getId());
				prepareStatement.setString(2, obj.getNom());
				prepareStatement.setString(3, obj.getPrenom());
				prepareStatement.setString(4, obj.getEmail());
				prepareStatement.setString(5, obj.getNumeroTelephone());
				prepareStatement.setString(6, obj.getNumeroAgrement());
				prepareStatement.executeUpdate();
				
				return true;
				
			} catch (SQLException e) {
				System.out.println("erreur base de donnée : connexion médecin 1");
				return false;
			}
		}
		else //le medecin est dans la BDD soit en patient soit en medecin
		{
			try {
				obj.setId(UtilitaireDAO.verifExistant(obj)); //changement de l'id du medecin avec l'id trouve
				return modifier(obj);
			} catch (EntreeException e) {
				throw new DAOException(e.getMessage());
			}
		}
	}

	@Override
	/**
	 * Suppression d'un medecin dans la BDD
	 * @param obj : medecin a supprimer
	 * @return statut de l'operation
	 */
	public boolean effacer(Medecin obj) {
		
		StringBuilder deleteMedecin = new StringBuilder();
		deleteMedecin.append("DELETE FROM personne ");
		deleteMedecin.append("WHERE personne_id = ? ");
		deleteMedecin.append("AND PERSONNE_NUMERO_AGREMENT IS NOT NULL ");
		
		try {
			PreparedStatement prepareStatement = this.connexion.prepareStatement(deleteMedecin.toString());
			prepareStatement.setInt(1, obj.getId());
			prepareStatement.executeUpdate();
			return true;
		} catch (SQLException e) {
			System.out.println("erreur base de donnée : connexion médecin 2");
			return false;
		}
	}

	@Override
	/**
	 * modification d'un medecin dans la BDD
	 * @param obj : medecin a modifier
	 * @return statut de l'operation
	 */
	public boolean modifier(Medecin obj) {
		
		StringBuilder updateMedecin = new StringBuilder();
		updateMedecin.append("UPDATE personne ");
		updateMedecin.append("SET ADRESSE_ID = ?, ");
		updateMedecin.append("PERSONNE_NOM = ?, ");
		updateMedecin.append("PERSONNE_PRENOM = ?, ");
		updateMedecin.append("PERSONNE_EMAIL = ?, ");
		updateMedecin.append("PERSONNE_NUMEROTELEPHONE = ?, ");
		updateMedecin.append("PERSONNE_NUMERO_AGREMENT = ? ");
		updateMedecin.append("WHERE PERSONNE_ID = ?");
		
		try {
			PreparedStatement prepareStatement = this.connexion.prepareStatement(updateMedecin.toString());
			
			prepareStatement.setInt(1, obj.getAdresse().getId());
			prepareStatement.setString(2, obj.getNom());
			prepareStatement.setString(3, obj.getPrenom());
			prepareStatement.setString(4, obj.getEmail());
			prepareStatement.setString(5, obj.getNumeroTelephone());
			prepareStatement.setString(6, obj.getNumeroAgrement() );
			prepareStatement.setInt(7, obj.getId());
			prepareStatement.executeUpdate();
			
			return true;
			
		} catch (SQLException e) {
			System.out.println("erreur base de donnée : connexion médecin 3");
			return false;
		}
	}

	@Override
	/**
	 * creation d'une instance de Medecin a partir de la BDD
	 * @param pId : id du medecin de la BDD
	 * @return medecin
	 * @throws DAOException 
	 */
	public Medecin chercher(Integer pId) throws DAOException {
		
		AdresseDAO conAdresse = new AdresseDAO();
		Medecin docteur = null;
		StringBuilder chercherMedecin = new StringBuilder();
		chercherMedecin.append("SELECT * FROM personne ");
		chercherMedecin.append("WHERE personne_id = ? AND ");
		chercherMedecin.append("PERSONNE_NUMERO_AGREMENT IS NOT NULL");
		
		if (verifMedecin(pId)==true) //le medecin est dans la BDD
		{
		try {
			PreparedStatement requeteChercher = this.connexion.prepareStatement(chercherMedecin.toString());
			requeteChercher.setInt(1, pId);
			ResultSet res = requeteChercher.executeQuery();
			
			while (res.next()) {
				try {
					docteur = new Medecin(
							res.getInt("PERSONNE_ID"), 
							res.getString("PERSONNE_NOM"),
							res.getString("PERSONNE_PRENOM"),
							res.getString("PERSONNE_EMAIL"),
							res.getString("PERSONNE_NUMEROTELEPHONE"), 
							conAdresse.chercher(res.getInt("ADRESSE_ID")),
							res.getString("PERSONNE_NUMERO_AGREMENT"));	
					} catch (EntreeException e) {
						throw new DAOException(e.getMessage());
					}
			}
			
			} catch (SQLException e) {
				System.out.println("erreur base de donnée : connexion médecin 4");
			}
		}
		else
		{
			throw new DAOException("L'identifiant donné ne correspond pas à un medecin");
		}
		
		return docteur;
	}

	@Override
	/**
	 * creation de la liste de tous les medecins
	 * @return liste des medecins
	 * @throws DAOException 
	 */
	public ArrayList<Medecin> chercherTous() throws DAOException {
		
		AdresseDAO conAdresse = new AdresseDAO();
		ArrayList<Medecin> list = new ArrayList<>();
		
		StringBuilder chercherMedecin = new StringBuilder();
		chercherMedecin.append("SELECT * FROM personne ");
		chercherMedecin.append("WHERE PERSONNE_NUMERO_AGREMENT IS NOT NULL");

		try {
			Statement requeteAll = this.connexion.createStatement();
			ResultSet res = requeteAll.executeQuery(chercherMedecin.toString());
			
			while (res.next()) {
				try {
					Medecin medecin = new Medecin(
							res.getInt("PERSONNE_ID"), 
							res.getString("PERSONNE_NOM"),
							res.getString("PERSONNE_PRENOM"),
							res.getString("PERSONNE_EMAIL"),
							res.getString("PERSONNE_NUMEROTELEPHONE"), 
							conAdresse.chercher(res.getInt("ADRESSE_ID")),
							res.getString("PERSONNE_NUMERO_AGREMENT"));			
					list.add(medecin);
				} catch (EntreeException e) {
					throw new DAOException(e.getMessage());
				}
			}
			
			return list;
		} catch (SQLException e) {
			System.out.println("erreur base de donnée : connexion médecin 6");
			return null;
		}
	}
	
	/**
	 * verification que l'id donnee est celle d'un medecin
	 * @param pId : identifiant du medecin entrant
	 * @return statut de l'operation
	 */
	public boolean verifMedecin(int pId)
	{
		boolean trouve = false;
		StringBuilder chercherMedecin = new StringBuilder();
		
		//requete pour voir tous les medecins
		chercherMedecin.append("SELECT * FROM personne ");
		chercherMedecin.append("WHERE PERSONNE_NUMERO_AGREMENT IS NOT NULL");
		
		try {
			Statement requeteAll = this.connexion.createStatement();
			ResultSet res = requeteAll.executeQuery(chercherMedecin.toString());
			//comparaison de chaque id trouve avec celle donnee
			while (res.next())
			{
				if (pId == res.getInt("PERSONNE_ID"))
				{
					trouve = true;
				}
			}
		} catch (SQLException e) {
			System.out.println("erreur base de donnée : connexion médecin 7");
		}
		
		return trouve;	
	}
	
	/**
	 * fonction pour recuperer la liste des patients d'un medecin
	 * @param pId : id du medecin
	 * @return liste des patients
	 * @throws DAOException : erreur utilisation BDD
	 */
	public ArrayList<Patient> getListePatient(int pId) throws DAOException
	{
		PatientDAO conPatient = new PatientDAO();
		ArrayList<Patient> liste = new ArrayList<Patient>();
		
		StringBuilder chercherPatient = new StringBuilder();
		chercherPatient.append("SELECT personne_id FROM personne ");
		chercherPatient.append("WHERE medecin_id = ? ");
		
		try {
			PreparedStatement requetePatient = this.connexion.prepareStatement(chercherPatient.toString());
			requetePatient.setInt(1, pId);
			ResultSet res = requetePatient.executeQuery();
			while(res.next())
			{
				Patient patient = conPatient.chercher(res.getInt("personne_id"));
				liste.add(patient);
			}
		} catch (SQLException e) {
			System.out.println("erreur base de donnée : connexion médecin 8");
		}
		return liste;
	}
	
	/**
	 * fonction pour obtenir toutes les ordonnances d'un medecin
	 * @param pId : id du medecin
	 * @return liste des ordonnances
	 * @throws DAOException : erreur BDD
	 */
	public ArrayList<AchatAvecOrdonnance> getListeOrdonnance(int pId) throws DAOException
	{
		AchatDAO conOrdo = new AchatDAO();
		ArrayList<AchatAvecOrdonnance> liste = new ArrayList<AchatAvecOrdonnance>();
		
		StringBuilder chercherOrdo = new StringBuilder();
		chercherOrdo.append("SELECT achat_id FROM achat ");
		chercherOrdo.append("WHERE medecin_id = ? ");
		
		try {
			PreparedStatement requetePatient = this.connexion.prepareStatement(chercherOrdo.toString());
			requetePatient.setInt(1, pId);
			ResultSet res = requetePatient.executeQuery();
			while(res.next())
			{
				AchatAvecOrdonnance ordonnance = (AchatAvecOrdonnance) conOrdo.chercher(res.getInt("achat_id"));
				liste.add(ordonnance);
			}
		} catch (SQLException e) {
			System.out.println("erreur base de donnée : connexion médecin 9");
		}
		return liste;
	}
}

package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import exceptions.DAOException;
import exceptions.EntreeException;
import metier.Specialiste;

/**
 * Classe DAO pour le specialiste
 * @author JSublon
 *
 */
public class SpecialisteDAO extends DAO<Specialiste> {

	@Override
	/**
	 * sauvegarde d'un specialiste dans la base de données
	 * @param obj : specialiste
	 */
	public boolean creer(Specialiste obj) throws DAOException {
		
		//vérification que le spécialiste entrant n'est pas déjà un médecin
		if(UtilitaireDAO.verifExistant(obj)==-1) throw new DAOException("Cette personne est un médecin");
		//vérification que le spécialiste entrant n'est pas dans la base de données
		if(UtilitaireDAO.verifExistant(obj)==0)
		{
			StringBuilder insertSpecialiste = new StringBuilder();
			insertSpecialiste.append("insert into personne ");
			insertSpecialiste.append("(ADRESSE_ID, ");
			insertSpecialiste.append("PERSONNE_NOM, ");
			insertSpecialiste.append("PERSONNE_PRENOM, ");
			insertSpecialiste.append("PERSONNE_EMAIL, ");
			insertSpecialiste.append("PERSONNE_NUMEROTELEPHONE, ");
			insertSpecialiste.append("SPECIALITE_ID) ");
			insertSpecialiste.append("values (?, ?, ?, ?, ?, ?)");
			
			try {
				PreparedStatement prepareStatement = this.connexion.prepareStatement(insertSpecialiste.toString());
				prepareStatement.setInt(1, obj.getAdresse().getId());
				prepareStatement.setString(2, obj.getNom());
				prepareStatement.setString(3, obj.getPrenom());
				prepareStatement.setString(4, obj.getEmail());
				prepareStatement.setString(5, obj.getNumeroTelephone());
				prepareStatement.setInt(6, chercherIDType(obj.getSpecialite()));
				prepareStatement.executeUpdate();
				
				return true;
				
			} catch (SQLException e) {
				System.out.println("erreur base de donnée : connexion spécialiste 1 ");
				return false;
			}
		}
		else
			{
			try {
				//Changement de l'ID du spécialiste avec celui trouvé
				obj.setId(UtilitaireDAO.verifExistant(obj));
				//modification du spécialiste existant
				return modifier(obj);
			} catch (EntreeException e) {
				System.out.println("erreur base de donnée : connexion spécialiste 13" + e.getMessage());
				return false;
			}
			}
	}

	@Override
	/**
	 * suppression du specialiste dans la bse de donnée
	 */
	public boolean effacer(Specialiste obj) {
		
		StringBuilder deleteSpecialiste = new StringBuilder();
		deleteSpecialiste.append("DELETE FROM personne ");
		deleteSpecialiste.append("WHERE personne_id = ? ");
		deleteSpecialiste.append("AND specialite_id IS NOT NULL");
		
		try {
			PreparedStatement prepareStatement = this.connexion.prepareStatement(deleteSpecialiste.toString());
			prepareStatement.setInt(1, obj.getId());
			prepareStatement.executeUpdate();
			return true;
			
		} catch (SQLException e) {
			System.out.println("erreur base de donnée : connexion spécialiste 2 ");
			return false;
		}
	}

	@Override
	/**
	 * modification du psecialiste dans la base de données
	 */
	public boolean modifier(Specialiste obj) {
		
		StringBuilder updateSpecialiste = new StringBuilder();
		updateSpecialiste.append("UPDATE personne ");
		updateSpecialiste.append("SET ADRESSE_ID = ?, ");
		updateSpecialiste.append("PERSONNE_NOM = ?, ");
		updateSpecialiste.append("PERSONNE_PRENOM = ?, ");
		updateSpecialiste.append("PERSONNE_EMAIL = ?, ");
		updateSpecialiste.append("PERSONNE_NUMEROTELEPHONE = ?, ");
		updateSpecialiste.append("SPECIALITE_ID = ? ");
		updateSpecialiste.append("WHERE PERSONNE_ID = ?");
		
		try {
			PreparedStatement prepareStatement = this.connexion.prepareStatement(updateSpecialiste.toString());
			
			prepareStatement.setInt(1, obj.getAdresse().getId());
			prepareStatement.setString(2, obj.getNom());
			prepareStatement.setString(3, obj.getPrenom());
			prepareStatement.setString(4, obj.getEmail());
			prepareStatement.setString(5, obj.getNumeroTelephone());
			prepareStatement.setInt(6, chercherIDType(obj.getSpecialite()));
			prepareStatement.setInt(7, obj.getId());
			prepareStatement.executeUpdate();
			return true;
			
		} catch (SQLException e) {
			System.out.println("erreur base de donnée : connexion spécialiste 3 "+e.getMessage());
			return false;
		}
	}

	@Override
	/**
	 * creation d'une instance Specialiste a partir de la base de données
	 * @throws DAOException 
	 */
	public Specialiste chercher(Integer pId) throws DAOException {
		
		AdresseDAO conAdresse = new AdresseDAO();
		Specialiste docteur = null;
		StringBuilder chercherSpecialiste = new StringBuilder();
		chercherSpecialiste.append("SELECT * FROM personne ");
		chercherSpecialiste.append("WHERE personne_id = ?");

		//vérification que l'ID entrée correspond a un specialiste
		if (verifSpecialiste(pId)==true)
		{
		try {
			PreparedStatement requeteChercher = this.connexion.prepareStatement(chercherSpecialiste.toString());
			requeteChercher.setInt(1, pId);
			ResultSet res = requeteChercher.executeQuery();
			
			while (res.next()) {
				try {
					docteur = new Specialiste(
							res.getInt("PERSONNE_ID"), 
							res.getString("PERSONNE_NOM"),
							res.getString("PERSONNE_PRENOM"),
							res.getString("PERSONNE_EMAIL"),
							res.getString("PERSONNE_NUMEROTELEPHONE"), 
							conAdresse.chercher(res.getInt("ADRESSE_ID")),
							chercherNomType(res.getInt("SPECIALITE_ID")));		
					} catch (EntreeException e) {
						throw new DAOException(e.getMessage());
					}
			}
			
			} catch (SQLException e) {
				System.out.println("erreur base de donnée : connexion spécialiste 5 ");
			}
		}
		else
		{
			throw new DAOException("L'identifiant donné ne correspond pas à un specialiste");
		}
		return docteur;
	}

	@Override
	/**
	 * creation d'une liste de tous les specialistes
	 * @throws DAOException 
	 */
	public ArrayList<Specialiste> chercherTous() throws DAOException {
		
		AdresseDAO conAdresse = new AdresseDAO();
		ArrayList<Specialiste> list = new ArrayList<>(); //liste des spécialistes
		StringBuilder chercherSpecialiste = new StringBuilder();
		chercherSpecialiste.append("SELECT * FROM personne ");
		chercherSpecialiste.append("WHERE SPECIALITE_ID IS NOT NULL");

		try {
			Statement requeteAll = this.connexion.createStatement();
			ResultSet res = requeteAll.executeQuery(chercherSpecialiste.toString());
			
			while (res.next()) {
				try {
					//creation d'un spécialiste trouvé
					Specialiste specialiste = new Specialiste(
							res.getInt("PERSONNE_ID"), 
							res.getString("PERSONNE_NOM"),
							res.getString("PERSONNE_PRENOM"),
							res.getString("PERSONNE_EMAIL"),
							res.getString("PERSONNE_NUMEROTELEPHONE"), 
							conAdresse.chercher(res.getInt("ADRESSE_ID")),
							chercherNomType(res.getInt("SPECIALITE_ID")));
					//ajout du spécialiste dans la liste
					list.add(specialiste);

				} catch (EntreeException e) {
					throw new DAOException(e.getMessage());
				}
			}
			return list;
			
		} catch (SQLException e) {
			System.out.println("erreur base de donnée : connexion spécialiste 7 ");
			return null;
		}
	}
	
	/**
	 * methode pour verifier l'existance d'un specialiste
	 * @param pId : identifiant de la personne a verifier
	 * @return resultat de l'operation
	 */
	public boolean verifSpecialiste(int pId)
	{
		boolean trouve = false; //booléen pour savoir si la spécialité est trouvé
		StringBuilder chercherSpecialiste = new StringBuilder();
		
		chercherSpecialiste.append("SELECT * FROM personne ");
		chercherSpecialiste.append("WHERE SPECIALITE_ID IS NOT NULL");
		
		try {
			Statement requeteAll = this.connexion.createStatement();
			ResultSet res = requeteAll.executeQuery(chercherSpecialiste.toString());
			while (res.next())
			{
				if (pId == res.getInt("PERSONNE_ID")) //la spécialité est trouvé
				{
					trouve = true;
				}
			}
		} catch (SQLException e) {
			System.out.println("erreur base de donnée : connexion spécialiste 8 ");
		}
		return trouve;
	}

	/**
	 * Recherche de l'ID d'une specialite par son nom
	 * @param nom : nom de la specialite
	 * @return l'id de la specialite
	 */
	public int chercherIDType(String nom)
	{
		int resultat=0; //déclaration de l'id
		StringBuilder chercherID = new StringBuilder();
		chercherID.append("SELECT SPECIALITE_ID FROM specialite ");
		chercherID.append("WHERE SPECIALITE_NOM = ?");
		
		try {
			PreparedStatement requeteChercher = this.connexion.prepareStatement(chercherID.toString());
			requeteChercher.setString(1,  nom);
			ResultSet res = requeteChercher.executeQuery();
			while (res.next())
			{
				resultat = res.getInt("SPECIALITE_ID"); //l'id est enregistré
			}
			
		} catch (SQLException e) {
			System.out.println("erreur base de donnée : connexion spécialiste 9 ");
		}
		return resultat;
	}
	
	/**
	 * Recherche du nom de la specialite avec l'id
	 * @param id : identifiant de la specialite
	 * @return nom de la specialite
	 */
	public String chercherNomType(int id)
	{
		String resultat = null; //declaration de la variable qui contiendra le nom
		StringBuilder chercherID = new StringBuilder();
		chercherID.append("SELECT SPECIALITE_NOM FROM specialite ");
		chercherID.append("WHERE SPECIALITE_ID = ?");
		
		try {
			PreparedStatement requeteChercher = this.connexion.prepareStatement(chercherID.toString());
			requeteChercher.setInt(1,  id);
			ResultSet res = requeteChercher.executeQuery();
			while (res.next())
			{
				resultat = res.getString("SPECIALITE_NOM"); //enregistrement du nom dans la variable
			}
		} catch (SQLException e) {
			System.out.println("erreur base de donnée : connexion spécialiste 10 ");
			
		}
		return resultat;
	}
	/**
	 * Recherche des specialistes d'un patient
	 * @param pId : identifiant du patient
	 * @return liste des specialistes
	 * @throws DAOException : erreur BDD
	 */
	public ArrayList<Specialiste> chercherSpecialisteDuPatient(int pId) throws DAOException {
		
		ArrayList<Specialiste> list = new ArrayList<>(); //liste des spécialistes
		StringBuilder chercherSpecialiste = new StringBuilder();
		chercherSpecialiste.append("SELECT * FROM specialiste ");
		chercherSpecialiste.append("WHERE CLIENT_ID = ?; ");

		try {
			PreparedStatement requeteAll = this.connexion.prepareStatement(chercherSpecialiste.toString());
			requeteAll.setInt(1, pId);
			ResultSet res = requeteAll.executeQuery();
			
			while (res.next()) {
				Specialiste specialiste = chercher(res.getInt("SPECIALISTE_ID")); //création d'un spécialiste à partir de la BDD
				list.add(specialiste); //ajout du spécialiste dans la liste
			}
			return list;		
		} catch (SQLException e) {
			System.out.println("erreur base de donnée : connexion spécialiste 11 ");
			return null;
		}
	}
	/**
	 * Recherche de toutes les specialites pour un specialiste
	 * @return liste des specialites
	 */
	public ArrayList<String> cherSpecialiteTous()
	{
		ArrayList<String> list = new ArrayList<>(); //liste des spécialites
		
		String chercherSpecialite = "SELECT SPECIALITE_NOM FROM specialite;";
		
		try {
			Statement requeteAll = this.connexion.createStatement();
			ResultSet res = requeteAll.executeQuery(chercherSpecialite);
			
			while(res.next())
			{
				list.add(res.getString("specialite_nom")); //ajout du nom dans la liste
			}
		} catch (SQLException e) {
			System.out.println("erreur base de donnée : connexion spécialiste 12 ");
		}	
		return list;
	}
}

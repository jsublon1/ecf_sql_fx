package dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import exceptions.DAOException;
import metier.Personne;
/**
 * Classe des fonctions utiles dans la DAO
 * @author JSublon
 *
 */
public class UtilitaireDAO {
	
	/**
	 * verification qu'une personne est deja dans la BDD
	 * @param personne : personne voulant etre cree
	 * @return identifiant
	 * @throws DAOException : erreur d'utilisation de la BDD
	 */
	public static int verifExistant(Personne personne) throws DAOException
	{
		//appel des DAO
		AdresseDAO conAdresse = new AdresseDAO();
		SpecialisteDAO conSpecialiste = new SpecialisteDAO();
		PatientDAO conPatient = new PatientDAO();
		MedecinDAO conMedecin = new MedecinDAO();
		
		int identifiant = 0; //identifiant de la BDD
		String classe =""; //variable pour retenir le nom d'une classe d'un objet
		ArrayList<Personne> list = new ArrayList<>(); //liste des personnes de la BDD
		list.addAll(conPatient.chercherTous()); //ajout des patients dans la liste
		list.addAll(conMedecin.chercherTous()); //ajout des medcins dans la liste
		list.addAll(conSpecialiste.chercherTous()); // ajout des specialistes dans la liste
		
		//boucle pour verifier chaque element de la liste
		for(Personne temp : list)
		{
			//Comparaison du nom, prenom, email, numero de telephone et de la'adresse
			if (personne.getNom().toUpperCase().equals(temp.getNom().toUpperCase())
					&& personne.getPrenom().equals(temp.getPrenom())
					&& personne.getEmail().toUpperCase().equals(temp.getEmail().toUpperCase())
					&& personne.getNumeroTelephone().toUpperCase().equals(temp.getNumeroTelephone().toUpperCase())
					&& conAdresse.verifAdresse(personne.getAdresse())==conAdresse.verifAdresse(temp.getAdresse()))
			{			
				classe=temp.getClass().getName().replaceAll("metier.", ""); //enregistrement de la classe de l'objet trouve
				identifiant = temp.getId(); //enregistrement de l'identifiant trouve
			}
		}
		if(identifiant!=0) //personne dans la BDD
		{
			//Le specialiste est deja medecin
			if(personne.getClass().getName().replaceAll("metier.", "").equals("Specialiste")&&classe.equals("Medecin"))
			{
				return -1;
			}
			//Le medecin est deja specialiste
			if(personne.getClass().getName().replaceAll("metier.", "").equals("Medecin")&&classe.equals("Specialiste"))
			{
				return -1;
			}
			return identifiant;
			
		}
		else
			{
			return identifiant;
			}
	}
	
	public static int dernierIdEntree()
	{
		int id = 0;
		Connection connexion = Singleton.getInstanceDB();
		String derID = "SELECT LAST_INSERT_ID();";
		
		try {
			Statement requeteId = connexion.createStatement();
			ResultSet res = requeteId.executeQuery(derID);
			while(res.next())
			{
				id = res.getInt("LAST_INSERT_ID()");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return id;
	}
}

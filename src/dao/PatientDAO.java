package dao;

import java.sql.PreparedStatement;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;

import exceptions.DAOException;
import exceptions.EntreeException;
import metier.Patient;
import metier.Specialiste;

/**
 * Classe DAO pour le patient
 * @author JSublon
 *
 */
public class PatientDAO extends DAO<Patient> {

	
	@Override
	/**
	 * creation d'un patient dans la BDD
	 * @throws EntreeException 
	 */
	public boolean creer(Patient obj) throws DAOException, EntreeException {
		
		if(UtilitaireDAO.verifExistant(obj)==0) //La personne n'est pas dans la BDD
		{
			StringBuilder insertPatient = new StringBuilder();
			insertPatient.append("insert into personne ");
			insertPatient.append("(ADRESSE_ID, ");
			insertPatient.append("MEDECIN_ID, MUTUELLE_ID, PERSONNE_NOM, ");
			insertPatient.append("PERSONNE_PRENOM, PERSONNE_EMAIL, ");
			insertPatient.append("PERSONNE_NUMEROTELEPHONE, PERSONNE_DATENAISSANCE, ");
			insertPatient.append("PERSONNE_NUMERO_SECURITE_SOCIALE) ");
			insertPatient.append("values (?, ?, ?, ?, ?, ?, ?, ?, ?)");
			
			try {
				PreparedStatement prepareStatement = this.connexion.prepareStatement(insertPatient.toString());
				prepareStatement.setInt(1, obj.getAdresse().getId());
				prepareStatement.setInt(2, obj.getDocteur().getId());
				prepareStatement.setInt(3, obj.getMutuelle().getId());
				prepareStatement.setString(4, obj.getNom());
				prepareStatement.setString(5, obj.getPrenom());
				prepareStatement.setString(6, obj.getEmail());
				prepareStatement.setString(7, obj.getNumeroTelephone());
				prepareStatement.setString(8, obj.dateToString());
				prepareStatement.setString(9, obj.getNumeroDeSecuriteSociale());
				prepareStatement.executeUpdate();
				obj.setId(UtilitaireDAO.dernierIdEntree());
				//ajout des specialistes du patient dans la BDD
				ajouterSpecialiste(obj.getListeSpecialiste(), obj.getId());
				
				return true;
				
			} catch (SQLException e) {
				System.out.println(e.getMessage());
				return false;
			}
		}
		else
		{
			try {
				obj.setId(UtilitaireDAO.verifExistant(obj)); //changement de l'id du patient pour correspondre a celui de la BDD
				return modifier(obj); //modifier la personne dans la BDD pour ajouter les champs correspondant au patient
			} catch (EntreeException e) {
				throw new DAOException(e.getMessage());
			}
		}	
	}

	@Override
	/**
	 * suppression du patient dans la BDD
	 */
	public boolean effacer(Patient obj) {
		
		StringBuilder deletePatient = new StringBuilder();
		deletePatient.append("DELETE FROM personne ");
		deletePatient.append("WHERE personne_id = ? ");
		deletePatient.append("AND PERSONNE_NUMERO_SECURITE_SOCIALE IS NOT NULL");
		
		try {
			PreparedStatement prepareStatement = this.connexion.prepareStatement(deletePatient.toString());
			prepareStatement.setInt(1, obj.getId());
			prepareStatement.executeUpdate();
			return true;
		} catch (SQLException e) {
			System.out.println("erreur base de donnée : connexion patient 2 ");
			return false;
		}
		
	}

	@Override
	/**
	 * modification du patient dans la BDD a partir d'une instance de Patient
	 */
	public boolean modifier(Patient obj) {
		
		
		effacerListeSpecialiste(obj.getId()); //suppression des specialistes du patient dans la BDD
		
		StringBuilder updatePatient = new StringBuilder();
		updatePatient.append("UPDATE personne\n");
		updatePatient.append("SET ADRESSE_ID = ?,\n");
		updatePatient.append("MEDECIN_ID = ?,\n");
		updatePatient.append("MUTUELLE_ID = ?,\n");
		updatePatient.append("PERSONNE_NOM = ?,\n");
		updatePatient.append("PERSONNE_PRENOM = ?,\n");
		updatePatient.append("PERSONNE_EMAIL = ?,\n");
		updatePatient.append("PERSONNE_NUMEROTELEPHONE = ?,\n");
		updatePatient.append("PERSONNE_DATENAISSANCE = ?,\n");
		updatePatient.append("PERSONNE_NUMERO_SECURITE_SOCIALE = ?\n");
		updatePatient.append("WHERE PERSONNE_ID = ?;");
		
		
		try {
			PreparedStatement prepareStatement = this.connexion.prepareStatement(updatePatient.toString());
			
			prepareStatement.setInt(1, obj.getAdresse().getId());
			prepareStatement.setInt(2, obj.getDocteur().getId());
			prepareStatement.setInt(3, obj.getMutuelle().getId());
			prepareStatement.setString(4, obj.getNom());
			prepareStatement.setString(5, obj.getPrenom());
			prepareStatement.setString(6, obj.getEmail());
			prepareStatement.setString(7, obj.getNumeroTelephone());
			prepareStatement.setString(8, obj.dateToString());
			prepareStatement.setString(9, obj.getNumeroDeSecuriteSociale());
			prepareStatement.setInt(10, obj.getId());
			prepareStatement.executeUpdate();
			
			ajouterSpecialiste(obj.getListeSpecialiste(), obj.getId()); //ajout des nouveaux specialistes du patient dans la BDD

			return true;
		} catch (SQLException e) {
			System.out.println("erreur base de donnée : connexion patient 3 ");
			return false;
		}
		
	}

	@Override
	/**
	 * creation d'une instance Patient a partir de la BDD
	 * @throws DAOException 
	 */
	public Patient chercher(Integer pId) throws DAOException {
		
		//Appel des DAO
		AdresseDAO conAdresse = new AdresseDAO();
		MedecinDAO conMedecin = new MedecinDAO();
		MutuelleDAO conMutuelle = new MutuelleDAO();
		Patient patient = null;
		
		StringBuilder chercherPatient = new StringBuilder();
		chercherPatient.append("SELECT * FROM personne ");
		chercherPatient.append("WHERE PERSONNE_NUMERO_SECURITE_SOCIALE IS NOT NULL ");
		chercherPatient.append("AND PERSONNE_ID = ?;");

		try {
			PreparedStatement requetePatient = this.connexion.prepareStatement(chercherPatient.toString());
			requetePatient.setInt(1,pId);
			ResultSet res = requetePatient.executeQuery();
			
			while (res.next()) {
				try {
					//Creation d'un patient a partir de l'ID
					patient = new Patient(
							res.getInt("PERSONNE_ID"), 
							res.getString("PERSONNE_NOM"),
							res.getString("PERSONNE_PRENOM"),
							res.getString("PERSONNE_EMAIL"),
							res.getString("PERSONNE_NUMEROTELEPHONE"), 
							conAdresse.chercher(res.getInt("ADRESSE_ID")),
							LocalDate.parse(res.getString("PERSONNE_DATENAISSANCE")),
							res.getString("PERSONNE_NUMERO_SECURITE_SOCIALE"),
							conMedecin.chercher(res.getInt("MEDECIN_ID")), 
							conMutuelle.chercher(res.getInt("MUTUELLE_ID")));
					//ajout de la liste des specialistes dans l'instance venant d'etre cree
					patient.setListeSpecialiste(chercherSpecialiste(patient.getId()));
				
				} catch (EntreeException e) {
					throw new DAOException(e.getMessage());
				}
			}			
			return patient;
		} catch (SQLException e) {
			System.out.println("erreur base de donnée : connexion patient 5 ");
			return null;
		}
	}

	@Override
	/**
	 * creation d'une liste de tous les patients
	 * @throws DAOException 
	 */
	public ArrayList<Patient> chercherTous() throws DAOException {
		//Appel des DAO
		AdresseDAO conAdresse = new AdresseDAO();
		MedecinDAO conMedecin = new MedecinDAO();
		MutuelleDAO conMutuelle = new MutuelleDAO();
		//declaration de la liste des patients
		ArrayList<Patient> list = new ArrayList<>();
		
		StringBuilder chercherPatient = new StringBuilder();
		chercherPatient.append("SELECT * FROM personne ");
		chercherPatient.append("WHERE PERSONNE_NUMERO_SECURITE_SOCIALE IS NOT NULL");

		try {
			Statement requeteAll = this.connexion.createStatement();
			ResultSet res = requeteAll.executeQuery(chercherPatient.toString());
			
			while (res.next()) {
				try {
					//creation d'un patient
					Patient patient = new Patient(
							res.getInt("PERSONNE_ID"), 
							res.getString("PERSONNE_NOM"),
							res.getString("PERSONNE_PRENOM"),
							res.getString("PERSONNE_EMAIL"),
							res.getString("PERSONNE_NUMEROTELEPHONE"), 
							conAdresse.chercher(res.getInt("ADRESSE_ID")),
							LocalDate.parse(res.getString("PERSONNE_DATENAISSANCE")),
							res.getString("PERSONNE_NUMERO_SECURITE_SOCIALE"),
							conMedecin.chercher(res.getInt("MEDECIN_ID")), 
							conMutuelle.chercher(res.getInt("MUTUELLE_ID")));
					//ajout des specialistes du patient
					patient.setListeSpecialiste(chercherSpecialiste(patient.getId()));
					//ajout du patient dans la liste
					list.add(patient);
				} catch (EntreeException e) {
					throw new DAOException(e.getMessage());
				}
			}			
			return list;
		} catch (SQLException e) {
			System.out.println("erreur base de donnée : connexion patient 7 ");
			return null;
		}
	}

	
	/**
	 * procedure pour ajouter une liste de specialistes dans la BDD pour un patient donnee
	 * @param listSpecialiste : liste des specialistes du patient
	 * @param idPatient : l'id du patient
	 * @return resultat de l'operation
	 */
	public boolean ajouterSpecialiste(ArrayList<Specialiste> listSpecialiste, int idPatient)
	{
		boolean erreur=false;
		/*Pour chaque specialiste du patient, on ajoute dans la table specialiste
		 * l'id du specialiste et l'id du patient
		 */
		for(Specialiste spe : listSpecialiste)
		{
			StringBuilder ajoutSpecialiste = new StringBuilder();
			ajoutSpecialiste.append("INSERT INTO specialiste (");
			ajoutSpecialiste.append("client_id, ");
			ajoutSpecialiste.append("specialiste_id) ");
			ajoutSpecialiste.append("VALUES(?, ?);");
			
			try {
				PreparedStatement requeteAjoutSpecialiste = this.connexion.prepareStatement(ajoutSpecialiste.toString());
				requeteAjoutSpecialiste.setInt(1, idPatient);
				requeteAjoutSpecialiste.setInt(2, spe.getId());
				requeteAjoutSpecialiste.executeUpdate();
				
				
			} catch (SQLException e) {
				System.out.println("erreur base de donnée : connexion patient 9 ");
				erreur=true;
			}
		}
		return erreur;
	}
	
	/**
	 * Fonction pour chercher les specialistes d'un patient dans la table
	 * specialiste de la BDD
	 * @param pId : id du patient
	 * @return liste de specialistes
	 * @throws DAOException  erreur BDD
	 */
	public ArrayList<Specialiste> chercherSpecialiste(int pId) throws DAOException
	{
		SpecialisteDAO conSpecialiste = new SpecialisteDAO();
		ArrayList<Specialiste> list = new ArrayList<>();
		
		/*La requete cherche tous les id 
		 * qui sont associe avec l'id du patient dans la table specialiste
		 */
		StringBuilder rechercheSpecialiste = new StringBuilder();
		rechercheSpecialiste.append("SELECT specialiste_id FROM specialiste ");
		rechercheSpecialiste.append("WHERE client_id = ? ;");
		
		PreparedStatement requeteSpecialiste;
		try {
			requeteSpecialiste = this.connexion.prepareStatement(rechercheSpecialiste.toString());
			requeteSpecialiste.setInt(1,pId);
			ResultSet res = requeteSpecialiste.executeQuery();
			
			while(res.next())
			{
				/*l'id obtenu est utilise pour creer un specialiste
				 * qui est ajoute a la liste des specialistes
				 */
				list.add(conSpecialiste.chercher(res.getInt("SPECIALISTE_ID")));
			}
		} catch (SQLException e) {
			System.out.println("erreur base de donnée : connexion patient 10 ");
		}
		return list;
	}
	
	/**
	 * procedure pour effacer les entree de la table specialiste
	 * qui ont pour id client celui donne
	 * @param pId : id du patient
	 * @return resultat de l'operation
	 */
	public boolean effacerListeSpecialiste(int pId)
	{
		StringBuilder effacerSpecialiste = new StringBuilder();
		effacerSpecialiste.append("DELETE FROM specialiste ");
		effacerSpecialiste.append("WHERE client_id = ?;");
		PreparedStatement requeteEffacer;
		
		try {
			requeteEffacer = this.connexion.prepareStatement(effacerSpecialiste.toString());
			requeteEffacer.setInt(1, pId);
			requeteEffacer.executeUpdate();
			return true;
		} catch (SQLException e) {
			System.out.println("erreur base de donnée : connexion patient 11 ");
			return false;
		}	
	}
}

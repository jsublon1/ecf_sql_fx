package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;

import exceptions.DAOException;
import exceptions.EntreeException;
import metier.Medicament;

/**
 * Classe DAO pour le medicament
 * @author JSublon
 *
 */
public class MedicamentDAO extends DAO<Medicament> {

	@Override
	/**
	 * Ajout d'un medicament dans la BDD
	 * @param obj : medicament a ajouter
	 */
	public boolean creer(Medicament obj) throws DAOException {
		
		StringBuilder creerMedicament = new StringBuilder();
		creerMedicament.append("INSERT INTO medicament ");
		creerMedicament.append("(TYPEMEDICAMENT_ID, MEDICAMENT_NOM, ");
		creerMedicament.append("MEDICAMENT_PRIX, MEDICAMENT_DATEMISESERVICE, ");
		creerMedicament.append("MEDICAMENT_STOCK)");
		creerMedicament.append("values (?, ?, ?, ?, ?)");
		//Recherche de l'id du type de medicament
		int id = chercherIDType(obj.getCategorie());
		if (id>0) //l'id est trouve
			{
				try {
					PreparedStatement requeteCreer = this.connexion.prepareStatement(creerMedicament.toString());
					requeteCreer.setInt(1,id);
					requeteCreer.setString(2, obj.getNom());
					requeteCreer.setFloat(3, (float) obj.getPrix());
					requeteCreer.setString(4, obj.dateToString());
					requeteCreer.setInt(5,obj.getQuantite());
					requeteCreer.executeUpdate();
					return true;
				} catch (SQLException e) {
					System.out.println("erreur base de donnée : connexion médicament 1");
					return false;
				}
			}
		else
		{
			throw new DAOException("Le type de médicament n'existe pas");
		}
	}

	@Override
	public boolean effacer(Medicament obj) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean modifier(Medicament obj) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	/**
	 * Creation d'une instance Medicament d'un medicament a partir de son ID
	 * @param pId : identifiant du medicament
	 * @throws DAOException 
	 */
	public Medicament chercher(Integer pId) throws DAOException {
		
		Medicament medicament = null;
		
		StringBuilder chercherMedicament = new StringBuilder();
		chercherMedicament.append("SELECT * FROM medicament ");
		chercherMedicament.append("WHERE MEDICAMENT_ID = ?");
		
		try {
			PreparedStatement requeteChercher = this.connexion.prepareStatement(chercherMedicament.toString());
			requeteChercher.setInt(1, pId);
			ResultSet res = requeteChercher.executeQuery();
			while (res.next())
			{
				medicament = new Medicament(pId, res.getString("MEDICAMENT_NOM"), 
						chercherNomType(res.getInt("TYPEMEDICAMENT_ID")), (double)(res.getFloat("MEDICAMENT_PRIX")),
						res.getInt("MEDICAMENT_STOCK"),
				LocalDate.parse(res.getString("MEDICAMENT_DATEMISESERVICE")));
			}
		} catch (EntreeException e) {
			throw new DAOException(e.getMessage());
		} catch (SQLException e) {
			System.out.println("erreur base de donnée : connexion médicament 3");
		}	
		return medicament;
		
	}

	@Override
	/**
	 * Creation d'une liste de tous les medicament de la BDD
	 * @throws DAOException 
	 */
	public ArrayList<Medicament> chercherTous() throws DAOException {
		
		ArrayList<Medicament> list = new ArrayList<>();
		StringBuilder chercherMedicament = new StringBuilder();
		chercherMedicament.append("SELECT * FROM medicament ");
		
		try {
			Statement requeteAll = this.connexion.createStatement();
			ResultSet res = requeteAll.executeQuery(chercherMedicament.toString());
			
			while (res.next()) {
				try {
					Medicament medicament = new Medicament(res.getInt("MEDICAMENT_ID"),
							res.getString("MEDICAMENT_NOM"), 
							chercherNomType(res.getInt("TYPEMEDICAMENT_ID")), 
							(double)(res.getFloat("MEDICAMENT_PRIX")), 
							res.getInt("MEDICAMENT_STOCK"),
							LocalDate.parse(res.getString("MEDICAMENT_DATEMISESERVICE")));
					list.add(medicament);
				} catch (EntreeException e) {
					throw new DAOException(e.getMessage());
				}
			}
			return list;
		} catch (SQLException e) {
			System.out.println("erreur base de donnée : connexion médicament 5");
			return null;
		}	
	}
	
	/**
	 * Recherche de l'ID d'un type de medicament a partir du nom dans la BDD
	 * @param nom : nom du type de medicament
	 * @return l'identifiant
	 */
	public int chercherIDType(String nom)
	{
		int resultat=0;
		StringBuilder chercherID = new StringBuilder();
		chercherID.append("SELECT TYPEMEDICAMENT_ID FROM typemedicament ");
		chercherID.append("WHERE TYPEMEDICAMENT_NOM = ?");
		
		try {
			PreparedStatement requeteChercher = this.connexion.prepareStatement(chercherID.toString());
			requeteChercher.setString(1,  nom);
			ResultSet res = requeteChercher.executeQuery();
			while (res.next())
			{
				resultat = res.getInt("TYPEMEDICAMENT_ID");
			}
		} catch (SQLException e) {
			System.out.println("erreur base de donnée : connexion médicament 6");		
		}
		return resultat;
	}
	
	/**
	 * Recherche du nom du type de medicament a partir de l'id dans la BDD
	 * @param id : identifiant du type de medicament
	 * @return nom du type de medicament
	 */
	public String chercherNomType(int id)
	{
		String resultat = null;
		StringBuilder chercherID = new StringBuilder();
		chercherID.append("SELECT TYPEMEDICAMENT_NOM FROM typemedicament ");
		chercherID.append("WHERE TYPEMEDICAMENT_ID = ?");
		
		try {
			PreparedStatement requeteChercher = this.connexion.prepareStatement(chercherID.toString());
			requeteChercher.setInt(1,  id);
			ResultSet res = requeteChercher.executeQuery();
			while (res.next())
			{
				resultat = res.getString("TYPEMEDICAMENT_NOM");
			}
		} catch (SQLException e) {
			System.out.println("erreur base de donnée : connexion médicament 7");	
		}
		return resultat;
	}
	
	/**
	 * Methode pour changer le stock de medicament dans la BDD
	 * @param medoc : medicament a modifier
	 * @return : resultat de l'operation
	 */
	public boolean changerStock(Medicament medoc)
	{
		boolean requeteOK = false;
		StringBuilder nouveauStock = new StringBuilder();
		nouveauStock.append("UPDATE medicament ");
		nouveauStock.append("SET medicament_stock = ? ");
		nouveauStock.append("WHERE medicament_id = ? ;");
		try {
			PreparedStatement requeteStock = this.connexion.prepareStatement(nouveauStock.toString());
			requeteStock.setInt(1, medoc.getQuantite());
			requeteStock.setInt(2, medoc.getId());
			int res = requeteStock.executeUpdate();
			if(res==0) requeteOK=false;
			else requeteOK=true;
		} catch (SQLException e) {
			System.out.println("erreur base de donnée : connexion médicament 8");
			requeteOK=false;
		}
		return requeteOK;
	}
}

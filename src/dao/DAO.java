package dao;

import java.sql.Connection;
import java.sql.SQLException;

import exceptions.DAOException;
import exceptions.EntreeException;

import java.util.ArrayList;

/**
 * Classe abstraite pour la creation des DAO
 * @author JSublon
 *
 * @param <T> : objet generique
 */
public abstract class DAO<T> {
	
	//connection à la base de données
	protected Connection connexion = Singleton.getInstanceDB();
	
	/**
	 * sauvegarde dans la base de donnees a partir d'un objet
	 * @param obj : objet JAVA
	 * @return resultat de l'operation
	 * @throws SQLException : erreur de base de donnees
	 * @throws DAOException : erreur dans l'utilisation de la base de donnees
	 * @throws EntreeException : erreur de saisie
	 */
	public abstract boolean creer(T obj) throws SQLException, DAOException, EntreeException;
	
	/**
	 * supression dans la base de donnees a partir d'un objet
	 * @param obj : objet JAVA
	 * @return resultat de l'operation
	 * @throws SQLException : erreur de base de donnees
	 * @throws DAOException : erreur dans l'utilisation de la base de donnees
	 */
	public abstract boolean effacer(T obj) throws SQLException, DAOException;
	
	/**
	 * modification de la base de donnees a partir d'un objet
	 * @param obj : objet JAVA
	 * @return resultat de l'operation
	 * @throws SQLException : erreur de base de donnees
	 * @throws DAOException : erreur dans l'utilisation de la base de donnees
	 */
	public abstract boolean modifier(T obj) throws SQLException, DAOException;
	
	/**
	 * creation d'un objet JAVA a partir de la BDD
	 * @param pId : identifiant dans la base de donnees
	 * @return objet
	 * @throws SQLException : erreur de base de donnees
	 * @throws DAOException : erreur dans l'utilisation de la base de donnees
	 */
	public abstract T chercher(Integer pId) throws SQLException, DAOException;
	
	/**
	 * creation d'une liste a partir de la BDD
	 * @return liste d'objet
	 * @throws SQLException : erreur de base de donnees
	 * @throws DAOException : erreur dans l'utilisation de la base de donnees
	 */
	public abstract ArrayList<T> chercherTous() throws SQLException, DAOException;

}

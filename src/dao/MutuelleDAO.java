package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import exceptions.DAOException;
import exceptions.EntreeException;
import metier.Mutuelle;

/**
 * Classe DAO pour la mutuelle
 * @author JSublon
 *
 */
public class MutuelleDAO extends DAO<Mutuelle> {

	@Override
	/**
	 * Ajout d'une mutuelle dans la BDD
	 */
	public boolean creer(Mutuelle obj) {
		
		StringBuilder insertMutuelle = new StringBuilder();
		insertMutuelle.append("insert into mutuelle ");
		insertMutuelle.append("(ADRESSE_ID, ");
		insertMutuelle.append("MUTUELLE_NOM, ");
		insertMutuelle.append("MUTUELLE_EMAIL, ");
		insertMutuelle.append("MUTUELLE_NUMEROTELEPHONE, ");
		insertMutuelle.append("MUTUELLE_DEPARTEMENT, ");
		insertMutuelle.append("MUTUELLE_TAUXPRISEENCHARGE) ");
		insertMutuelle.append("values (?, ?, ?, ?, ?, ?)");
		
		try {
			PreparedStatement prepareStatement = this.connexion.prepareStatement(insertMutuelle.toString());
			prepareStatement.setInt(1, obj.getAdresse().getId());
			prepareStatement.setString(2, obj.getNom());
			prepareStatement.setString(3, obj.getEmail());
			prepareStatement.setString(4, obj.getNumeroTelephone());
			prepareStatement.setString(5, obj.getDepartement());
			prepareStatement.setDouble(6, obj.getTauxPriseEnCharge());
			prepareStatement.executeUpdate();
			return true;		
		} catch (SQLException e) {
			System.out.println("erreur base de donnée : connexion mutuelle 1");
			return false;
		}
	}

	@Override
	/**
	 * suppression d'une mutuelle de la BDD
	 */
	public boolean effacer(Mutuelle obj) {
		
		StringBuilder deleteMutuelle = new StringBuilder();
		deleteMutuelle.append("DELETE FROM mutuelle ");
		deleteMutuelle.append("WHERE mutuelle_id = ?");
		
		try {
			PreparedStatement prepareStatement = this.connexion.prepareStatement(deleteMutuelle.toString());
			prepareStatement.setInt(1, obj.getId());
			prepareStatement.executeUpdate();
			return true;		
		} catch (SQLException e) {
			System.out.println("erreur base de donnée : connexion mutuelle 2");
			return false;
		}
	}

	@Override
	/**
	 * modification d'une mutuelle de la BDD
	 */
	public boolean modifier(Mutuelle obj) {
		
		StringBuilder updateMutuelle = new StringBuilder();
		updateMutuelle.append("UPDATE mutuelle ");
		updateMutuelle.append("SET ADRESSE_ID = ?, ");
		updateMutuelle.append("MUTUELLE_NOM = ?, ");
		updateMutuelle.append("MUTUELLE_EMAIL = ?, ");
		updateMutuelle.append("MUTUELLE_NUMEROTELEPHONE = ?, ");
		updateMutuelle.append("MUTUELLE_DEPARTEMENT = ?, ");
		updateMutuelle.append("MUTUELLE_TAUXPRISEENCHARGE = ? ");
		updateMutuelle.append("WHERE MUTUELLE_ID = ?");
		
		try {
			PreparedStatement prepareStatement = this.connexion.prepareStatement(updateMutuelle.toString());
			
			prepareStatement.setInt(1, obj.getAdresse().getId());
			prepareStatement.setString(2, obj.getNom());
			prepareStatement.setString(3, obj.getEmail());
			prepareStatement.setString(4, obj.getNumeroTelephone());
			prepareStatement.setString(5, obj.getDepartement());
			prepareStatement.setDouble(6, obj.getTauxPriseEnCharge() );
			prepareStatement.setInt(7, obj.getId());
			prepareStatement.executeUpdate();
			
			return true;
			
		} catch (SQLException e) {
			System.out.println("erreur base de donnée : connexion mutuelle 3");
			return false;
		}
	}

	@Override
	/**
	 * creation d'une instance Mutuelle a partir de la BDD
	 * @param pId : id de la mutuelle
	 */
	public Mutuelle chercher(Integer pId) throws DAOException {
		
		Mutuelle mutuelle = null;
		AdresseDAO connexionAdresse = new AdresseDAO();
		
		StringBuilder chercherMutuelle = new StringBuilder();
		chercherMutuelle.append("SELECT * FROM mutuelle ");
		chercherMutuelle.append("WHERE mutuelle_id = ?");
		
		try {
			PreparedStatement requeteChercher = this.connexion.prepareStatement(chercherMutuelle.toString());
			requeteChercher.setInt(1, pId);
			ResultSet res = requeteChercher.executeQuery();
			
			while (res.next()) {
				try {
					mutuelle = new Mutuelle(
							res.getInt("MUTUELLE_ID"), 
							res.getString("MUTUELLE_NOM"),
							res.getString("MUTUELLE_EMAIL"),
							res.getString("MUTUELLE_DEPARTEMENT"),
							res.getString("MUTUELLE_NUMEROTELEPHONE"),
							res.getDouble("MUTUELLE_TAUXPRISEENCHARGE") ,
							connexionAdresse.chercher(res.getInt("ADRESSE_ID")));		
					} catch (EntreeException e) {
						throw new DAOException(e.getMessage());
					}
			}
			
			} catch (SQLException e) {
				System.out.println("erreur base de donnée : connexion mutuelle 5");
			}
		
		return mutuelle;
	}

	@Override
	/**
	 * procedure pour creer les instances de toutes les Mutuelles
	 * presentes dans la BDD
	 * @throws DAOException 
	 */
	public ArrayList<Mutuelle> chercherTous() throws DAOException {
		
		AdresseDAO connexionAdresse = new AdresseDAO();
		ArrayList<Mutuelle> list = new ArrayList<>();
		StringBuilder chercherMutuelle = new StringBuilder();
		
		chercherMutuelle.append("SELECT * FROM mutuelle ");

		try {
			Statement requeteAll = this.connexion.createStatement();
			ResultSet res = requeteAll.executeQuery(chercherMutuelle.toString());
			
			while (res.next()) {
				try {
					Mutuelle mutuelle = new Mutuelle(
							res.getInt("MUTUELLE_ID"), 
							res.getString("MUTUELLE_NOM"),
							res.getString("MUTUELLE_EMAIL"),
							res.getString("MUTUELLE_DEPARTEMENT"),
							res.getString("MUTUELLE_NUMEROTELEPHONE"),
							res.getDouble("MUTUELLE_TAUXPRISEENCHARGE") ,
							connexionAdresse.chercher(res.getInt("ADRESSE_ID")));	
					;
					list.add(mutuelle);

				} catch (EntreeException e) {
					throw new DAOException(e.getMessage());
				}
			}
			return list;
		} catch (SQLException e) {
			System.out.println("erreur base de donnée : connexion mutuelle 7 ");
			return null;
		}
	}
}

package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.time.LocalDate;
import java.util.ArrayList;

import exceptions.DAOException;
import exceptions.EntreeException;
import metier.Achat;
import metier.AchatAvecOrdonnance;
import metier.AchatSansOrdonnance;
import metier.Medicament;
import metier.Specialiste;

/**
 * Classe DAO pour les achats
 * @author JSublon
 *
 */
public class AchatDAO extends DAO<Achat> {
	
	@Override
	/**
	 * Ajout d'un achat dans la BDD
	 * @param obj : achat a ajouter
	 * @return statut de l'operation
	 * @throws SQLException : erreur de connexion
	 * @throws DAOException  : erreur base de donnee 
	 */
	public boolean creer(Achat obj) throws SQLException, DAOException, EntreeException {
		boolean requeteOK = false;
		
		if(obj.isOrdonnance()==true) //l'achat contient une ordonnance
		{
			requeteOK = creerAvecOrdonnance((AchatAvecOrdonnance) obj);
		}
		else
		{
			requeteOK = creerSansOrdonnance((AchatSansOrdonnance) obj);
		}
		return requeteOK;
	}

	@Override
	/**
	 * suppression d'un achat de la BDD
	 * @param obj : achat a effacer
	 * @return statut de l'operation
	 */
	public boolean effacer(Achat obj) {
		StringBuilder deleteAchatAvecOrdonnance = new StringBuilder();
		deleteAchatAvecOrdonnance.append("DELETE FROM achat ");
		deleteAchatAvecOrdonnance.append("WHERE achat_id = ?");
		
		try {
			PreparedStatement prepareStatement = this.connexion.prepareStatement(deleteAchatAvecOrdonnance.toString());
			prepareStatement.setInt(1, obj.getId());
			prepareStatement.executeUpdate();
			return true;
			
		} catch (SQLException e) {
			System.out.println("erreur base de donnée : connexion achat 1");
			return false;
		}
	}

	@Override
	/**
	 * modifier un achat de la BDD
	 * @param obj : achat a modifier
	 * @return statut de l'operation
	 * @throws SQLException 
	 */
	public boolean modifier(Achat obj) throws SQLException {
		boolean requeteOK = false;
		if(obj.isOrdonnance()==true) //l'achat a une ordonnance
		{
			requeteOK = modifierAvecOrdonnance((AchatAvecOrdonnance) obj);
		}
		else
		{
			requeteOK = modifierSansOrdonnance((AchatSansOrdonnance) obj);
		}
		return requeteOK;
	}
	
	/**
	 * ajout d'un achat avec ordonnance dans la BDD
	 * @param obj : achat avec ordonnance a ajouter
	 * @return statut de l'operation
	 * @throws DAOException  : erreur base de donnee
	 * @throws EntreeException  : erreur de saisie
	 */
	public boolean creerAvecOrdonnance(AchatAvecOrdonnance obj) throws DAOException, EntreeException {
		
		boolean transactionOK = false;
		try {
			//debut transaction
			this.connexion.setAutoCommit(false);
			StringBuilder insertAchatAvecOrdonnance = new StringBuilder();
			insertAchatAvecOrdonnance.append("insert into achat ");
			insertAchatAvecOrdonnance.append("(CLIENT_ID, ");
			insertAchatAvecOrdonnance.append("SPECIALISTE_ID, ");
			insertAchatAvecOrdonnance.append("MEDECIN_ID, ");
			insertAchatAvecOrdonnance.append("ACHAT_DATE) ");
			insertAchatAvecOrdonnance.append("values (?, ?, ?, ?)");
			
			PreparedStatement prepareStatement = this.connexion.prepareStatement(insertAchatAvecOrdonnance.toString());
			prepareStatement.setInt(1, obj.getPatient().getId());
				
			prepareStatement.setInt(3, obj.getDocteur().getId());
			prepareStatement.setString(4, obj.dateToString());
			
			/*on differencie le cas ou l'achat a un specialiste
			 * et un achat ou il n'y en a pas
			 */
			if (obj.getSpecialiste()==null)
			{
				prepareStatement.setNull(2, Types.INTEGER); //on met une id null
			}
			else
			{
				
				prepareStatement.setInt(2, obj.getSpecialiste().getId());
			}
			int pS=prepareStatement.executeUpdate();
			if(pS==0) this.connexion.rollback();
			else
			{
				try {
					obj.setId(UtilitaireDAO.dernierIdEntree()); //on cherche l'id de l'achat qu'on vient de créer
					transactionOK=true;
				} catch (EntreeException e) {
					transactionOK=false;
					throw new DAOException(e.getMessage());
				}
				if(transactionOK==true)
				{
					transactionOK=creerPanier(obj); //ajout des medicament dans la table contenir
				}
				else 
					{
					this.connexion.rollback();
					}
				if(transactionOK==true)
				{
					transactionOK=changerStock(obj.getId());
				}
				else this.connexion.rollback();
				if(transactionOK==true)
				{
					this.connexion.commit();
				}
				else
				{
					this.connexion.rollback();
				}
			}
		}
		catch(SQLException sqle) {
			System.out.println("erreur base de donnée : connexion achat 3");
		}
		return transactionOK;
	}		
	
	/**
	 * ajout d'un achat sans ordonnance dans la BDD
	 * @param obj : achat a ajouter
	 * @return statut de l'operation
	 * @throws DAOException  : erreur base de donnee 
	 * @throws EntreeException  : erreur de saisie
	 */
	public boolean creerSansOrdonnance(AchatSansOrdonnance obj) throws DAOException, EntreeException {

		boolean transactionOK=false;
			try {
				//debut de la transaction
				this.connexion.setAutoCommit(false);
				StringBuilder insertAchatSansOrdonnance = new StringBuilder();
				insertAchatSansOrdonnance.append("insert into achat ");
				insertAchatSansOrdonnance.append("(CLIENT_ID, ");
				insertAchatSansOrdonnance.append("ACHAT_DATE) ");
				insertAchatSansOrdonnance.append("values (?, ?)");
				
				PreparedStatement prepareStatement = this.connexion.prepareStatement(insertAchatSansOrdonnance.toString());
				prepareStatement.setInt(1, obj.getPatient().getId());
				prepareStatement.setString(2, obj.dateToString());
				int pS = prepareStatement.executeUpdate();
				if(pS==0) this.connexion.rollback();
				else
				{
					try {
						obj.setId(UtilitaireDAO.dernierIdEntree()); //id de l'achat venant d'etre enregistre
						transactionOK=true;
					} catch (EntreeException e) {
						transactionOK=false;
						throw new DAOException(e.getMessage());
					}
					if(transactionOK==true)
					{
						transactionOK=creerPanier(obj); //ajout des medicament dans la table contenir
					}
					else 
						{
						this.connexion.rollback();
						}
					if(transactionOK==true)
					{
						transactionOK=changerStock(obj.getId());
					}
					else this.connexion.rollback();
					if(transactionOK==true)
					{
						this.connexion.commit();
					}
					else
					{
						this.connexion.rollback();
					}
				}
			}
			catch (SQLException  e){
				System.out.println("erreur base de donnée : connexion achat 5");
			}
			return transactionOK;
		}		
	

	/**
	 * modifier un achat avec ordonnance dans la BDD 
	 * @param obj : achat avec ordonnance
	 * @return statut de l'operation
	 */
	public boolean modifierAvecOrdonnance(AchatAvecOrdonnance obj){
		
		boolean transactionOK=false;
		try {
			//debut de la transaction
			this.connexion.setAutoCommit(false);
			StringBuilder updateAchatAvecOrdonnance = new StringBuilder();
			updateAchatAvecOrdonnance.append("UPDATE achat ");
			updateAchatAvecOrdonnance.append("SET CLIENT_ID = ?, ");
			updateAchatAvecOrdonnance.append("SPECIALISTE_ID = ?, ");
			updateAchatAvecOrdonnance.append("MEDECIN_ID = ?, ");
			updateAchatAvecOrdonnance.append("ACHAT_DATE = ? ");
			updateAchatAvecOrdonnance.append("WHERE ACHAT_ID = ?");

			PreparedStatement prepareStatement = this.connexion.prepareStatement(updateAchatAvecOrdonnance.toString());
				
			prepareStatement.setInt(1, obj.getPatient().getId());
			prepareStatement.setInt(3, obj.getDocteur().getId());
			prepareStatement.setString(4, obj.dateToString());
			prepareStatement.setInt(5, obj.getId());
				
			//Cas avec Specialiste et cas sans
			if (obj.getSpecialiste()==null)
			{
				prepareStatement.setNull(2, Types.INTEGER);
			}
			else
			{
				prepareStatement.setInt(2, obj.getSpecialiste().getId());
				}
			int pS = prepareStatement.executeUpdate();
			if(pS==0) this.connexion.rollback();
			else
			{
				transactionOK=modifierPanier(obj); //modification de la table contenir
			}
			if(transactionOK==true) this.connexion.commit();
			else 
			{
				this.connexion.rollback();
			}
		} catch (SQLException e) {
			System.out.println("erreur base de donnée : connexion achat 6");
		}
		return transactionOK;
	}

	/**
	 * Modifier un achat sans ordonnance dans la BDD
	 * @param obj : achat sans ordonnance
	 * @return staut de l'operation
	 * @throws SQLException : erreur sur la base de donnees
	 */
	public boolean modifierSansOrdonnance(AchatSansOrdonnance obj) throws SQLException {
			
		boolean transactionOK=false;
			try {
				//debut de la transaction
				this.connexion.setAutoCommit(false);
				StringBuilder updateAchatSansOrdonnance = new StringBuilder();
				updateAchatSansOrdonnance.append("UPDATE achat ");
				updateAchatSansOrdonnance.append("SET CLIENT_ID = ?, ");
				updateAchatSansOrdonnance.append("ACHAT_DATE = ? ");
				updateAchatSansOrdonnance.append("WHERE ACHAT_ID = ?");
				
				PreparedStatement prepareStatement = this.connexion.prepareStatement(updateAchatSansOrdonnance.toString());
					
				prepareStatement.setInt(1, obj.getPatient().getId());
				prepareStatement.setString(2, obj.dateToString());
				prepareStatement.setInt(3, obj.getId());	
				int pS=prepareStatement.executeUpdate();
				if(pS==0) this.connexion.rollback();
				else
				{
					transactionOK=modifierPanier(obj); //modification de la table contenir
				}
				if(transactionOK==true) this.connexion.commit();
				else this.connexion.rollback();
			} catch (SQLException e) {
				System.out.println("erreur base de donnée : connexion achat 7");
			}	
			return transactionOK;
		}

	@Override
	/**
	 * creation d'un achat a partir de la base de donnees
	 * @param pId : id de l'achat
	 * @return achat avec ou sans ordonnance
	 * @throws DAOException  : erreur base de donnee 
	 * 
	 */
	public Achat chercher(Integer pId) throws DAOException {
		
		Achat achat = null;
		StringBuilder chercherAchatAvecOrdonnance = new StringBuilder();
		chercherAchatAvecOrdonnance.append("SELECT * FROM achat ");
		chercherAchatAvecOrdonnance.append("WHERE achat_id = ?;");
	
		//appel des DAO
		MedecinDAO conMedecin = new MedecinDAO();
		PatientDAO conPatient = new PatientDAO();
		SpecialisteDAO conSpecialiste = new SpecialisteDAO();
		
		try {
			PreparedStatement requeteChercher = this.connexion.prepareStatement(chercherAchatAvecOrdonnance.toString());
			requeteChercher.setInt(1, pId);
			ResultSet res = requeteChercher.executeQuery();
			
			while (res.next()) {
				/*Il y a un medecin pour l'achat
				 * c'est un achat avec ordonnance
				 */
				if(res.getObject("MEDECIN_ID")!=null) 
				{
					//il n'y a pas de specialiste
					if(res.getObject("SPECIALISTE_ID")==null)
					{
						try {
							achat = new AchatAvecOrdonnance(
									res.getInt("ACHAT_ID"), 
									conPatient.chercher(res.getInt("CLIENT_ID")),
									LocalDate.parse(res.getString("ACHAT_DATE")),
									chercherMedocs(res.getInt("ACHAT_ID")),
									conMedecin.chercher(res.getInt("MEDECIN_ID")));	
							
							} catch (EntreeException e) {
								throw new DAOException(e.getMessage());
							}
					}
					
					else
					{
						try {
							achat = new AchatAvecOrdonnance(
									res.getInt("ACHAT_ID"), 
									conPatient.chercher(res.getInt("CLIENT_ID")),
									LocalDate.parse(res.getString("ACHAT_DATE")),
									chercherMedocs(res.getInt("ACHAT_ID")),
									conMedecin.chercher(res.getInt("MEDECIN_ID")),
									conSpecialiste.chercher(res.getInt("SPECIALISTE_ID")));	
							
							} catch (EntreeException e) {
								throw new DAOException(e.getMessage());
							}
					}
				}
				else //achat sans ordonnance
				{
					try {
						achat = new AchatSansOrdonnance(
								res.getInt("ACHAT_ID"), 
								conPatient.chercher(res.getInt("CLIENT_ID")),
								LocalDate.parse(res.getString("ACHAT_DATE")),
								chercherMedocs(res.getInt("ACHAT_ID")));	
						
						} catch (EntreeException e) {
							throw new DAOException(e.getMessage());
						}
				}
				
			}
			
			} catch (SQLException e) {
				System.out.println("erreur base de donnée : connexion achat 11");
			}
		return achat;
	}

	@Override
	/**
	 * Methode pour recuperer tous les achats
	 * @return liste des achats
	 * @throws DAOException  : erreur base de donnee 
	 */
	public ArrayList<Achat> chercherTous() throws DAOException {
		
		ArrayList<Achat> list = new ArrayList<>();
		StringBuilder chercherAchatAvecOrdonnance = new StringBuilder();
		chercherAchatAvecOrdonnance.append("SELECT * FROM achat; ");
		//appel des DAO
		Specialiste special = new Specialiste();
		MedecinDAO conMedecin = new MedecinDAO();
		PatientDAO conPatient = new PatientDAO();
		SpecialisteDAO conSpecialiste = new SpecialisteDAO();
		
		Achat achat = null;
		
		try {
			Statement requeteAll = this.connexion.createStatement();
			ResultSet res = requeteAll.executeQuery(chercherAchatAvecOrdonnance.toString());
			
			while (res.next()) {
				achat = null; //on reinitilise l'achat
				//on verifie que l'achat est d'aujourd'hui
				if(res.getString("ACHAT_DATE").contains(LocalDate.now().toString()))
				{
					//il y a un medecin donc c'est un achat avec ordonnance
					if(res.getObject("MEDECIN_ID")!=null)
					{
						if(res.getObject("SPECIALISTE_ID")==null)
						{
							try {
								achat = new AchatAvecOrdonnance(
										res.getInt("ACHAT_ID"), 
										conPatient.chercher(res.getInt("CLIENT_ID")),
										LocalDate.parse(res.getString("ACHAT_DATE")),
										chercherMedocs(res.getInt("ACHAT_ID")),
										conMedecin.chercher(res.getInt("MEDECIN_ID")),
										special);	
								} catch (EntreeException e) {
									throw new DAOException(e.getMessage());
								}
						}
						
						else
						{
							try {
								achat = new AchatAvecOrdonnance(
										res.getInt("ACHAT_ID"), 
										conPatient.chercher(res.getInt("CLIENT_ID")),
										LocalDate.parse(res.getString("ACHAT_DATE")),
										chercherMedocs(res.getInt("ACHAT_ID")),
										conMedecin.chercher(res.getInt("MEDECIN_ID")),
										conSpecialiste.chercher(res.getInt("SPECIALISTE_ID")));	
								
								} catch (EntreeException e) {
									throw new DAOException(e.getMessage());
								}
						}
					}
					else
					{
						try {
							achat = new AchatSansOrdonnance(
									res.getInt("ACHAT_ID"), 
									conPatient.chercher(res.getInt("CLIENT_ID")),
									LocalDate.parse(res.getString("ACHAT_DATE")),
									chercherMedocs(res.getInt("ACHAT_ID")));	
							} catch (EntreeException e) {
								throw new DAOException(e.getMessage());
							}
					}
				}
				if (achat!=null) list.add(achat);
			}		
			return list;
		} catch (SQLException e) {
			System.out.println("erreur base de donnée : connexion achat 15");
			return null;
		}
	}
	

	/**
	 * methode pour creer un panier dans la table contenir
	 * @param obj : achat
	 * @return statut de l'operation
	 */
	public boolean creerPanier(Achat obj) {

		boolean erreur = false;
		for(Medicament medoc : obj.getListeMedicament())
		{
			ajouterMedoc(medoc.getId(), obj.getId());
		}	
		return !erreur;
	}

	/**
	 * Methode pour effacer un panier
	 * @param obj : achat
	 * @return statut de l'operation
	 */
	public boolean effacerPanier(Achat obj) {

		StringBuilder effacer = new StringBuilder();
		effacer.append("DELETE FROM contenir ");
		effacer.append("WHERE achat_id = ?;");
		
		try {
			PreparedStatement requeteEffacer = this.connexion.prepareStatement(effacer.toString());
			requeteEffacer.setInt(1, obj.getId());
			requeteEffacer.executeUpdate();
			return true;
		} catch (SQLException e) {
			System.out.println("erreur base de donnée : connexion achat 18");
			return false;
		}
	}

	/**
	 * Methode pour modifier un panier dans la table contenir
	 * @param obj : achat
	 * @return statut de l'operation
	 */
	public boolean modifierPanier(Achat obj) {
		
		boolean trouve = true;
		//on supprime la panier
		trouve=effacerPanier(obj);
		//si on a supprimer le panier, on le rempli avec les nouvelles donnees
		if (trouve==true)
		trouve=creerPanier(obj);
		return trouve;
	}

	/**
	 * Methode pour creer une liste de medicaments a partir de l'id d'un achat
	 * @param pId : id de l'achat
	 * @return liste des medicament de l'achat
	 * @throws SQLException : erreur sur la base de donnee
	 * @throws DAOException  : erreur base de donnee
	 */
	public ArrayList<Medicament> chercherMedocs(int pId) throws SQLException, DAOException
	{
		ArrayList<Medicament> listeMedoc = new ArrayList<>();
		MedicamentDAO conMedicament = new MedicamentDAO();
		StringBuilder ajoutMedoc = new StringBuilder();
		ajoutMedoc.append("SELECT medicament_id, contenir_quantite FROM contenir ");
		ajoutMedoc.append("WHERE achat_id = ?;");
		
		PreparedStatement requeteMedoc = this.connexion.prepareStatement(ajoutMedoc.toString());
		requeteMedoc.setInt(1, pId);
		ResultSet res = requeteMedoc.executeQuery();
		while(res.next())
		{
			/*Pour chaque medicament trouve dans la table avec l'id de l'achat
			 * on fait des boucles en fonction de la quantité du médicament
			 * pour créer une liste de médicaments
			 */
			for(int i=1; i<=res.getInt("CONTENIR_QUANTITE"); i++)
			{
				listeMedoc.add(conMedicament.chercher(res.getInt("MEDICAMENT_ID")));
			}
		}
		return listeMedoc;
	}
	
	/**
	 * Methode pour verifier qu'un medicament est dans un achat
	 * @param idMedoc : id du medicament
	 * @param idAchat : id de l'achat
	 * @return statut de l'operation
	 */
	public boolean verifMedoc(int idMedoc, int idAchat)
	{
		boolean trouve = false;
		/*requete pour compter le nombre de fois que le couple
		 * (id medoc, id achat) est dans la table contenir
		 */
		StringBuilder verif = new StringBuilder();
		verif.append("SELECT COUNT(*) FROM contenir ");
		verif.append("WHERE medicament_id = ? AND ");
		verif.append("achat_id = ?;");
		
		try {
			PreparedStatement requeteVerif = this.connexion.prepareStatement(verif.toString());
			requeteVerif.setInt(1, idMedoc);
			requeteVerif.setInt(2, idAchat);
			ResultSet res = requeteVerif.executeQuery();
			while(res.next())
			{
				//si le couple est trouvé une fois, c'est bon
				if(res.getInt("COUNT(*)")==1)
					{
						trouve=true;
					}
			}
		} catch (SQLException e) {
			System.out.println("erreur base de donnée : connexion achat 19");
		}		
		return trouve;
	}
	
	/**
	 * Methode pour ajouter un medicament dans la table contenir
	 * @param idMedoc : id du medicament
	 * @param idAchat : id de l'achat
	 * @return statut de l'operation
	 */
	public boolean ajouterMedoc(int idMedoc, int idAchat)
	{
		boolean transactionOK=false;
		int nombreMedoc=0;
		
		try {
			StringBuilder nbrMedoc = new StringBuilder();
			nbrMedoc.append("SELECT contenir_quantite FROM contenir ");
			nbrMedoc.append("WHERE medicament_id = ? AND achat_id = ?;");
			
			PreparedStatement getNbrMedoc = this.connexion.prepareStatement(nbrMedoc.toString());
			getNbrMedoc.setInt(1, idMedoc);
			getNbrMedoc.setInt(2, idAchat);
			
			ResultSet res = getNbrMedoc.executeQuery();
			while(res.next())
			{
				nombreMedoc = res.getInt("contenir_quantite");
			}
			
			StringBuilder ajoutMedoc = new StringBuilder();
			
			/*Si le médicament ne se trouve pas dans la panier
			 * on l'ajoute dans la table avec la quantité 1
			 */
			if(nombreMedoc==0)
			{
				ajoutMedoc.append("INSERT INTO contenir (");
				ajoutMedoc.append("medicament_id, ");
				ajoutMedoc.append("achat_id, ");
				ajoutMedoc.append("contenir_quantite) ");
				ajoutMedoc.append("VALUES (?, ?, 1); ");				
			}
			/*
			 * Si le médicament est dans la table contenir
			 * on modifier la quantité pour l'incrémenter de 1
			 */
			else
			{
				ajoutMedoc.append("UPDATE contenir ");
				ajoutMedoc.append("SET contenir_quantite = contenir_quantite + 1 ");
				ajoutMedoc.append("WHERE medicament_id = ? AND achat_id = ?;");
			}
			
			PreparedStatement requeteAjout = this.connexion.prepareStatement(ajoutMedoc.toString());
			requeteAjout.setInt(1, idMedoc);
			requeteAjout.setInt(2, idAchat);
			requeteAjout.executeUpdate();
			transactionOK=true;

		} catch (SQLException e) {
			System.out.println("erreur base de donnée : connexion achat 20");
			transactionOK=false;
	
		}
		return transactionOK;
	}

	/**
	 * fonction pour modifier le stock de medicament
	 * @param idAchat : id de l'achat
	 * @return : resultat de l'operation
	 * @throws EntreeException : erreur saisie
	 * @throws DAOException : erreur de connexion BDD
	 */
	public boolean changerStock(int idAchat) throws EntreeException, DAOException
	{
		MedicamentDAO conMedoc = new MedicamentDAO();
		boolean requeteOK = true;
		StringBuilder chercherPanier = new StringBuilder();
		chercherPanier.append("SELECT medicament_id, contenir_quantite FROM contenir ");
		chercherPanier.append("WHERE achat_id = ? ;");
		
		try {
			PreparedStatement requetePanier = this.connexion.prepareStatement(chercherPanier.toString());
			requetePanier.setInt(1, idAchat);
			ResultSet res = requetePanier.executeQuery();
			while(res.next()&&requeteOK==true)
			{
				int id = res.getInt("medicament_id");
				int stock = res.getInt("contenir_quantite");
				Medicament medoc = conMedoc.chercher(id);
				medoc.setQuantite(medoc.getQuantite()-stock);
				requeteOK=conMedoc.changerStock(medoc);
			}
		} catch (SQLException e) {
			requeteOK=false;
			System.out.println("erreur base de donnée : connexion achat 21");
		}
		return requeteOK;
	}
}

package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import exceptions.DAOException;
import exceptions.EntreeException;
import metier.Adresse;

/**
 * Classe DAO pour l'adresse
 * @author JSublon
 *
 */
public class AdresseDAO extends DAO<Adresse> {

	@Override
	/**
	 * Ajout d'une adresse dans la BDD
	 * @param obj : adresse a ajouter
	 * @return statut de l'operation
	 */
	public boolean creer(Adresse obj) throws DAOException {
		
		if (verifAdresse(obj)==0) //l'adresse n'existe pas dans la BDD
			{
				StringBuilder creerAdresse = new StringBuilder();
				creerAdresse.append("INSERT INTO adresse ");
				creerAdresse.append("(ADRESSE_NUMERORUE, ADRESSE_NOMRUE, ");
				creerAdresse.append("ADRESSE_CODEPOSTAL, ADRESSE_NOMVILLE) ");
				creerAdresse.append("values (?, ?, ?, ?)");
				
				try {
					
					PreparedStatement requeteCreer = this.connexion.prepareStatement(creerAdresse.toString());
					requeteCreer.setString(1,obj.getNumeroRue());
					requeteCreer.setString(2, obj.getVoie());
					requeteCreer.setString(3, obj.getCodePostal());
					requeteCreer.setString(4, obj.getVille());				
					requeteCreer.executeUpdate();				
					return true;
				} catch (SQLException e) {
					System.out.println("erreur base de donnée : connexion adresse 1");
					return false;
				}
			}
		else return false;
	}

	@Override
	public boolean effacer(Adresse obj) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean modifier(Adresse obj) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	/**
	 * creation d'une adresse a partir de la BDD
	 * @param pId : id de l'adresse
	 * @return adresse
	 */
	public Adresse chercher(Integer pId) throws DAOException {
		
		StringBuilder chercherAdresse = new StringBuilder();
		chercherAdresse.append("SELECT * FROM adresse ");
		chercherAdresse.append("WHERE ADRESSE_ID = ?");
		
		Adresse adresse = null;
		
		try {
			PreparedStatement requeteChercher = this.connexion.prepareStatement(chercherAdresse.toString());
			requeteChercher.setInt(1, pId);
			ResultSet res = requeteChercher.executeQuery();
			while (res.next())
			{
			adresse = new Adresse(pId, res.getString("ADRESSE_NUMERORUE"),
					res.getString("ADRESSE_NOMRUE"), res.getString("ADRESSE_CODEPOSTAL"),
					res.getString("ADRESSE_NOMVILLE"));
			}
			return adresse;
		} catch (EntreeException e) {
			throw new DAOException(e.getMessage());
		} catch (SQLException  e) {
			System.out.println("erreur base de donnée : connexion adresse 3");
			return null;
		} 	
	}

	@Override
	/**
	 * creation de la liste de toutes les adresses
	 * @return liste des adresses
	 * @throws DAOException 
	 */
	public ArrayList<Adresse> chercherTous() throws DAOException {
		
		StringBuilder chercherAdresse = new StringBuilder();
		chercherAdresse.append("SELECT * FROM adresse ");
		
		ArrayList<Adresse> list = new ArrayList<>();
		
		try {
			Statement requeteChercher = this.connexion.createStatement();
			
			ResultSet res = requeteChercher.executeQuery(chercherAdresse.toString());
			while (res.next())
			{
			Adresse adresse = new Adresse(res.getInt("ADRESSE_ID"), res.getString("ADRESSE_NUMERORUE"),
					res.getString("ADRESSE_NOMRUE"), res.getString("ADRESSE_CODEPOSTAL"),
					res.getString("ADRESSE_NOMVILLE"));
			list.add(adresse);
			}
			return list;
		} catch (EntreeException e) {
			throw new DAOException(e.getMessage());
		} catch (SQLException e) {
			System.out.println("erreur base de donnée : connexion adresse 5");
			return null;
		}
	}
	
	/**
	 * fonction pour verifier qu'une adresse existe dans la BDD
	 * @param adresse : adresse recherchee
	 * @return : identifiant de l'adresse de la BDD
	 * @throws DAOException : erreur utilisation BDD
	 */
	public int verifAdresse(Adresse adresse) throws DAOException
	{
		int identifiant = 0;
		
		ArrayList<Adresse> listAdresse = chercherTous();
		for(Adresse tempAdresse : listAdresse)
		{
			if (adresse.getVille().toUpperCase().equals(tempAdresse.getVille().toUpperCase())
					&& adresse.getCodePostal().equals(tempAdresse.getCodePostal())
					&& adresse.getVoie().toUpperCase().equals(tempAdresse.getVoie().toUpperCase())
					&& adresse.getNumeroRue().toUpperCase().equals(tempAdresse.getNumeroRue().toUpperCase()))
			{
				
				identifiant = tempAdresse.getId();
			}
		}
		return identifiant;
	}
}
